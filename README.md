# Modernizing Medicine (modmed) Java Workshop

This repo contains all examples and exercises for the Java Workshop organized by Modernizing Medicine.

# Install
````shell
$ ./mvnw clean install
````