package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class BasicConversion {
    public static void main(String[] args) {
        String stringValue = "100";
        Integer integerWrappedObject = Integer.valueOf(stringValue);
        // Using Unboxing again...
        int resultValue = Integer.sum(integerWrappedObject, 30000);
        String octalRepresentation = Integer.toOctalString(resultValue);
        String hexRepresentation = Integer.toHexString(resultValue);
        System.out.println("resultValue = " + resultValue);
        System.out.println("octalRepresentation = " + octalRepresentation);
        System.out.println("hexRepresentation = " + hexRepresentation);
    }
}
