package com.modmed.workshop.session4.numbersandstrings.challenges.numbers;

public class EuroConversion {
    /***
     * Convert CLP (pesos) in euros
     * @param args
     */
    public static void main(String[] args) {
        Double clpPartialAmount = 1200000.91;
        Double clpTotalAmount = 5521546.51;
        Double currentClpEuro = 851.20;

        /***
         * NOTE: Use as an example "ConversionUseCase"
         * 1. Use the 5 decimals and use the ROUNDING_MODE to truncate amounts
         * 1.1 Use the same pattern as BTC for total amount.
         * 2. Take both converted amount and assigned to a bill with this information
         * 2.1 Patient full name: Arturo Lopez - RUT: 15623369.1
         * 2.2 Description: "Europe tour"
         * 3.3 CurrencyType: EUR
         * 4. Printed out to console
         */

    }
}
