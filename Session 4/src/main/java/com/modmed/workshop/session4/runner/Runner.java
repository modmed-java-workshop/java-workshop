package com.modmed.workshop.session4.runner;

import com.modmed.workshop.session4.strings.builder.HtmlBuilder;

public class Runner {

    public static void main(String[] args) {
        // ordinary non-fluent builder
        HtmlBuilder builder = new HtmlBuilder("ul");
        builder.addChild("li", "hello");
        builder.addChild("li", "world");
        System.out.println(builder);

        // fluent builder
        builder.clear();
        builder.addChildFluent("li", "hello")
                .addChildFluent("li", "world");
        System.out.println(builder);
    }
}
