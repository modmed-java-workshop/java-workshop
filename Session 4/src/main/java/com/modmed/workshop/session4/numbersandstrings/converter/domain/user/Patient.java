package com.modmed.workshop.session4.numbersandstrings.converter.domain.user;

public class Patient {
    private final String comepleteName;
    private final String rut;

    public Patient(String comepleteName, String rut) {
        this.comepleteName = comepleteName;
        this.rut = rut;
    }

    public String getComepleteName() {
        return comepleteName;
    }

    public String getRut() {
        return rut;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "comepleteName='" + comepleteName + '\'' +
                ", rut='" + rut + '\'' +
                '}';
    }
}
