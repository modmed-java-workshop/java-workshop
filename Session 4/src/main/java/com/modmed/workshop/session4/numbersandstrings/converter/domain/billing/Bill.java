package com.modmed.workshop.session4.numbersandstrings.converter.domain.billing;

import com.modmed.workshop.session4.numbersandstrings.converter.domain.CurrencyType;
import com.modmed.workshop.session4.numbersandstrings.converter.domain.user.Patient;
import java.math.BigDecimal;

public class Bill {
    private String description;
    private Patient patient;
    private CurrencyType currencyType;
    private Double partialAmount;
    private BigDecimal totalAmount;

    public String getDescription() {
        return description;
    }

    public Patient getPatient() {
        return patient;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public Double getPartialAmount() {
        return partialAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    private Bill(Builder builder) {
        this.description = builder.description;
        this.patient = builder.patient;
        this.currencyType = builder.currencyType;
        this.partialAmount = builder.partialAmount;
        this.totalAmount = builder.totalAmount;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "\n\tdescription='" + description + '\'' +
                ", \n\tpatient=" + patient +
                ", \n\tcurrencyType=" + currencyType +
                ", \n\tcurrencyType(label)=" + currencyType.getLabel() +
                ", \n\tpartialAmount=" + partialAmount +
                ", \n\ttotalAmount=" + totalAmount +
                "\n}";
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String description;
        private Patient patient;
        private CurrencyType currencyType;
        private BigDecimal totalAmount;
        private Double partialAmount;

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withPatient(Patient patient) {
            this.patient = patient;
            return this;
        }

        public Builder withCurrencyType(CurrencyType currencyType) {
            this.currencyType = currencyType;
            return this;
        }

        public Builder withPartialAmount(Double partialAmount) {
            this.partialAmount = partialAmount;
            return this;
        }

        public Builder withTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        public Bill build() {
            return new Bill(this);
        }
    }
}
