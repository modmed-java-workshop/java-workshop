package com.modmed.workshop.session4.numbersandstrings.challenges.strings;

import static com.modmed.workshop.session4.numbersandstrings.basicexamples.PalindromeCheckWithStringBuilder.bestPalindromeCheck;

public class PalindromeShortImplementation {
    /*
     * Challenge: Rewrite previous implementation using the reverse() method of StringBuilder
     * */
    public static void main(String[] args) {
        System.out.println(bestPalindromeCheck("reconocer"));
    }
}
