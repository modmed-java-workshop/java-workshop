package com.modmed.workshop.session4.numbersandstrings.converter.usecases;

import com.modmed.workshop.session4.numbersandstrings.converter.domain.CurrencyType;
import com.modmed.workshop.session4.numbersandstrings.converter.domain.billing.Bill;
import com.modmed.workshop.session4.numbersandstrings.converter.domain.user.Patient;
import com.modmed.workshop.session4.numbersandstrings.converter.services.ConversionUtil;
import java.math.BigDecimal;

public class ConversionUseCase {
    public static void main(String[] args) {
        // Pesos amount
        Double ufAmount = 12.91;
        Double currentClpUf = 31264.0;
        Double currentClpBtc = 35537311.0;

        Number partialClp = ConversionUtil.convert(ufAmount, currentClpUf, CurrencyType.CLP);

        Patient patient = new Patient("Pythagoras L", "13.309.276-1");

        // Monto en UF
        Bill partialBill = Bill.builder()
                .withPatient(patient)
                .withDescription("Partial paid bill")
                .withCurrencyType(CurrencyType.CLP)
                .withPartialAmount(partialClp.doubleValue())
                .build();

        System.out.println("El estado de la boleta parcialmente pagada es: " + partialBill);

//        correctConcatenation(partialBill);

        /*
        * Total Bill amount
        * */
        Number partialBtc = ConversionUtil.convert(partialClp, currentClpBtc, CurrencyType.BTC);
        Long totalClpAmount = 213564633L;

        Number totalBtcAmount = ConversionUtil.convert(totalClpAmount, currentClpBtc, CurrencyType.BTC);

        Bill paidBill = Bill.builder()
                .withPatient(patient)
                .withDescription("Paid bill")
                .withCurrencyType(CurrencyType.BTC)
                .withPartialAmount(partialBtc.doubleValue())
                .withTotalAmount((BigDecimal) totalBtcAmount)
                .build();

        System.out.println("El estado de la boleta pagada es: " + paidBill);

//        correctConcatenation(paidBill);
    }

    static void correctConcatenation(Bill bill) {
        String estado = "El estado de la boleta es: ";
        String estadoResultante = estado.concat(bill.toString());
        System.out.println(estadoResultante);
    }

}
