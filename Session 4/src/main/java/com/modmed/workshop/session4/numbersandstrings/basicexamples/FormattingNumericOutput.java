package com.modmed.workshop.session4.numbersandstrings.basicexamples;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class FormattingNumericOutput {
    public static void main(String[] args) {
        BigDecimal bigMoney = BigDecimal.valueOf(1203253213264.23646);
        String currencyFormat = NumberFormat.getCurrencyInstance().format(bigMoney);
        System.out.format("My big wallet has: %s dollars\n", currencyFormat);
        currencyFormat = NumberFormat.getCurrencyInstance(new Locale("es", "cl")).format(bigMoney);
        System.out.format("My big wallet has: %s dollars\n", currencyFormat);
        System.out.format("Another way to show you the money!! %s \n", new DecimalFormat("###,###.##").format(bigMoney));
        System.out.format("Another way to show you the money IN CHILE!! %s \n\n",
                new DecimalFormat("###,###.##",
                        new DecimalFormatSymbols(
                                new Locale("es", "cl"))).format(bigMoney));

        float floatVar = 0.22f;
        int intVar = 20;
        String stringVar = "\"any string value\"";

        System.out.format("The value of the float variable is " +
                "%f, while the value of the integer variable is %d, " +
                "and the string is %s \n", floatVar, intVar, stringVar);
    }
}
