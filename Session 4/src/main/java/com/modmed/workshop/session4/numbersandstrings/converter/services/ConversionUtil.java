package com.modmed.workshop.session4.numbersandstrings.converter.services;

import com.modmed.workshop.session4.numbersandstrings.converter.domain.CurrencyType;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class ConversionUtil {

    public static Number convert(Number clpAmount, Double currentClpRate, CurrencyType currencyType) {
        // amount != null
        if (Objects.nonNull(clpAmount)) {
            switch (currencyType) {
                case BTC:
                    BigDecimal bdClpAmount = new BigDecimal(clpAmount.longValue());
                    BigDecimal bdCurrentClpRate = new BigDecimal(currentClpRate);
                    return bdClpAmount.divide(bdCurrentClpRate, 10, RoundingMode.HALF_UP);
                case CLF:
                    return clpAmount.longValue() / currentClpRate;
                case CLP:
                    double amountToEval = clpAmount.doubleValue() * currentClpRate;
                    // Mention the main difference between rint and round
                    return Math.round(amountToEval);
                default:
                    return 0;
            }
        }
        return 0;
    }

    public static String convertCurrencyToString(Double amount, Double currentClpUf, CurrencyType currencyType) {
        return null;
    }
}
