package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class PalindromeWithStringCore {
    public static void main(String[] args) {
        String text = "reconocer";
        System.out.println("Is this text a Palindrome? : " + isPalindrome(text));
    }
    static boolean isPalindrome(String text) {
        int length = text.length();
        int forward = 0;
        int backward = length - 1;
        while (backward > forward) {
            // Using charAt(n) to obtain a char for a given position
            char forwardChar = text.charAt(forward++);
            char backwardChar = text.charAt(backward--);
            if (forwardChar != backwardChar)
                return false;
        }
        return true;
    }
}
