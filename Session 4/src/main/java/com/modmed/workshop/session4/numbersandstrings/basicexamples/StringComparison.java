package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class StringComparison {
    public static void main(String[] args) {
        String simpleString = "This is a Simple String representation";
        System.out.println("This is NOT a Simple String representation".equals(simpleString));
        System.out.println(simpleString.endsWith("tion"));
        System.out.println(simpleString.startsWith("Th"));
        System.out.println(simpleString.equalsIgnoreCase("THIS IS A SIMPLE STRING REPRESENTATION"));
    }
}
