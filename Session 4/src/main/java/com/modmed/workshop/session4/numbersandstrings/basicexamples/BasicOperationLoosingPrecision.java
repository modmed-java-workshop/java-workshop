package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class BasicOperationLoosingPrecision {
    public static void main(String[] args) {
//        int currentUfClpValue = 30000;
         int currentUfClpValue = 31152;
        System.out.println("25000 pesos son UF: " + UfClpPrimitiveTypeUtil.clpToUf(25000, currentUfClpValue));
        System.out.println("35000 pesos son UF: " + UfClpPrimitiveTypeUtil.clpToUf(35000, currentUfClpValue));
        System.out.println("3.51 UFs son CLP: " + UfClpPrimitiveTypeUtil.ufToClp(3.51, currentUfClpValue));
    }
}

class UfClpPrimitiveTypeUtil {
    static double clpToUf(int clpAmount, int currentUfClp) {
        return clpAmount / currentUfClp;
    }
    static int ufToClp(double ufAmount, int currentUfClp) {
        return (int) ufAmount * currentUfClp;
    }
}