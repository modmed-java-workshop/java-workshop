package com.modmed.workshop.session4.runner;

public class Sandbox {
    public static void main(String[] args) {
//        String name1 = "Alv" + "aro";
//        String name2 = new String("Alvaro");
//        System.out.println(name1 == name2);
//
//        name1 = "Alv" + "aro";
//        name2 = "Alvaro";
//        System.out.println(name1 == name2);

        // Failing concat
        String string1 = "Hola ";
        String string2 = " Mundo";
        String stringCorrectConcat = string1.concat(string2);
        System.out.println(stringCorrectConcat);
    }
}
