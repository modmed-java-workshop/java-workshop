package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class BasicComparison {
    public static void main(String[] args) {
        Integer objectA = 1;
        Integer objectB = 2;
        System.out.println(objectA.compareTo(objectB));
        objectA = 1;
        objectB = 1;
        System.out.println(objectA.compareTo(objectB));
        objectA = 2;
        objectB = 1;
        System.out.println(objectA.compareTo(objectB));
        Byte b = 100;
        Integer i = 100;
        b.equals(i);
    }
}
