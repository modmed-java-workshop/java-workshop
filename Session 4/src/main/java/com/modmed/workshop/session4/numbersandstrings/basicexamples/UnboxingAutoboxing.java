package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class UnboxingAutoboxing {
    public static void main(String[] args) {
        int primitiveInt = -1;
        /*By AUTOBOXING conversion*/
        Integer integerWrappedObject = primitiveInt;
        System.out.println("(Autoboxing) value is: " + integerWrappedObject);
        /*Passing a parameter and UNBOXING*/
        int absoluteValueResult = absoluteValue(integerWrappedObject);
        System.out.println("(Unboxing) value is: " + absoluteValueResult);
    }

    private static int absoluteValue(int i) {
        return (i < 0) ? -i : i;
    }
}
