package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class StringManipulation {
    public static void main(String[] args) {
        String simpleString = " This is a Simple String representation ";
        String trimmedString = simpleString.trim();
        String stringArray[] = trimmedString.split("\\s");
        for (String piece :
                stringArray) {
            System.out.println(piece);
            System.out.println("Contained into trimmedString " + trimmedString.contains(piece));
        }
        System.out.println("This is SHOUTING: " + simpleString.toUpperCase());
    }
}
