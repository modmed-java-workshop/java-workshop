package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class PalindromeCheckWithStringBuilder {
    public static void main(String[] args) {
//        System.out.println(isPalindromeReverseTheString("reconocer"));
        System.out.println(bestPalindromeCheck("reconocer"));
    }
    static boolean isPalindromeReverseTheString(String text) {
        StringBuilder reverse = new StringBuilder();
        char[] plain = text.toCharArray();
        for (int i = plain.length - 1; i >= 0; i--) {
            reverse.append(plain[i]);
        }
        return (reverse.toString()).equals(text);
    }

    public static boolean bestPalindromeCheck(String text) {
        return true;
    }
}
