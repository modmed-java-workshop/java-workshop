package com.modmed.workshop.session4.numbersandstrings.basicexamples;

import java.math.BigInteger;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println("**************");
        System.out.println("Integer max value: " + Integer.MAX_VALUE);
        System.out.println("**************");
        Fibonacci.generateBigIntegerList(2048);
//        Fibonacci.generateIntegerList(2048);
    }

    /**
     * Fibonacci always start as classic (e.g. 0, 1, 1, 2, 3, 5)
     */
    public static void generateBigIntegerList(int series) {
        System.out.println("**************");
        System.out.println("Integer max value: " + Integer.MAX_VALUE);
        System.out.println("**************");

        BigInteger[] bigIntegers = new BigInteger[series];

        for (int i = 0; i < series; i++) {
            if(i == 0) {
                bigIntegers[i] = BigInteger.ZERO;
            }
            int nextItemIndex = i + 1;
            if(nextItemIndex - series < 0) {
                bigIntegers[nextItemIndex] = i == 0 ? BigInteger.ONE : bigIntegers[i-1].add(bigIntegers[i]);
            }
            System.out.println("i: " + i + " - " + bigIntegers[i]);
            if (bigIntegers[i].longValue() > Integer.MAX_VALUE) {
                System.out.println("At (" + i + ") INTEGER MAX VALUE SURPASSED!! - " + Integer.MAX_VALUE);
            }
        }
    }

    public static void generateIntegerList(int series) {
        Integer[] integers = new Integer[series];

        for (int i = 0; i < series; i++) {
            if(i == 0) {
                integers[i] = 0;
            }
            int nextItemIndex = i + 1;
            if(nextItemIndex - series < 0) {
                integers[nextItemIndex] = i == 0 ? 1 : integers[i-1] + integers[i];
            }
            System.out.println("i: " + i + " - " + integers[i]);
            if (integers[i] < 0) {
                System.out.println("At (" + i + ") INTEGER MAX VALUE REACHED!! - " + Integer.MAX_VALUE);
            }
        }
    }
}
