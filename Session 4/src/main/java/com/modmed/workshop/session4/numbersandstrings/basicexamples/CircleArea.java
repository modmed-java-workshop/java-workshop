package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class CircleArea {
    public static void main(String[] args) {
        //Initialize the variables
        int radius = 10; //1 + (int)(Math.random()*100); //Random Integer in the set (11,100]

        double area1 = Math.PI * Math.pow(radius,2);
        double circumference1 = 2 * Math.PI * radius;
        double area2, circumference2, areaRatio, circumferenceRatio;

        //Output the information for the first circle
        System.out.format("The area of a circle with radius %d is %.3f\n", radius, area1);
        System.out.format("The circumference of the circle is %.3f\n", circumference1);

        //Assign the values for the 
        radius *= 2;
        area2 = Math.PI * Math.pow(radius,2);
        circumference2 = 2 * Math.PI * radius;

        System.out.format("The area of a circle with radius %d is %.3f\n", radius, area2);
        System.out.format("The circumference of the circle is %.3f\n", circumference2);

        areaRatio = (area2 / area1);
        System.out.format("The area ratio between the two circles is %.3f\n", areaRatio);

        circumferenceRatio = (circumference2 / circumference1);
        System.out.format("The circumference ratio between the two circles is %.3f\n", circumferenceRatio);
        
    }
}
