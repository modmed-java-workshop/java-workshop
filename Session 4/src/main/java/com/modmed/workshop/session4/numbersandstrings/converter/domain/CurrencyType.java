package com.modmed.workshop.session4.numbersandstrings.converter.domain;

public enum CurrencyType {
    CLP("Pesos Chilenos"),
    CLF("Unidad de Fomento"),
    EUR("Euro"),
    BTC("Bitcoin");

    private final String label;

    CurrencyType(String s) {
        label = s;
    }

    public String getLabel() {
        return label;
    }
}
