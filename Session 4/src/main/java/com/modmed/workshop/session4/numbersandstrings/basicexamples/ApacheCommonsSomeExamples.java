package com.modmed.workshop.session4.numbersandstrings.basicexamples;

import org.apache.commons.lang3.StringUtils;

public class ApacheCommonsSomeExamples {
    public static void main(String[] args) {
        // Some common used methods of ApacheCommonsSomeExamples StringUtils
        String sample = StringUtils.abbreviate("And after a long time, he finally met his son.", "..", 10);
        System.out.println(sample);
        String missing = StringUtils.appendIfMissing(sample, " And daughter");
        System.out.println(missing);
        String centering = StringUtils.center(missing, 50, '-');
        System.out.println(centering);
        // And SO much more
    }
}
