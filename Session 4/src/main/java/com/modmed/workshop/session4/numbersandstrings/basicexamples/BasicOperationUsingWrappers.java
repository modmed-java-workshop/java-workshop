package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class BasicOperationUsingWrappers {
    public static void main(String[] args) {
        Integer currentUfClp = Integer.valueOf(30000);
        System.out.println("25000 pesos son UF: " + UfClpUtilWrapper.clpToUf(25000, currentUfClp));
        System.out.println("35000 pesos son UF: " + UfClpUtilWrapper.clpToUf(35000, currentUfClp));
        System.out.println("3.5 UF son pesos: " + UfClpUtilWrapper.ufToClp(3.5, currentUfClp));
    }
}

class UfClpUtilWrapper {
    static double clpToUf(Integer clpAmount, Integer currentUfClp) {
        return clpAmount / currentUfClp.doubleValue();
    }
    static int ufToClp(Double ufAmount, Integer currentClp) {
        return ufAmount.intValue() * currentClp;
    }
}