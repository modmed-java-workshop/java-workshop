package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class CountMatcher {
    public static void main(String[] args) {
        char letter = 'r';
        String word = "caucasian";
        System.out.println("Occurrences of a char: " + letter + " within this word (String): " + word + ", are: " + countMatches(word, letter));
    }

    private static int countMatches(final String str, final char ch) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (ch == str.charAt(i)) {
                count++;
            }
        }
        return count;
    }
}
