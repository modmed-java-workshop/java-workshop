package com.modmed.workshop.session4.numbersandstrings.basicexamples;

public class NumberInitialization {
    public static void main(String[] args) {
        Long longObject = 999999L;
        Integer integerObject = new Integer(65665);
        Short shortObject = new Short((short) 32767);
        Byte byteObject = new Byte((byte) 126);

        byte primitiveByte = byteObject.byteValue();

        Float floatObject = 9.0f;
        Double doubleObject = 999999.0; // Can put a d suffix

        System.out.println(
                "*********** \n" +
                "Long = " + longObject + "\n" +
                "Is this long object at its maximum value (Max value: " + Long.MAX_VALUE + " ) ? Answer is " + longObject.equals(Long.MAX_VALUE) + "\n" +
                "*********** \n" +
                "Integer = " + integerObject + "\n" +
                "Is this integer object at its maximum value (Max value: " + Integer.MAX_VALUE + " ) ? Answer is " + integerObject.equals(Integer.MAX_VALUE) + "\n" +
                "*********** \n" +
                "Short = " + shortObject + "\n" +
                "Is this short object at its maximum value  (Max value: " + Short.MAX_VALUE + " ) ? Answer is " + shortObject.equals(Short.MAX_VALUE) + "\n" +
                "*********** \n" +
                "Byte = " + byteObject + "\n" +
                "Is this byte object at its maximum value  (Max value: " + Byte.MAX_VALUE + " ) ? ? Answer is " + byteObject.equals(Byte.MAX_VALUE) +"\n" +
                "*********** \n" +
                "Float = " + floatObject + "\n" +
                "*********** \n" +
                "Double = " + doubleObject);
    }
}

