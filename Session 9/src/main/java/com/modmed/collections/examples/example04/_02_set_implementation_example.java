package com.modmed.collections.examples.example04;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class _02_set_implementation_example {

    public static void main(String[] args) {
        System.out.println("HashSet initilized in constructor with 3 numbers and null: 1, 3, 2, null");
        HashSet<Integer> hashSet = new HashSet<>(Arrays.asList(1, 3, 2, null));

        System.out.println("HashSet doesn't maintain order:");
        System.out.println(hashSet);

        System.out.println("LinkedHashSet initilized in constructor with 3 numbers and null: 1, 3, 2, null");
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>(Arrays.asList(1, 3, 2, null));

        System.out.println("LinkedHashSet does maintain order:");
        System.out.println(linkedHashSet);

        System.out.println("TreeSet initilized in constructor with 3 numbers and null: 1, 3, 2");
        TreeSet<Integer> treeSet = new TreeSet<>(Arrays.asList(1, 3, 2));

        System.out.println("TreeSet orders collection:");
        System.out.println(treeSet);

        System.out.println("TreeSet does not accept null:");
        treeSet.add(null);
    }
}
