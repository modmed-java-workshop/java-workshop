package com.modmed.collections.examples.example02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class _03_array_list_parallel {
    public static void main(String[] args) throws InterruptedException {

        List<String> testList = Arrays.asList(new String[100]);

        //ways to go through
        long s = Calendar.getInstance().getTimeInMillis();
        for (int i = 0; i < testList.size(); i++) {
            Thread.sleep(1);
        }
        System.out.println("simple for with index ---> " +(Calendar.getInstance().getTimeInMillis() - s));

        s = Calendar.getInstance().getTimeInMillis();
        for (String innerValue : testList) {
            Thread.sleep(1);
        }
        System.out.println("for without index ---> " +(Calendar.getInstance().getTimeInMillis() - s));


        s = Calendar.getInstance().getTimeInMillis();
        testList.stream().forEach(innerValue -> {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("for each stream ---> " +(Calendar.getInstance().getTimeInMillis() - s));

        s = Calendar.getInstance().getTimeInMillis();
        testList.parallelStream().forEach(innerValue -> {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("for each parallel stream ---> " +(Calendar.getInstance().getTimeInMillis() - s));
    }
}
