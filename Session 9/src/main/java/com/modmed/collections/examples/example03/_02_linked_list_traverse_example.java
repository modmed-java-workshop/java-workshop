package com.modmed.collections.examples.example03;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

public class _02_linked_list_traverse_example {

    public static void main(String[] args) {
        System.out.println("LinkedList initilized in constructor with 5 numbers: ");
        LinkedList<Integer> linkedList = new LinkedList<>(Arrays.asList(1, 2, 3, 4, 5));
        System.out.println(linkedList);

        System.out.println("Using for loop: ");
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.println(linkedList.get(i));
        }

        System.out.println("Using foreach loop: ");
        for (Integer element : linkedList) {
            System.out.println(element);
        }

        System.out.println("Using iterator: ");
        Iterator<Integer> iterator = linkedList.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("Using stream foreach loop: ");
        linkedList.stream()
                .forEach(innerValue -> System.out.println(innerValue));


        System.out.println("Using parallel stream foreach loop: ");
        linkedList.parallelStream()
                .forEach(innerValue -> System.out.println(innerValue));
    }
}
