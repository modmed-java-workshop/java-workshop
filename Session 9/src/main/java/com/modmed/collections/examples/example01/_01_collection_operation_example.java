package com.modmed.collections.examples.example01;

import java.util.ArrayList;
import java.util.Collection;

public class _01_collection_operation_example {

    public static void main(String[] args) {
        Collection<Integer> numbers = new ArrayList<>();

        // add numbers to collection:
        System.out.println("Adding numbers 1, 2 and 3 to collection:");
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);

        System.out.println(numbers);

        // Check how many elements does the collection have:
        int size = numbers.size();
        System.out.println("Check the size of the collection: " + size);

        // Remove an element from the collection:
        System.out.println("Removing element 2 from collection:");
        numbers.remove(2);
        System.out.println(numbers);

        // Check the new size of the collection:
        size = numbers.size();
        System.out.println("Check the new size of the collection: " + size);

        System.out.println("Adding duplicate to collection:");
        numbers.add(3);
        System.out.println(numbers);

        // Check if collection is empty:
        System.out.println("Is the collection empty? " + numbers.isEmpty());

        // Removing all elements from collection:
        System.out.println("Removing all elements from collection.");
        numbers.clear();

        // Check if collection is empty:
        System.out.println("Is the collection empty? " + numbers.isEmpty());
    }
}
