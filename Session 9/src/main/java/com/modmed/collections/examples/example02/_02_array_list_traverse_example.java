package com.modmed.collections.examples.example02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class _02_array_list_traverse_example {

    public static void main(String[] args) {
        System.out.println("Array List initilized in constructor with 5 numbers: ");
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        System.out.println(arrayList);

        System.out.println("Using for loop: ");
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        System.out.println("Using foreach loop: ");
        for (Integer element : arrayList) {
            System.out.println(element);
        }

        System.out.println("Using iterator: ");
        Iterator<Integer> iterator = arrayList.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("Using stream foreach loop: ");
        arrayList.stream()
                .forEach(innerValue -> System.out.println(innerValue));


        System.out.println("Using parallel stream foreach loop: ");
        arrayList.parallelStream()
                .forEach(innerValue -> System.out.println(innerValue));
    }
}
