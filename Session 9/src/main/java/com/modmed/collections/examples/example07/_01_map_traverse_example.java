package com.modmed.collections.examples.example07;

import java.util.HashMap;
import java.util.Map;

public class _01_map_traverse_example {

    public static void main(String[] args) {
        Map<Integer, String> mapHttpErrors = new HashMap<>();
        mapHttpErrors.put(400, "Bad Request");
        mapHttpErrors.put(304, "Not Modified");
        mapHttpErrors.put(200, "OK");
        mapHttpErrors.put(301, "Moved Permanently");
        mapHttpErrors.put(500, "Internal Server Error");

        System.out.println(mapHttpErrors);

        System.out.println("Iterate over KeySet: ");
        for (Integer key : mapHttpErrors.keySet()) {
            System.out.println(key);
        }

        System.out.println("Iterate over Values: ");
        for (String value : mapHttpErrors.values()) {
            System.out.println(value);
        }

        System.out.println("Iterate over Values: ");
        for (Map.Entry<Integer, String> element : mapHttpErrors.entrySet()) {
            System.out.println("key: " + element.getKey() + " | value: " + element.getValue());
        }
    }
}
