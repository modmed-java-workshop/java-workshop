package com.modmed.collections.examples.example05;

import java.util.LinkedList;
import java.util.Queue;

public class _01_queue_example {

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        System.out.println("Adding 3 numbers to Queue: 1, 2 and 3");
        queue.add(1);
        queue.add(2);
        queue.add(3);

        System.out.println(queue);

        System.out.println("Using peek() to view head element without removing it: ");
        System.out.println(queue.peek());

        System.out.println("Show queue elements: ");
        System.out.println(queue);

        System.out.println("Using poll() to remove head element: ");
        System.out.println(queue.poll());

        System.out.println("Show queue elements: ");
        System.out.println(queue);

    }
}
