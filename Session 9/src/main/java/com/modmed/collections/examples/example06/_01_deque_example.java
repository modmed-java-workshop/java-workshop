package com.modmed.collections.examples.example06;

import java.util.ArrayDeque;
import java.util.Deque;

public class _01_deque_example {

    public static void main(String[] args) {
        Deque<String> deque = new ArrayDeque<>();
        System.out.println("Add element at tail using add(): ");
        deque.add("1 (Tail)");
        System.out.println(deque);

        System.out.println("Add element at head using addFirst(): ");
        deque.addFirst("Element 2 (Head)");
        System.out.println(deque);

        System.out.println("Add element at tail using addLast(): ");
        deque.addLast("Element 3 (Tail)");
        System.out.println(deque);

        System.out.println("Add element at head using push(): ");
        deque.push("Element 4 (Head)");
        System.out.println(deque);

        System.out.println("Add element at tail using offer(): ");
        deque.offer("Element 5 (Tail)");
        System.out.println(deque);

        System.out.println("Add element at head using offerFirst(): ");
        deque.offerFirst("Element 6 (Head)");
        System.out.println(deque);

        System.out.println("Remove element at head using removeFirst(): ");
        deque.removeFirst();
        System.out.println(deque);

        System.out.println("Remove element at tail using removeLast(): ");
        deque.removeLast();
        System.out.println(deque);

    }
}
