package com.modmed.collections.examples.example03;

import java.util.ArrayList;
import java.util.LinkedList;

public class _03_collection_performance_example {

    public static void main(String[] args) {
        ArrayList<Integer> arraylist = new ArrayList<>();
        LinkedList<Integer> linkedlist = new LinkedList<>();

        // arraylist add
        long startTime = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            arraylist.add(i);
        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        System.out.println("arraylist add:  " + nanoToSec(duration));

        // linkedlist add
        startTime = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            linkedlist.add(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("linkedlist add: " + nanoToSec(duration));

        // arraylist get
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            arraylist.get(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("arraylist get:  " + nanoToSec(duration));

        // linkedlist get
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            linkedlist.get(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("linkedlist get: " + nanoToSec(duration));


        // arraylist remove
        startTime = System.nanoTime();

        for (int i = 9999; i >=0; i--) {
            arraylist.remove(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("arraylist remove:  " + nanoToSec(duration));

        // linkedlist remove
        startTime = System.nanoTime();

        for (int i = 9999; i >=0; i--) {
            linkedlist.remove(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("linkedlist remove: " + nanoToSec(duration));
    }

    private static double nanoToSec(long nano) {
        return nano/1000000000.0;
    }
}
