package com.modmed.collections.examples.example02;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_array_list_example {

    public static void main(String[] args) {
        System.out.println("Array List initilized in constructor with 3 numbers: ");
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList("1", "2","3"));
        System.out.println(arrayList);

        System.out.println("Using .addAll() to add 3 more numbers: ");
        arrayList.addAll(Arrays.asList("4", "5", "6"));
        System.out.println(arrayList);

        System.out.println("Add an element at the end: ");
        arrayList.add("7");
        System.out.println(arrayList);

        System.out.println("Add an element in index 3: ");
        arrayList.add(3, "0");
        System.out.println(arrayList);

        System.out.println("Remove an element in index 1: ");
        arrayList.remove(1);
        System.out.println(arrayList);

        System.out.println("Replace an element in index 2: ");
        arrayList.set(2, "22");
        System.out.println(arrayList);
    }
}
