package com.modmed.collections.examples.example04;

import java.util.HashSet;
import java.util.Set;

public class _01_set_example {

    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();

        System.out.println("Adding 3 numbers to Set: 1, 3 and 2");
        set.add(1);
        set.add(3);
        set.add(2);

        System.out.println("Set doesn't maintain order:");
        System.out.println(set);

        System.out.println("Adding duplicate number 3:");
        set.add(3);

        System.out.println("Set doesn't duplicate number in collection:");
        System.out.println(set);

        System.out.println("Adding null to collection:");
        set.add(null);
        System.out.println(set);
    }
}
