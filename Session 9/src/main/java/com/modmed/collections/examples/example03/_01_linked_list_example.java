package com.modmed.collections.examples.example03;

import java.util.Arrays;
import java.util.LinkedList;

public class _01_linked_list_example {

    public static void main(String[] args) {
        System.out.println("LinkedList initilized in constructor with 3 numbers: ");
        LinkedList<Integer> linkedList = new LinkedList<>(Arrays.asList(1, 2, 3));
        System.out.println(linkedList);

        System.out.println("Using .addAll() to add 3 more numbers: ");
        linkedList.addAll(Arrays.asList(4, 5, 6));
        System.out.println(linkedList);

        System.out.println("Add an element at the first position: ");
        linkedList.addFirst(0);
        System.out.println(linkedList);

        System.out.println("Add an element at the last position: ");
        linkedList.addLast(99);
        System.out.println(linkedList);

        System.out.println("Add an element in index 3: ");
        linkedList.add(3, 0);
        System.out.println(linkedList);

        System.out.println("Remove an element in index 1: ");
        linkedList.remove(1);
        System.out.println(linkedList);

        System.out.println("Replace an element in index 2: ");
        linkedList.set(2, 22);
        System.out.println(linkedList);
    }
}
