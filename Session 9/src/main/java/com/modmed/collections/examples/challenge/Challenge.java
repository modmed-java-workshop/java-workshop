package com.modmed.collections.examples.challenge;

import java.util.Arrays;
import java.util.List;

public class Challenge {

    private static List<String> collection = Arrays.asList(new String[]{"I", "f", " ", "y", "o", "u", " ", "g", "e", "t", " ", "a", " ", "t", "e", "x", "t", " ", "m", "e", "s", "s", "a", "g", "e", " ", "f", "r", "o", "m", " ", "a", "n", " ", "e", "m", "a", "i", "l", " ", "a", "d", "d", "r", "e", "s", "s", " ", "o", "r", " ", "n", "u", "m", "b", "e", "r", " ", "y", "o", "u", " ", "d", "o", "n", "'", "t", " ", "r", "e", "c", "o", "g", "n", "i", "z", "e", ",", " ", "i", "t", "'", "s", " ", "p", "r", "o", "b", "a", "b", "l", "y", " ", "b", "e", "s", "t", " ", "t", "o", " ", "i", "g", "n", "o", "r", "e", " ", "i", "t"});

    public static void main(String[] args) {
        StringBuilder resp = new StringBuilder();

        singleLineAction(resp);

        System.out.println(resp);

        showUntil();
    }


    /**
     * Using the collection define on this class, reemplace all the letter 'a' with a letter 'A'
     * this action must be performed using just a single line code
     */
    private static void singleLineAction(StringBuilder resp) {
    }

    /**
     * print every word until found a comma on the response
     */
    private static void showUntil() {

    }
}
