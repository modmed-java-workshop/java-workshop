package com.modmed.collections.examples.example07;

import java.util.HashMap;
import java.util.Map;

public class _01_map_example {

    public static void main(String[] args) {
        Map<Integer, String> mapHttpErrors = new HashMap<>();
        mapHttpErrors.put(400, "Bad Request");
        mapHttpErrors.put(304, "Not Modified");
        mapHttpErrors.put(200, "OK");
        mapHttpErrors.put(301, "Moved Permanently");
        mapHttpErrors.put(500, "Internal Server Error");

        System.out.println(mapHttpErrors);

        System.out.println("Check if map contains key 500 and print value: ");
        if (mapHttpErrors.containsKey(500)) {
            System.out.println(mapHttpErrors.get(500));
        }

        System.out.println("Check if map contains a value 'OK': ");
        if (mapHttpErrors.containsValue("OK")) {
            System.out.println("Found status OK");
        }

        System.out.println("Removing element with key 500: ");
        String removedValue = mapHttpErrors.remove(500);
        if (removedValue != null) {
            System.out.println("Removed value: " + removedValue);
        }

        System.out.println("Print map elements: ");
        System.out.println(mapHttpErrors);

        System.out.println("Replace element with key 304: ");
        mapHttpErrors.replace(304, "No Changes");
        System.out.println("Print map elements: ");
        System.out.println(mapHttpErrors);

    }
}
