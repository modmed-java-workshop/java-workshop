package com.modmed.collections.examples.example05;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class _02_queue_implementation_example {

    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();

        System.out.println("Adding 3 numbers to LinkedList: 3, 1 and 2");
        linkedList.add(3);
        linkedList.add(1);
        linkedList.add(2);

        System.out.println("Show LinkedList elements: ");
        System.out.println(linkedList);

        System.out.println("polling element: " + linkedList.poll());
        System.out.println("Show LinkedList elements: ");
        System.out.println(linkedList);

        System.out.println("polling element: " + linkedList.poll());
        System.out.println("Show LinkedList elements: ");
        System.out.println(linkedList);

        System.out.println("polling element: " + linkedList.poll());
        System.out.println("Show LinkedList elements: ");
        System.out.println(linkedList);

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        System.out.println("Adding 3 numbers to PriorityQueue: 3, 1 and 2");
        priorityQueue.add(3);
        priorityQueue.add(1);
        priorityQueue.add(2);

        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue);

        System.out.println("polling element: " + priorityQueue.poll());
        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue);

        System.out.println("polling element: " + priorityQueue.poll());
        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue);

        System.out.println("polling element: " + priorityQueue.poll());
        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue);

        PriorityQueue<Integer> priorityQueue2 = new PriorityQueue<>(Comparator.reverseOrder());

        System.out.println("Adding 3 numbers to PriorityQueue: 3, 1 and 2");
        priorityQueue2.add(3);
        priorityQueue2.add(1);
        priorityQueue2.add(2);

        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue2);

        System.out.println("polling element: " + priorityQueue2.poll());
        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue2);

        System.out.println("polling element: " + priorityQueue2.poll());
        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue2);

        System.out.println("polling element: " + priorityQueue2.poll());
        System.out.println("Show PriorityQueue elements: ");
        System.out.println(priorityQueue2);

    }
}
