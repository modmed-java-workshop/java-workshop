package workshop.modmed.com.generics.examples.example06;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.domain.Patient;
import workshop.modmed.com.generics.domain.Person;
import java.util.ArrayList;
import java.util.List;

public class _01_wildcards_generics_substitution_problem {

    public static void main(String[] args) {

        Doctor doctor = new Doctor(1, "Jane Doe", 30);
        Patient patient = new Patient(1, "Bart Simpson", 15, 1);

        List<Person> people = new ArrayList<>();
        people.add(doctor);
        people.add(patient);

        print(people);

        List<Doctor> doctors = new ArrayList<>();
        doctors.add(doctor);

        // Cannot pass List<Doctor> to a method accepting List<Person>
        //print(doctors);
    }

    public static void print(List<Person> elements){
        for(Person o : elements){
            System.out.println(o);
        }
    }
}
