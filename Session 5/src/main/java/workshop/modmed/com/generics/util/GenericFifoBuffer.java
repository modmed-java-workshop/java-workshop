package workshop.modmed.com.generics.util;

import java.util.Arrays;

public class GenericFifoBuffer<T> {
    private T[] buffer;
    private int pointer;

    public GenericFifoBuffer(int size) {
        buffer = (T[]) new Object[size];
        this.pointer = 0;
    }

    public T get() {
        T o = buffer[0];
        if (pointer > 0) {
            this.buffer = Arrays.copyOfRange(buffer, 1, buffer.length + 1);
            pointer--;
        }

        return o;
    }

    public void put(T o) {
        if (pointer > buffer.length - 1) {
            throw new IndexOutOfBoundsException();
        }
        buffer[pointer] = o;
        pointer++;
    }

    public int countElements() {
        return pointer;
    }
}
