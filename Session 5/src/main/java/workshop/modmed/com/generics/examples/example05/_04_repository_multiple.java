package workshop.modmed.com.generics.examples.example05;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.domain.Patient;
import workshop.modmed.com.generics.repository.MemoryRepository;
import workshop.modmed.com.generics.repository.Repository;

public class _04_repository_multiple {

    public static void main(String[] args) {
        Repository<Doctor> doctorRepository = new MemoryRepository<>();
        Repository<Patient> patientRepository = new MemoryRepository<>();

        initDoctorRepository(doctorRepository);
        initPatientRepository(patientRepository);

        Doctor availableDoctor = getAvailableDoctor(doctorRepository);
        Patient newPatient = new Patient(patientRepository.count() + 1, "John Doe", 30, availableDoctor.getId());

        patientRepository.save(newPatient);

        System.out.println("Total patients: " + patientRepository.filter(d -> true));
    }

    private static void initDoctorRepository(Repository<Doctor> doctorRepository) {
        doctorRepository.save(new Doctor(1, "Jane Doe", 30));
        doctorRepository.save(new Doctor(2, "Mark Smith", 35));
        doctorRepository.save(new Doctor(3, "Paul Brown", 50));
        doctorRepository.save(new Doctor(4, "Mike West", 40));
    }

    private static void initPatientRepository(Repository<Patient> patientRepository) {
        patientRepository.save(new Patient(1, "Bart Simpson", 15, 1));
    }

    private static Doctor getAvailableDoctor(Repository<Doctor> doctorRepository) {
        return doctorRepository.getById(1);
    }
}
