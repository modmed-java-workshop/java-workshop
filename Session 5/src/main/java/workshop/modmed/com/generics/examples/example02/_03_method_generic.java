package workshop.modmed.com.generics.examples.example02;

import workshop.modmed.com.generics.util.GenericFifoBuffer;

public class _03_method_generic {

    public static void main(String[] args) {
        GenericFifoBuffer<String> buffer = new GenericFifoBuffer<>(5);

        buffer.put("he");
        buffer.put("ll");
        buffer.put("o!");


        String value  = concatenateBuffer(buffer);

        System.out.println(value);

        GenericFifoBuffer<Integer> buffer2 = new GenericFifoBuffer<>(5);

        buffer2.put(1);
        buffer2.put(2);
        buffer2.put(3);

        String value2 = concatenateBuffer(buffer2);

        System.out.println(value2);
    }

    private static <T> String concatenateBuffer(GenericFifoBuffer<T> buffer) {
        StringBuilder result = new StringBuilder();

        T value;
        while ((value = buffer.get()) != null)
        {
            result.append(value);
        }

        return result.toString();
    }
}
