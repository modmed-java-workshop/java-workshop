package workshop.modmed.com.generics.examples.example01;

import workshop.modmed.com.generics.util.GenericFifoBuffer;

public class _03_type_safe_generic {

    public static void main(String[] args) {
        GenericFifoBuffer<String> buffer = new GenericFifoBuffer<>(5);

        buffer.put("he");
        buffer.put("ll");
        buffer.put("o!");

        //buffer.put(1);

        String value  = concatenate(buffer);

        System.out.println(value);
    }

    private static String concatenate(GenericFifoBuffer<String> buffer) {
        StringBuilder result = new StringBuilder();

        String value;
        while ((value = buffer.get()) != null)
        {
            result.append(value);
        }

        return result.toString();
    }
}
