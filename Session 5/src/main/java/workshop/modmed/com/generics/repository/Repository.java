package workshop.modmed.com.generics.repository;

import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;

public interface Repository<T extends Identifiable & Serializable> {

    T getById(int id);

    void save(T element);

    void delete(T element);

    List<T> filter(Predicate<T> predicate);

    int count();
}
