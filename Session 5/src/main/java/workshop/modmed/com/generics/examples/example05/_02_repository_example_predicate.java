package workshop.modmed.com.generics.examples.example05;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.repository.MemoryRepository;
import workshop.modmed.com.generics.repository.Repository;
import java.util.List;
import java.util.function.Predicate;

public class _02_repository_example_predicate {

    public static void main(String[] args) {
        Repository<Doctor> doctorRepository = new MemoryRepository<>();
        doctorRepository.save(new Doctor(1, "Jane Doe", 30));
        doctorRepository.save(new Doctor(2, "Mark Smith", 35));
        doctorRepository.save(new Doctor(3, "Paul Brown", 50));
        doctorRepository.save(new Doctor(4, "Mike West", 40));

        List<Doctor> oldDoctors = doctorRepository.filter(isOlderThan(40));

        System.out.println(oldDoctors);

        List<Doctor> doctorResult = doctorRepository.filter(isOlderThan(30).and(nameStartsWith("M")));

        System.out.println(doctorResult);
    }

    public static Predicate<Doctor> isOlderThan(int age) {
        return d -> d.getAge() > age;
    }

    public static Predicate<Doctor> nameStartsWith(String prefix) {
        return d -> d.getName().startsWith(prefix);
    }
}
