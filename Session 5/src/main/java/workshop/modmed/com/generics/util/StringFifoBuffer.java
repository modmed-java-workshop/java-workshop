package workshop.modmed.com.generics.util;

import java.util.Arrays;

public class StringFifoBuffer {
    private String[] buffer;
    private int pointer;

    public StringFifoBuffer(int size) {
        buffer = new String[size];
        this.pointer = 0;
    }

    public String get() {
        String o = buffer[0];
        if (pointer > 0) {
            this.buffer = Arrays.copyOfRange(buffer, 1, buffer.length + 1);
            pointer--;
        }

        return o;
    }

    public void put(String o) {
        if (pointer > buffer.length - 1) {
            throw new IndexOutOfBoundsException();
        }
        buffer[pointer] = o;
        pointer++;
    }

    public int countElements() {
        return pointer;
    }
}
