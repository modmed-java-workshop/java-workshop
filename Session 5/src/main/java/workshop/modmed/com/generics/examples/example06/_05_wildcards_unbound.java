package workshop.modmed.com.generics.examples.example06;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.domain.Patient;
import workshop.modmed.com.generics.domain.Person;
import java.util.Arrays;
import java.util.List;

public class _05_wildcards_unbound {

    public static void main(String[] args) {

        Doctor doctor = new Doctor(1, "Jane Doe", 30);
        Patient patient = new Patient(1, "Bart Simpson", 15, 1);

        List<Person> people = Arrays.asList(doctor, patient);
        printNames(people);

        List<Object> objects = Arrays.asList(new Object(), 1, "String", doctor);
        printNames(objects);
    }

    public static void printNames(List<?> elements){
        for(Object o : elements){
            System.out.println(o);
        }
    }
}
