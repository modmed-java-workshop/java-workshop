package workshop.modmed.com.generics.examples.example06;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.domain.Patient;
import workshop.modmed.com.generics.domain.Person;
import java.util.ArrayList;
import java.util.List;

public class _04_wildcards_lower_bound {

    public static void main(String[] args) {

        Doctor doctor = new Doctor(1, "Jane Doe", 30);
        Patient patient = new Patient(1, "Bart Simpson", 15, 1);

        List<Person> people = new ArrayList<>();

        appendToList(people, doctor, patient);

        printNames(people);
    }

    public static void appendToList(List<? super Person> elements, Person... people){
        for (Person p : people) {
            elements.add(p);
        }
    }

    public static void printNames(List<? extends Person> elements){
        for(Person o : elements){
            System.out.println(o);
        }
    }
}
