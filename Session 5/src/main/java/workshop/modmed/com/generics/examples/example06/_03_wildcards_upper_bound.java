package workshop.modmed.com.generics.examples.example06;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.domain.Patient;
import workshop.modmed.com.generics.domain.Person;
import java.util.Arrays;
import java.util.List;

public class _03_wildcards_upper_bound {

    public static void main(String[] args) {

        Doctor doctor = new Doctor(1, "Jane Doe", 30);
        Patient patient = new Patient(1, "Bart Simpson", 15, 1);

        List<Person> people = Arrays.asList(doctor, patient);
        List<Doctor> doctors = Arrays.asList(doctor);

        printNames(doctors);
        printNames(people);
    }

    public static void printNames(List<? extends Person> elements){
        for(Person o : elements){
            System.out.println(o);
        }
    }

    /*
    // Alternative Way: Using Generic Method
    public static <T extends Person> void printNames2(List<T> elements){
        for(Person o : elements){
            System.out.println(o);
        }
    }
     */
}
