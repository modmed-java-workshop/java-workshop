package workshop.modmed.com.generics.examples.example01;

import workshop.modmed.com.generics.util.StringFifoBuffer;

public class _02_type_safe_copy {

    public static void main(String[] args) {
        StringFifoBuffer buffer = new StringFifoBuffer(5);

        buffer.put("he");
        buffer.put("ll");
        buffer.put("o!");

        //buffer.put(1);

        String value  = concatenate(buffer);

        System.out.println(value);
    }

    private static String concatenate(StringFifoBuffer buffer) {
        StringBuilder result = new StringBuilder();

        String value;
        while ((value = buffer.get()) != null)
        {
            result.append(value);
        }

        return result.toString();
    }
}
