package workshop.modmed.com.generics.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MemoryRepository<T extends Identifiable & Serializable> implements Repository<T> {
    private List<T> elements;

    public MemoryRepository() {
        this.elements = new ArrayList<>();
    }

    @Override
    public T getById(int id) {
        return elements.stream()
                .filter(e -> id == e.getId())
                .findFirst()
                .orElse(null);
    }

    @Override
    public void save(T element) {
        elements.add(element);
    }

    @Override
    public void delete(T element) {
        elements.remove(element);
    }

    @Override
    public List<T> filter(Predicate<T> predicate) {
        return elements.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public int count() {
        return elements.size();
    }
}
