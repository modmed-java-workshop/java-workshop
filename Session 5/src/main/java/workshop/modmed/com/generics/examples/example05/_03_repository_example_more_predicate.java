package workshop.modmed.com.generics.examples.example05;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.repository.MemoryRepository;
import workshop.modmed.com.generics.repository.Repository;
import workshop.modmed.com.generics.util.DoctorPredicate;
import java.util.function.Predicate;

public class _03_repository_example_more_predicate {

    public static void main(String[] args) {
        Repository<Doctor> doctorRepository = new MemoryRepository<>();
        doctorRepository.save(new Doctor(1, "Jane Doe", 30));
        doctorRepository.save(new Doctor(2, "Mark Smith", 35));
        doctorRepository.save(new Doctor(3, "Paul Brown", 50));
        doctorRepository.save(new Doctor(4, "Mike West", 40));

        Predicate<Doctor> olderThan45 = DoctorPredicate.isOlderThan(45);

        System.out.println("Older than 45: " + doctorRepository.filter(olderThan45));

        System.out.println("Younger than 45: " + doctorRepository.filter(olderThan45.negate()));

        System.out.println("Younger than 45 and Name starts with 'M' : " +
                doctorRepository.filter(olderThan45.negate().and(DoctorPredicate.nameStartsWith("M"))));
    }
}
