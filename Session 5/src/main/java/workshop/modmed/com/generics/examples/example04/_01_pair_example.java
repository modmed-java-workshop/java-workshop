package workshop.modmed.com.generics.examples.example04;

import workshop.modmed.com.generics.util.Pair;
import java.util.ArrayList;
import java.util.List;

public class _01_pair_example {

    public static void main(String[] args) {
        Pair<Integer, String> idPair = new Pair<>(1234567, "Jane Doe");
        Pair<String, String> runPair = new Pair<>("1234567-K", "Jane Doe");

        System.out.println(idPair);
        System.out.println(runPair);

        List<Pair<Integer, String>> optionList = new ArrayList<>();
        optionList.add(new Pair<>(1, "Option 1"));
        optionList.add(new Pair<>(2, "Option 2"));
        optionList.add(new Pair<>(3, "Option 3"));

        for (Pair<Integer, String> option : optionList) {
            System.out.println(option);
        }
    }
}
