package workshop.modmed.com.generics.examples.example03;

import workshop.modmed.com.generics.domain.Doctor;
import java.util.HashSet;
import java.util.Set;

public class _02_set_example {

    public static void main(String[] args) {
        Doctor emmettBrown = new Doctor(1, "Emmet Brown", 50);
        Doctor doctorStrange = new Doctor(2, "Stephen Strange", 36);

        Set<Doctor> doctors = new HashSet<>();
        doctors.add(emmettBrown);
        doctors.add(doctorStrange);

        System.out.println(doctors);

        // auto resize:
        doctors.add(new Doctor(3, "Doctor Who", 100));

        // Type Safe Compile Error:
        //doctors.add(new Object());

        System.out.println(doctors);

        // We can query for size
        System.out.println(doctors.size());

        // We can check if an object exists in collection
        System.out.println("Is Doctor Strange in Collection? " + doctors.contains(doctorStrange));
        doctors.remove(doctorStrange);
        System.out.println("Is Doctor Strange in Collection? " + doctors.contains(doctorStrange));

        // Cannot add duplicates
        System.out.println("Size before adding duplicate: " + doctors.size());
        doctors.add(emmettBrown);
        System.out.println("Size after adding duplicate: " + doctors.size());

        // can loop over them with a for loop - no order
        for (Doctor doc : doctors) {
            System.out.println(doc);
        }
    }
}
