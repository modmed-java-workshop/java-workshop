package workshop.modmed.com.generics.examples.example04;

import workshop.modmed.com.generics.domain.Doctor;
import java.util.HashMap;
import java.util.Map;

public class _02_map_example {

    public static void main(String[] args) {
        Doctor emmettBrown = new Doctor(1, "Emmet Brown", 50);
        Doctor doctorStrange = new Doctor(2, "Stephen Strange", 36);
        Doctor doctorWho = new Doctor(3, "Doctor Who", 100);

        Map<Integer, Doctor> doctors = new HashMap<>();
        doctors.put(emmettBrown.getId(), emmettBrown);
        doctors.put(doctorStrange.getId(), doctorStrange);
        doctors.put(doctorWho.getId(), doctorWho);

        // get by Key value
        System.out.println("Doctor with ID = 1 : " + doctors.get(1));

        // foreach key
        for (Integer id : doctors.keySet()) {
            System.out.println(id);
        }

        // foreach value
        for (Doctor doc : doctors.values()) {
            System.out.println(doc);
        }

        // foreach entry - again two generic parameters
        for (Map.Entry<Integer, Doctor> entry : doctors.entrySet()) {
            System.out.println(entry);
        }
    }
}
