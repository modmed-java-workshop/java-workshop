package workshop.modmed.com.generics.repository;

public interface Identifiable {
    int getId();
    void setId(int id);
}
