package workshop.modmed.com.generics.domain;

import workshop.modmed.com.generics.repository.Identifiable;
import java.io.Serializable;
import java.util.Date;

public class Drug implements Identifiable, Serializable {
    private int id;
    private String name;
    private Date expirationDate;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || getClass() != o.getClass()) return false;

        Drug drug = (Drug) o;

        return getId() == drug.getId();
    }

    @Override
    public int hashCode()
    {
        return new Integer(getId()).hashCode();
    }

    @Override
    public String toString()
    {
        return "Drug{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", expirationDate=" + getExpirationDate().toString() +
                '}';
    }
}
