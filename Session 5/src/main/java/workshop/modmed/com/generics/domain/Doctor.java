package workshop.modmed.com.generics.domain;

public class Doctor extends Person {

    public Doctor(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String toString()
    {
        return "Doctor{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}