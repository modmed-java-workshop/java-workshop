package workshop.modmed.com.generics.util;

import java.util.Arrays;

public class FifoBuffer {
    private Object[] buffer;
    private int pointer;

    public FifoBuffer(int size) {
        buffer = new Object[size];
        this.pointer = 0;
    }

    public Object get() {
        Object o = buffer[0];
        if (pointer > 0) {
            this.buffer = Arrays.copyOfRange(buffer, 1, buffer.length + 1);
            pointer--;
        }

        return o;
    }

    public void put(Object o) {
        if (pointer > buffer.length - 1) {
            throw new IndexOutOfBoundsException();
        }
        buffer[pointer] = o;
        pointer++;
    }

    public int countElements() {
        return pointer;
    }
}
