package workshop.modmed.com.generics.util;

import workshop.modmed.com.generics.domain.Doctor;
import java.util.function.Predicate;

public class DoctorPredicate {
    public static Predicate<Doctor> isOlderThan(int age) {
        return d -> d.getAge() > age;
    }

    public static Predicate<Doctor> nameStartsWith(String prefix) {
        return d -> d.getName().startsWith(prefix);
    }
}
