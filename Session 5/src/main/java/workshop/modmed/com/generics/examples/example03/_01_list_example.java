package workshop.modmed.com.generics.examples.example03;

import workshop.modmed.com.generics.domain.Doctor;
import java.util.ArrayList;
import java.util.List;

public class _01_list_example {

    public static void main(String[] args) {
        Doctor emmettBrown = new Doctor(1, "Emmet Brown", 50);
        Doctor doctorStrange = new Doctor(2, "Stephen Strange", 36);

        List<Doctor> doctors = new ArrayList<>();
        doctors.add(emmettBrown);
        doctors.add(doctorStrange);

        System.out.println(doctors);

        // auto resize:
        doctors.add(new Doctor(3, "Doctor Who", 100));

        // Type Safe Compile Error:
        //doctors.add(new Object());

        System.out.println(doctors);

        // We can query for size
        System.out.println(doctors.size());

        // Lists have order, we retrieve elements by index
        for (int i = 0; i < doctors.size(); i++) {
            // Generic get method
            Doctor doc = doctors.get(i);
            System.out.println(doc);
        }

        // can loop over them with a for loop
        for (Doctor doc : doctors) {
            System.out.println(doc);
        }

        // Can still add duplicates though
        doctors.add(emmettBrown);
        System.out.println(doctors);
    }
}
