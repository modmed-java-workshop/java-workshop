package workshop.modmed.com.generics.examples.example05;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.repository.MemoryRepository;
import workshop.modmed.com.generics.repository.Repository;

public class _01_repository_example {

    public static void main(String[] args) {
        Repository<Doctor> doctorRepository = new MemoryRepository<>();
        doctorRepository.save(new Doctor(1, "Jane Doe", 30));
        doctorRepository.save(new Doctor(2, "Mark Smith", 35));
        doctorRepository.save(new Doctor(3, "Paul Brown", 50));
        doctorRepository.save(new Doctor(4, "Mike West", 40));

        System.out.println("Total number of doctors: " + doctorRepository.count());

        Doctor jDoe = doctorRepository.getById(1);

        System.out.println("Doctor with ID = 1 : " + jDoe);

        // delete doctor
        doctorRepository.delete(new Doctor(3, "Paul Brown", 50));

        System.out.println("Total number of doctors: " + doctorRepository.count());

        // filter:
        System.out.println("Total doctors: " + doctorRepository.filter(d -> true));

        System.out.println("Total doctors older than 30: " + doctorRepository.filter(d -> d.getAge() > 30));

        System.out.println("Total doctors name starts with 'M': " + doctorRepository.filter(d -> d.getName().startsWith("M")));
    }
}
