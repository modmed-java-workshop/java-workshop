package workshop.modmed.com.generics.examples.example06;

import workshop.modmed.com.generics.domain.Doctor;
import workshop.modmed.com.generics.domain.Patient;
import workshop.modmed.com.generics.domain.Person;

public class _02_wildcards_covariant_problem {

    public static void main(String[] args) {

        Doctor doctor = new Doctor(1, "Jane Doe", 30);
        Patient patient = new Patient(1, "Bart Simpson", 15, 1);

        Person[] people = new Person[2];
        people[0] = doctor;
        people[1] = patient;

        print(people);

        Doctor[] doctors = new Doctor[1];
        doctors[0] = doctor;

        // With Arrays it is possible to pass a Doctor[] to a method accepting Person[]
        print(doctors);

        // just like we can assign a Doctor to a Person, we can assign an array Doctor[] to an array Person[]
        //Person p = new Person(1, "person name", 22);
        //Doctor d = new Doctor(1, "doctor name", 33);
        //p = d;
        people = doctors;

        print(people);

        Doctor[] doctors2 = new Doctor[2];
        Person[] doctorsAsPerson = doctors2;
        doctorsAsPerson[0] = doctor;
        // Runtime Exception!! But compiler lets us assign a patient in a list of doctors!
        doctorsAsPerson[1] = patient;

        print(doctorsAsPerson);
    }

    public static void print(Person[] elements){
        for(Person o : elements){
            System.out.println(o);
        }
    }
}
