package workshop.modmed.com.generics.domain;

public class Patient extends Person {
    private int doctorId;

    public Patient(int id, String name, int age, int doctorId) {
        super(id, name, age);
        this.doctorId = doctorId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public String toString()
    {
        return "Patient{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", doctorId=" + getDoctorId() +
                '}';
    }
}