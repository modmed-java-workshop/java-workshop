package workshop.modmed.com.generics.examples.example01;

import workshop.modmed.com.generics.util.FifoBuffer;

public class _01_type_unsafe {

    public static void main(String[] args) {
        FifoBuffer buffer = new FifoBuffer(5);

        buffer.put("he");
        buffer.put("ll");
        buffer.put("o!");

        //buffer.put(1);

        String value  = concatenate(buffer);

        System.out.println(value);
    }

    private static String concatenate(FifoBuffer buffer) {
        StringBuilder result = new StringBuilder();

        String value;
        while ((value = (String) buffer.get()) != null)
        {
            result.append(value);
        }

        return result.toString();
    }
}
