package com.modmed.workshop.example5;

import java.time.LocalDate;

public class CustomException extends Exception {

    private final String message;
    private final LocalDate date;

    public CustomException(String message, LocalDate date) {

        this.message = message;
        this.date = date;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public LocalDate getDate() {
        return date;
    }
}
