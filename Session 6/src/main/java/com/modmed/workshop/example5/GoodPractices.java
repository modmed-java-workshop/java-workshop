package com.modmed.workshop.example5;

import java.time.LocalDate;

public class GoodPractices {
    public static void main(String[] args) {
        try {
            foo();
        } catch (IllegalArgumentException e) {
            System.out.println("Ilegal argument");
        } catch (CustomException e) {
            System.out.println(e.getMessage() + " - " + e.getDate());
        } finally {
            System.out.println("Always works");
        }
    }

    private static void foo() throws CustomException {
        try {
            bar();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new CustomException("Did not work " + e.getMessage(), LocalDate.now());
        }
    }

    private static void bar() {
        try {
            throwException();
        } catch (NullPointerException e) {
            System.out.println("Null!");
        }
    }

    private static void throwException() {
        throw new ArrayIndexOutOfBoundsException("ups");
    }
}
