package com.modmed.workshop.example1;

public class UseJavaExceptions {

    public static void main(String[] args) {
        foo("Hello world!");
        String myString = null;
        foo(myString);
    }

    static void foo(String string) {
        try {
            System.out.println(string.length());
        } catch (NullPointerException e) {
            System.out.println("Please use non null strings");
        }
    }
}
