package com.modmed.workshop.example1;

public class Normal {

    public static void main(String[] args) {
        foo("Hello world!");
        String myString = null;
        foo(myString);
    }

    static void foo(String string) {
        System.out.println(string.length());
    }
}
