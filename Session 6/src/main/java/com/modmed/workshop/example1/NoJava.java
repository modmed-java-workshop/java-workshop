package com.modmed.workshop.example1;

public class NoJava {

    public static void main(String[] args) {
        foo("Hello world!");
        String myString = null;
        String foo = foo(myString);
        if (foo.equals("OK")) {
            System.out.println("All good");
        } else if (foo.equals("FAIL-NULL")) {
            System.out.println("Please use non null strings");
        }
    }

    static String foo(String string) {
        if (string != null) {
            System.out.println(string.length());
            return "OK";
        } else {
            return "FAIL-NULL";
        }
    }
}
