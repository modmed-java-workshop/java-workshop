package com.modmed.workshop.example3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExceptionsClasses {
    public static void main(String[] args) {
        try {
            Files.readAllLines(Paths.get("/not_found.txt"));
        } catch (IOException e) {
            System.out.println("Error in " + e.getMessage());
        }
    }
}
