package com.modmed.workshop.example3;

public class ErrorClasses {
    public static void main(String[] args) {
        foo();
    }

    static void foo() {
        bar();
    }

    static void bar() {
        foo();
    }
}
