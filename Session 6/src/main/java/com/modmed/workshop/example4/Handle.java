package com.modmed.workshop.example4;


import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Handle {
    public static void main(String[] args) {
        try {
            foo();
        } catch (IllegalArgumentException e) {
            System.out.println("Ilegal argument");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Always works");
        }
    }

    private static void foo() throws Exception {
        try {
            bar();
        } catch (NotImplementedException e) {
            throw new Exception("Did not work" + e.getMessage());
        }
    }

    private static void bar() {
        try {
            throwException();
        } catch (NullPointerException e) {
            System.out.println("Null!");
        }
    }

    private static void throwException() {
        throw new NotImplementedException();
    }
}
