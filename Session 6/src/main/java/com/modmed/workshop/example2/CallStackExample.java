package com.modmed.workshop.example2;

public class CallStackExample {

    public static void main(String[] args) {
        foo(null);
    }

    static void foo(String string) {
        bar(string);
        System.out.println("All good");
    }

    static void bar(String string) {
        possibleNullException(string);
    }

    static void possibleNullException(String string) {
        System.out.println(string.length());
    }
}
