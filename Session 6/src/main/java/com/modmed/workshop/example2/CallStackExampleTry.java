package com.modmed.workshop.example2;

public class CallStackExampleTry {

    public static void main(String[] args) {
        foo(null);
    }

    static void foo(String string) {
        bar(string);
        System.out.println("All good");
    }

    static void bar(String string) {
        try {
            possibleNullException(string);
        } catch (NullPointerException e) {
            System.out.println("Null!");;
        }
    }

    static void possibleNullException(String string) {
        System.out.println(string.length());
    }
}
