package com.modmed.example01;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
    private Calculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new Calculator();
    }

    @Test
    public void shouldSum42And73() {
        long sum = calculator.sum(42, 73);

        assertEquals(42+73, sum);
    }
}