package com.modmed.mockito;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthentificationServiceTest {
    @Mock
    private Repository repository;

    @Spy
    @InjectMocks
    private AuthentificationService authentificationService;

    @Test(expected = UserValidationException.class)
    public void getUser_user_not_found() throws UserValidationException {
        doReturn(Optional.empty()).when(repository).get(anyInt());

        authentificationService.getUser(1);
    }

    @Test
    public void getUser_user_exits() throws UserValidationException {
        User user = mock(User.class);
        doReturn(Optional.of(user)).when(repository).get(anyInt());

        User foundUser = authentificationService.getUser(1);

        assertThat("Service return user when finds one",
                foundUser,
                is(user));
    }

    @Test
    public void singUp_not_save_if_exists() throws UserValidationException {
        User user = mock(User.class);
        doReturn(new ArrayList<>()).when(repository).search(anyString());

        authentificationService.singUp(user);

        verify(repository, times(1)).save(any());
    }

    @Test
    public void getUser_using_when() throws UserValidationException {
        User user = mock(User.class);
        doReturn(user).when(authentificationService).getUser(anyInt());
        //when(authentificationService.getUser(anyInt())).thenReturn(user);

        User foundUser = authentificationService.getUser(1);

        assertThat("Service return user when finds one",
                foundUser,
                is(user));
    }
}