package com.modmed.basic;

import java.time.LocalDate;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class MathTest {
    @Test
    public void abs_for_subtraction() {
        int a = 10;
        int b = 20;

        int result = Math.abs(a - b);

        Assert.assertEquals(10, result);
    }

    @Test
    public void max_intengers() {
        int a = 100;
        int b = 0;

        int result = Math.max(a, b);

        Assert.assertEquals(a, result);
    }

    /**
     * https://rules.sonarsource.com/java/RSPEC-1244?search=double
     */
    @Test
    @Ignore
    public void doubles_not_using_delta() {
        double d1 = 0;
        // d1 sum up to 0.8
        for (int i = 1; i <= 8; i++) {
            d1 += 0.1;
        }

        // d2 is 0.8 too
        double d2 = 0.1 * 8;

        System.out.println(d1);
        System.out.println(d2);

        Assert.assertTrue(d1 == d2);
    }

    @Test
    public void doubles_using_delta() {
        double d1 = 0;
        // d1 sum up to 0.8
        for (int i = 1; i <= 8; i++) {
            d1 += 0.1;
        }

        // d2 is 0.8 too
        double d2 = 0.1 * 8;

        System.out.println(d1);
        System.out.println(d2);
        // deltas 0.0000000000000000001 - 0.00000000001
        Assert.assertTrue(Math.abs(d1 - d2) < 0.00000000001);
        Assert.assertEquals(d1, d2, 0.00000000001);
    }

    @Test
    public void isBefore_future_date_fixed() {
        LocalDate now = LocalDate.now();
        LocalDate future = LocalDate.parse("2025-05-30");

        boolean isBefore = now.isBefore(future);

        Assert.assertTrue(isBefore);
    }

    @Test
    @Ignore
    public void isBefore_future_date_fixed_old_test() {
        LocalDate now = LocalDate.now();
        LocalDate future = LocalDate.parse("2020-05-30");

        boolean isBefore = now.isBefore(future);

        Assert.assertTrue(isBefore);
    }

    @Test
    public void isBefore_future_date_not_fixed() {
        LocalDate now = LocalDate.now();
        LocalDate future = LocalDate.now().plusDays(1);

        boolean isBefore = now.isBefore(future);

        Assert.assertTrue(isBefore);
    }

    @Test
    public void isBefore_all_dates_fixed() {
        LocalDate now = LocalDate.parse("2020-05-29");
        LocalDate future = LocalDate.parse("2020-05-30");

        boolean isBefore = now.isBefore(future);

        Assert.assertTrue(isBefore);
    }
}
