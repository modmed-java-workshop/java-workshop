package com.modmed.basic;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class FileTest {

    private static BufferedReader reader;

    @BeforeClass
    static public void beforeClass() throws URISyntaxException, IOException {
        System.out.println("Openning reader ...");
        reader = Files.newBufferedReader(Paths.get(FileTest.class.getClassLoader().getResource("dump").toURI()));
    }

    @AfterClass
    static public void afterClass() throws IOException {
        System.out.println("Closing reader ...");
        reader.close();
    }

    @Test
    public void isFile() throws IOException {
        System.out.println("running test 1");
        long lines = reader.lines().count();

        Assert.assertTrue(lines > 0);
    }

    @Test
    public void reader_not_null() throws IOException {
        System.out.println("running test 2");

        Assert.assertNotNull(reader);
    }
}
