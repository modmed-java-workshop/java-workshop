package com.modmed.basic;

import org.junit.Assert;
import org.junit.Test;


public class StringTest {

    @Test
    public void isEmpty_not_empty() {
        String message = "nice tests!";

        String substring = message.substring(1, 3);

        Assert.assertFalse(substring.isEmpty());
    }

    @Test
    public void substring_out_of_bounds_exception() {
        String message = "nice tests!";
        try {
            String substring = message.substring(1, 30);
            Assert.fail("This should not be executed");
        } catch (StringIndexOutOfBoundsException e) {
            Assert.assertTrue(true);
        }
    }

    @Test(expected = StringIndexOutOfBoundsException.class)
    public void substring_out_of_bounds_exception_using_test() {
        String message = "nice tests!";
        String substring = message.substring(1, 30);
    }
}
