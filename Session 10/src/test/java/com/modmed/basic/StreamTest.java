package com.modmed.basic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StreamTest {

    private static BufferedReader reader;
    private Stream<String> lines;


    @BeforeClass
    static public void beforeClass() throws URISyntaxException, IOException {
        System.out.println("Openning reader ...");
        reader = Files.newBufferedReader(Paths.get(FileTest.class.getClassLoader().getResource("dump").toURI()));
    }

    @AfterClass
    static public void afterClass() throws IOException {
        System.out.println("Closing reader ...");
        reader.close();
    }

    @Before
    public void before() {
        System.out.println("Setting lines ...");
        lines = reader.lines();
    }

    @After
    public void after() throws URISyntaxException, IOException {
        System.out.println("Cheking if closed ...");
        try {
            reader.ready();
        } catch (IOException e) {
            System.out.println("reader was closed, openning ...");
            reader = Files.newBufferedReader(Paths.get(FileTest.class.getClassLoader().getResource("dump").toURI()));
        }
    }

    @Test
    public void findFirst_has_one_element() {
        System.out.println("running test 1");
        Optional<String> first = lines.findFirst();

        MatcherAssert.assertThat(
                "Has one element",
                first.isPresent(),
                CoreMatchers.is(true)
        );
    }

    @Test
    public void findFirst_has_empty_lines() {
        System.out.println("running test 2");
        long count = lines.filter(line -> line.isEmpty()).count();

        MatcherAssert.assertThat(
                "Has empty lines",
                count,
                CoreMatchers.not(0)
        );
    }

    @Test(expected = UncheckedIOException.class)
    public void stream_closed() throws IOException {
        System.out.println("running test 3");
        reader.close();

        lines.findFirst();
    }
}
