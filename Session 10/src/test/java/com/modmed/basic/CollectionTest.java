package com.modmed.basic;

import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CollectionTest {

    private final Set<Integer> set = new HashSet<>();

    @Before
    public void after() {
        System.out.println("Filling ...");
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
    }

    @After
    public void before() {
        System.out.println("Clearing ...");
        set.clear();
    }

    @Test
    public void add_new_element() {
        System.out.println("running test 1");
        int initial = set.size();

        set.add(5);

        Assert.assertEquals(initial + 1, set.size());
    }

    @Test
    public void add_duplicated_element() {
        System.out.println("running test 2");
        int initial = set.size();

        set.add(1);

        Assert.assertEquals(initial, set.size());
    }

    @Test
    public void contains() {
        System.out.println("running test 3");
        Assert.assertTrue(set.contains(1));
    }
}
