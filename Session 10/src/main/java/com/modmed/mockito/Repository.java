package com.modmed.mockito;

import java.util.List;
import java.util.Optional;

public interface Repository {
    void save(User user);

    List<User> search(String name);

    Optional<User> get(int id);
}
