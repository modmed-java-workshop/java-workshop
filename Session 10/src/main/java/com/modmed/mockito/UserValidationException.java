package com.modmed.mockito;

public class UserValidationException extends Exception {
    public UserValidationException(String message) {
        super(message);
    }
}
