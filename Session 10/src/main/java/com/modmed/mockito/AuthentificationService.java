package com.modmed.mockito;

import java.util.Optional;

public class AuthentificationService {
    private final Repository repository;

    public AuthentificationService(Repository repository) {
        this.repository = repository;
    }

    public void singUp(User user) throws UserValidationException {
        if (!repository.search(user.getName()).isEmpty()) {
            throw new UserValidationException(String.format("User [%s] already exists.", user.getName()));
        }
        repository.save(user);
    }

    public User getUser(int id) throws UserValidationException {
        Optional<User> user = repository.get(id);
        if (!user.isPresent()) {
            throw new UserValidationException(String.format("User [%s] does not exist.", id));
        }
        return user.get();
    }
}
