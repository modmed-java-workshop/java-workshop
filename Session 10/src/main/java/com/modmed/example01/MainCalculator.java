package com.modmed.example01;

public class MainCalculator {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        long sum = calculator.sum(42, 73);

        String message = sum == 42+73 ? "Calculator sum OK" : "Calculator sum Not OK";

        System.out.println("***** calculator.sum(42 + 73) *****");
        System.out.println(message);
    }
}
