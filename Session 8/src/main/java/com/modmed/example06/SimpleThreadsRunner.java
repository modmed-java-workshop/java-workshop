package com.modmed.example06;

import static com.modmed.example03.ThreadColor.ANSI_BLUE;

public class SimpleThreadsRunner {
    private final long patience;

    public SimpleThreadsRunner(long patience) {
        this.patience = patience;
    }

    public void runExercise() throws InterruptedException {
        ThreadUtil.threadMessage(ANSI_BLUE, "Starting MessageLoop thread");

        long startTime = System.currentTimeMillis();

        Thread t = new Thread(new MessageLoopRunnable());
        t.start();

        ThreadUtil.threadMessage(ANSI_BLUE, "Waiting for MessageLoop thread to finish");

        while (t.isAlive()) { // loop until MessageLoop thread exits
            ThreadUtil.threadMessage(ANSI_BLUE, "Still waiting...");

            t.join(1000); // Wait maximum of 1 second for MessageLoop thread to finish.

            if (isTimeExpired(patience, startTime) && t.isAlive()) {
                ThreadUtil.threadMessage(ANSI_BLUE, "Tired of waiting!");

                t.interrupt();

                // Shouldn't be long now -- wait indefinitely
                t.join();
            }
        }

        ThreadUtil.threadMessage(ANSI_BLUE, "Finally!");
    }

    private boolean isTimeExpired(long patience, long startTime) {
        return (System.currentTimeMillis() - startTime) > patience;
    }
}
