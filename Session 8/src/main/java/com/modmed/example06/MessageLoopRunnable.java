package com.modmed.example06;

import com.modmed.example03.ThreadColor;

class MessageLoopRunnable implements Runnable {
    private static final String[] IMPORTANT_INFO = new String[] {"Mares eat oats",
            "Does eat oats",
            "Little lambs eat ivy",
            "A kid will eat ivy too"};

    public void run() {
        try {
            for (String info : IMPORTANT_INFO) {
                Thread.sleep(4000); // Pause for 4 seconds

                ThreadUtil.threadMessage(ThreadColor.ANSI_RED,info); // Print a message
            }
        } catch (InterruptedException e) {
            ThreadUtil.threadMessage(ThreadColor.ANSI_RED,"I wasn't done!");
        }
    }
}
