package com.modmed.example06;

public class SimpleThreadsMain { // Adapted from Oracle Java Tutorial
    private static final int ONE_HOUR = 1000 * 60 * 60;

    public static void main(String[] args) throws InterruptedException {
        // Delay, in milliseconds before we interrupt MessageLoop thread (default one hour).
        long patience = getPatience(args);

        SimpleThreadsRunner simpleThreadsRunner = new SimpleThreadsRunner(patience);

        simpleThreadsRunner.runExercise();
    }

    private static long getPatience(String[] args) {
        long patience = ONE_HOUR;

        // If command line argument present, gives patience in seconds.
        if (args.length > 0) {
            try {
                patience = Long.parseLong(args[0]) * 1000;
            } catch (NumberFormatException e) {
                System.err.println("Argument must be an integer.");

                System.exit(1);
            }
        }

        return patience;
    }
}
