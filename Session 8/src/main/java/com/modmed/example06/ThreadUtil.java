package com.modmed.example06;

public class ThreadUtil {
    private ThreadUtil() {
    }

    // Display a message, preceded by the name of the current thread
    public static void threadMessage(String color, String message) {
        String threadName = Thread.currentThread().getName();

        System.out.format("%s%s: %s%n", color, threadName, message);
    }
}
