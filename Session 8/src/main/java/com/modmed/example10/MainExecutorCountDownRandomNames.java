package com.modmed.example10;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import com.github.javafaker.Faker;
import com.modmed.example03.ThreadColor;

public class MainExecutorCountDownRandomNames {
    private final LinkedList<String> colors = new LinkedList<>();

    public MainExecutorCountDownRandomNames() {
        initColors();
    }

    private void initColors() {
        colors.add(ThreadColor.ANSI_PURPLE);
        colors.add(ThreadColor.ANSI_GREEN);
        colors.add(ThreadColor.ANSI_CYAN);
        colors.add(ThreadColor.ANSI_BLUE);
        colors.add(ThreadColor.ANSI_RED);
        colors.add(ThreadColor.ANSI_YELLOW);
    }

    public static void main(String[] args) {
        new MainExecutorCountDownRandomNames().doRunThreads();
    }

    private void doRunThreads() {
        AtomicInteger atomicInteger = new AtomicInteger(10000);
        List<Callable<CountResultDTO>> tasks = new ArrayList<>();

        for (int i = 1; i <= 20; i++) {
            CountDownCallable callable = new CountDownCallable(i + "-" + getRandomName(), nextColor(i), atomicInteger);
            callable.setDoSleep(false);

            tasks.add(callable);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(20);

        try {
            List<Future<CountResultDTO>> futures = executorService.invokeAll(tasks);

            List<CountResultDTO> resultDTOS = collectResults(futures);

            resultDTOS.sort(Comparator.comparing(CountResultDTO::getTimesPrinted));

            printReport(resultDTOS);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        executorService.shutdown();
    }

    private String getRandomName() {
        return new Faker().funnyName()
                .name();
    }

    private String nextColor(int i) {
        return colors.get(i % colors.size());
    }

    private List<CountResultDTO> collectResults(List<Future<CountResultDTO>> futures) throws InterruptedException, ExecutionException {
        List<CountResultDTO> resultDTOS = new ArrayList<>();

        for (Future<CountResultDTO> futureResultDto : futures) {
            resultDTOS.add(futureResultDto.get());
        }

        return resultDTOS;
    }

    private void printReport(List<CountResultDTO> resultDTOS) {
        System.err.printf("%s***********************************%n", ThreadColor.ANSI_RESET);
        System.err.println("*** RESULTS FROM main() method ***");
        for (CountResultDTO resultDTO : resultDTOS) {
            System.err.format("%-5d %s%n", resultDTO.getTimesPrinted(), resultDTO.getThreadName());
        }
        System.err.println("***********************************");
    }
}
