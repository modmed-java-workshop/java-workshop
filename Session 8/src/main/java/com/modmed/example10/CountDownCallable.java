package com.modmed.example10;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public class CountDownCallable implements Callable<CountResultDTO> {
    private final String name;
    private final String color;
    private final AtomicInteger atomicInteger;

    private boolean doSleep;

    public CountDownCallable(String name, String color, AtomicInteger atomicInteger) {
        this.name = name;
        this.color = color;
        this.atomicInteger = atomicInteger;
    }

    @Override
    public CountResultDTO call() {
        int printCounter = 0;

        while (true) {
            int number = atomicInteger.getAndDecrement();

            if (number > 0) {
                printNumber(number);
                printCounter++;

                if (doSleep) {
                    sleep(1000);
                }
            } else {
                return new CountResultDTO(this.name, printCounter);
            }
        }
    }

    private void printNumber(int number) {
        String numberMessage = String.format("%s%-35s__ %d", color, name, number);

        System.out.println(numberMessage);
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setDoSleep(boolean doSleep) {
        this.doSleep = doSleep;
    }
}
