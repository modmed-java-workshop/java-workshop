package com.modmed.example10;

public class CountResultDTO {
    private String threadName;
    private int timesPrinted;

    public CountResultDTO(String threadName, int timesPrinted) {
        this.threadName = threadName;
        this.timesPrinted = timesPrinted;
    }

    public String getThreadName() {
        return threadName;
    }

    public int getTimesPrinted() {
        return timesPrinted;
    }
}
