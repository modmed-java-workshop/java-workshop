package com.modmed.challenge;

import java.io.IOException;
import java.net.URISyntaxException;

public class ChallengeMain {
    private static final FileSourceReader FILE_SOURCE_READER = new FileSourceReader();
    private static final FileDestinationWriter FILE_DESTINATION_WRITER = new FileDestinationWriter();

    public static void main(String[] args) throws IOException, URISyntaxException {
        FILE_SOURCE_READER.openFileRead();
        FILE_DESTINATION_WRITER.openFileWrite();

        while (FILE_SOURCE_READER.hasNext()) {
            NumberedLine numberedLine = FILE_SOURCE_READER.readLine();

            FILE_DESTINATION_WRITER.writeLine(numberedLine.getLineNumber() + " - " + numberedLine.getLine());
        }

        // T1(sourceReader, destinationWriter)
        // T2(sourceReader, destinationWriter)
        // T1.start()
        // T2.start()
        // T1.join()
        // T2.join()

        FILE_SOURCE_READER.closeFileRead();
        FILE_DESTINATION_WRITER.closeFileWrite();
    }
}
