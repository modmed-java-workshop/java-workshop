**Concurrency Challenge - Text obfuscation**

Write a Java program that performs the following: read text from a source input file
and "obfuscate" each text line by reversing their content. Then save the lines onto an output
file that will contain all the obfuscated lines from source input file (in any order)

`source file ----> obfuscation process -----> output file`

Create a class that will:
- call a reader class to obtain a new line from the input source file
- flip the text line
- put the thread name and the line number at the beginning of the line
- write the line onto an output file

You must implement:
- Create two threads with a name from this new class that read the file contents from 
a provided FileSourceReader class and write its contents against FileDestinationWriter.
- Apply proper synchronization to reader and writer classes' methods. 

In case of doubts, ask your teacher.
