package com.modmed.challenge;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class FileSourceReader {
    private final AtomicInteger lineNumber = new AtomicInteger();
    private Iterator<String> lineIterator;
    private Stream<String> linesStream;

    public void openFileRead() throws IOException, URISyntaxException {
        URL resource = requireNonNull(FileSourceReader.class.getResource("harry_potter.txt"), "Missing source file, try checking your implementation.");
        linesStream = Files.lines(Paths.get(resource.toURI()));
        lineIterator = linesStream.iterator();
    }

    public boolean hasNext() {
        return lineIterator.hasNext();
    }

    public NumberedLine readLine() {
        return new NumberedLine(lineNumber.incrementAndGet(), lineIterator.next());
    }

    public void closeFileRead() {
        linesStream.close();
    }
}
