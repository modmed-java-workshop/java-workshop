package com.modmed.challenge;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileDestinationWriter {
    private BufferedWriter writer;

    public void openFileWrite() throws IOException {
        writer = Files.newBufferedWriter(Paths.get("session10_challenge_official_output.txt"));
    }

    public void writeLine(String line) throws IOException {
        writer.write(line);
        writer.newLine();
    }

    public void closeFileWrite() throws IOException {
        writer.close();
    }
}
