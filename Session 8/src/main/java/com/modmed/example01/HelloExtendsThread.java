package com.modmed.example01;

public class HelloExtendsThread extends Thread {

    @Override
    public void run() { //overrides superclass method
        System.out.println("Hello from a thread! - extends Thread");
    }

}
