package com.modmed.example08;

import com.modmed.example07.ICounter;

public class CounterSync implements ICounter {
    private int counter = 0;

    public synchronized void increment() {
        counter++;
    }

    public synchronized void decrement() {
        counter--;
    }

    public int value() {
        return counter;
    }
}
