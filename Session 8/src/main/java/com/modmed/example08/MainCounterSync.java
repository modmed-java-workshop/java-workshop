package com.modmed.example08;

import com.modmed.example07.CounterRunnable;
import java.util.ArrayList;
import java.util.List;

public class MainCounterSync {
    public static void main(String[] args) {
        CounterSync counterSync = new CounterSync();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread(new CounterRunnable(counterSync));

            threads.add(thread);
        }

        startAll(threads);
        waitForAll(threads);

        System.out.println("Counter Sync value: " + counterSync.value());
    }

    private static void startAll(List<Thread> threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    public static void waitForAll(List<Thread> threads) {
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
