package com.modmed.example04;

import java.util.concurrent.atomic.AtomicInteger;

import com.modmed.example03.ThreadColor;

public class MainCountDown {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(20);

        CountDownRunnable pedro = new CountDownRunnable("2-pedro", ThreadColor.ANSI_GREEN, atomicInteger);
        CountDownRunnable juan = new CountDownRunnable("1-juan", ThreadColor.ANSI_RED, atomicInteger);
        CountDownRunnable diego = new CountDownRunnable("3-diego", ThreadColor.ANSI_BLUE, atomicInteger);

//        pedro.setDoSleep(true);
//        juan.setDoSleep(true);
//        diego.setDoSleep(true);

        new Thread(pedro).start();
        new Thread(juan).start();
        new Thread(diego).start();
    }
}
