package com.modmed.example04;

import java.util.concurrent.atomic.AtomicInteger;

public class CountDownRunnable implements Runnable {
    private final String name;
    private final String color;
    private final AtomicInteger atomicInteger;

    private boolean doSleep;

    public CountDownRunnable(String name, String color, AtomicInteger atomicInteger) {
        this.name = name;
        this.color = color;
        this.atomicInteger = atomicInteger;
    }

    @Override
    public void run() {
        while (true) {
            int number = atomicInteger.decrementAndGet();

            if (number > 0) {
                printNumber(number);

                if (doSleep) {
                    sleep(1000);
                }
            } else {
                return;
            }
        }
    }

    private void printNumber(int number) {
        String numberMessage = String.format("%s%-35s__ %d", color, name, number);

        System.out.println(numberMessage);
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setDoSleep(boolean doSleep) {
        this.doSleep = doSleep;
    }
}
