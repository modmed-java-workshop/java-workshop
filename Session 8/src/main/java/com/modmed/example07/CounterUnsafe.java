package com.modmed.example07;

public class CounterUnsafe implements ICounter {
    private int counter = 0;

    @Override
    public void increment() {
        counter++;
    }

    @Override
    public void decrement() {
        counter--;
    }

    @Override
    public int value() {
        return counter;
    }
}
