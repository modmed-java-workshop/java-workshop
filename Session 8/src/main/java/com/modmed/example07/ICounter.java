package com.modmed.example07;

public interface ICounter {
    void increment();

    void decrement();

    int value();
}
