package com.modmed.example07;

import java.util.ArrayList;
import java.util.List;

public class MainCounterUnsafe {
    public static void main(String[] args) {
        CounterUnsafe counterUnsafe = new CounterUnsafe();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread(new CounterRunnable(counterUnsafe));

            threads.add(thread);
        }

        startAll(threads);
        waitForAll(threads);

        System.out.println("Counter Unsafe value: " + counterUnsafe.value());
    }

    private static void startAll(List<Thread> threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    public static void waitForAll(List<Thread> threads) {
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
