package com.modmed.example07;

public class CounterRunnable implements Runnable {
    private static final int TIMES = 10000;

    private final ICounter someCounter;

    public CounterRunnable(ICounter someCounter) {
        this.someCounter = someCounter;
    }


    @Override
    public void run() {
        incrementAndDecrement();
    }

    private void incrementAndDecrement() {
        for (int i = 0; i < TIMES; i++) {
            someCounter.increment();
        }

        for (int i = 0; i < TIMES; i++) {
            someCounter.decrement();
        }

        System.out.println(Thread.currentThread().getName() +  "- I just finished");
    }
}
