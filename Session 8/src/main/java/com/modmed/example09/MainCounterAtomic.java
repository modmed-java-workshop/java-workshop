package com.modmed.example09;

import java.util.ArrayList;
import java.util.List;

import com.modmed.example07.CounterRunnable;

public class MainCounterAtomic {
    public static void main(String[] args) {
        CounterAtomic counterAtomic = new CounterAtomic();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread(new CounterRunnable(counterAtomic));

            threads.add(thread);
        }

        startAll(threads);
        waitForAll(threads);

        System.out.println("Counter Atomic value: " + counterAtomic.value());
    }

    private static void startAll(List<Thread> threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    public static void waitForAll(List<Thread> threads) {
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
