package com.modmed.example09;

import com.modmed.example07.ICounter;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterAtomic implements ICounter {
    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void increment() {
        counter.incrementAndGet();
    }

    @Override
    public void decrement() {
        counter.decrementAndGet();
    }

    @Override
    public int value() {
        return counter.intValue();
    }
}
