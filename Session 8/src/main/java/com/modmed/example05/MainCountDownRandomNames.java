package com.modmed.example05;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

import com.github.javafaker.Faker;
import com.modmed.example03.ThreadColor;
import com.modmed.example04.CountDownRunnable;

public class MainCountDownRandomNames {
    private final LinkedList<String> colors = new LinkedList<>();

    public MainCountDownRandomNames() {
        initColors();
    }

    private void initColors() {
        colors.add(ThreadColor.ANSI_PURPLE);
        colors.add(ThreadColor.ANSI_GREEN);
        colors.add(ThreadColor.ANSI_CYAN);
        colors.add(ThreadColor.ANSI_BLUE);
        colors.add(ThreadColor.ANSI_RED);
        colors.add(ThreadColor.ANSI_YELLOW);
    }

    public static void main(String[] args) {
        new MainCountDownRandomNames().doRunThreads();
    }

    private void doRunThreads() {
        AtomicInteger atomicInteger = new AtomicInteger(10000+1);

        for (int i = 1; i <= 20; i++) {
            CountDownRunnable runnable = new CountDownRunnable(i + "-" + getRandomName(), nextColor(i), atomicInteger);
            runnable.setDoSleep(false);
            Thread thread = new Thread(runnable);

            thread.start();
        }
    }

    private String getRandomName() {
        return new Faker().funnyName().name();
    }

    private String nextColor(int i) {
        return colors.get(i % colors.size());
    }
}
