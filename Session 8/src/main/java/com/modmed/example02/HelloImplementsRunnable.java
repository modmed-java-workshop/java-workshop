package com.modmed.example02;

public class HelloImplementsRunnable implements Runnable {

    @Override
    public void run() { //from interface
        System.out.println("Hello from a thread!");
    }

}
