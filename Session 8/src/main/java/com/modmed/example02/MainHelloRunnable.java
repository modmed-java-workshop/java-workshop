package com.modmed.example02;

public class MainHelloRunnable {

    public static void main(String[] args) {
        HelloImplementsRunnable runnable = new HelloImplementsRunnable();

        Thread thread = new Thread(runnable);

        thread.start();
    }

}
