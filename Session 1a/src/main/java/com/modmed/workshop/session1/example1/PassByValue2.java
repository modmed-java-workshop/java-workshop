package com.modmed.workshop.session1.example1;

class PassByValue2 {
    public static void main(String[] args) {
        Patient patient = new Patient("James");
        Patient samePatient = patient;

        // we pass the object reference as value
        foo(patient);
        // patient variable now is Vineet
        System.out.println(patient.getName()); // Vineet
        System.out.println(patient.equals(samePatient)); // still same patient
    }

    static void foo(Patient patient) {
        System.out.println(patient.getName()); // James
        patient.setName("Vineet");
        System.out.println(patient.getName()); // Vineet
    }
}
