package com.modmed.workshop.session1.example3;

public class Polymorphism3 {
    static class Person {
        protected void communicate() {
            System.out.println("I don't know who I am");
        }
    }

    static class Patient extends Person {
        @Override
        public void communicate() {
            System.out.println("I'm a Patient");
        }
    }

    static class Medic extends Person {
        @Override
        public void communicate() {
            System.out.println("I'm a Medic");
        }
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.communicate(); // I don't know who I am

        Patient patient = new Patient();
        patient.communicate(); // I'm a Patient

        Medic medic = new Medic();
        medic.communicate(); // I'm a Medic

        // what happens here?
        Person medicPerson = new Medic();
        medicPerson.communicate();
    }
}
