package com.modmed.workshop.session1.example6;

public class PcrSample {

    class PcrKit {
        private void open(){}
        /*
        public void open(){}
        protected void open(){}
        void open(){}
         */

        Swab getSwab(){return new Swab();}
    }

    class Sanitizer {
        void sanitize(Patient patient){}
    }

    class Swab {}

    class Patient {
        void insertSwabToRightNostril(Swab swab){}

        void insertSwabToLeftNostril(Swab swab){}

        void rotateNasalSwab(){}

        Swab getSwab(){return new Swab();}
    }

    PcrKit pcrKit = new PcrKit();
    Sanitizer sanitizer = new Sanitizer();

    void collectAnteriorNasal(Patient patient) {
        // step 1
        pcrKit.open();
        // step 2
        sanitizer.sanitize(patient);
        // step 3
        Swab newSwab = pcrKit.getSwab();
        // step 4
        patient.insertSwabToRightNostril(newSwab);
        // step 5
        patient.rotateNasalSwab();
        // step 6
        Swab usedSwab = patient.getSwab();
        // step 7
        patient.insertSwabToLeftNostril(usedSwab);
    }
}
