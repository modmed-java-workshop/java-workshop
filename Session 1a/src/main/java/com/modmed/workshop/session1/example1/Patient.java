package com.modmed.workshop.session1.example1;

class Patient {
    private String name;

    Patient(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Patient)) {
            return false;
        }
        return ((Patient) o).name.equals(this.name);
    }
}
