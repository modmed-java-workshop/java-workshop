package com.modmed.workshop.session1.example4;

public class Abstraction1 {
    abstract class Person {
        abstract void communicate();
    }

    class Patient extends Person {
        @Override
        public void communicate() {
            System.out.println("I'm a Patient");
        }
    }

    class Medic extends Person {
        @Override
        public void communicate() {
            System.out.println("I'm a Medic");
        }
    }
}
