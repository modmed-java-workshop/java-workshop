package com.modmed.workshop.session1.example5;

public class Interface1 {
    interface Person {
        void speak();
    }

    interface Payer {
        void pay();
    }

    class Patient implements Person, Payer {

        @Override
        public void speak() {
            System.out.println("I feel good!");
        }

        @Override
        public void pay() {
            System.out.println("not feeling good ...");
        }
    }
}
