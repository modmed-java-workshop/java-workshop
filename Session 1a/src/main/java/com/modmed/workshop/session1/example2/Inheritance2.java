package com.modmed.workshop.session1.example2;

public class Inheritance2 {
    static class Person {
        protected void communicate() {}
    }

    static class Patient extends Person {
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.communicate();
        // patient also communicates
        Patient patient = new Patient();
        patient.communicate();
        // patients are persons too!
        Person patientPerson = new Patient();
        patientPerson.communicate();
    }
}
