package com.modmed.workshop.session1.example4;

public class Abstraction2 {
    static abstract class Person {
        abstract void communicate();
        protected void isAlive() {
            System.out.println("Yes!");
        }
    }

    static class Patient extends Person {
        @Override
        public void communicate() {
            System.out.println("I'm a Patient");
        }

        @Override
        public void isAlive() {
            System.out.println("Maybe ...");
        }
    }

    static class Medic extends Person {
        @Override
        public void communicate() {
            System.out.println("I'm a Medic");
        }
    }

    public static void main(String[] args) {
        //Person person = new Person(); // not possible

        Medic medic = new Medic();
        medic.communicate(); // I'm a Medic

        Patient patient = new Patient();
        patient.isAlive(); // Maybe ...
    }
}
