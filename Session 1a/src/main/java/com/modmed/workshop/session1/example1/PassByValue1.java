package com.modmed.workshop.session1.example1;

class PassByValue1 {
    public static void main(String[] args) {
        Patient patient = new Patient("James");
        Patient samePatient = patient;

        // we pass the object reference as value
        foo(patient);
        // patient variable is still James
        System.out.println(patient.getName()); // James
        System.out.println(patient.equals(samePatient)); // still same patient
    }

    static void foo(Patient patient) {
        System.out.println(patient.getName()); // James
        patient = new Patient("Elena");
        System.out.println(patient.getName()); // Elena
    }
}
