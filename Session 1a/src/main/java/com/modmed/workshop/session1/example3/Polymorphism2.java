package com.modmed.workshop.session1.example3;

public class Polymorphism2 {
    class Person {
        void communicate() {}
    }

    class Patient extends Person {
    }

    class Medic extends Person {
    }
}
