package com.modmed.workshop.session1.example2;

public class Inheritance1 {
    class Person {
        void communicate() {}
    }

    class Patient {
        void communicate() {}
    }
}
