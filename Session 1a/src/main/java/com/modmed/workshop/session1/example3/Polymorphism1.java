package com.modmed.workshop.session1.example3;

public class Polymorphism1 {
    class Person {
        void communicate() {}
    }

    class Patient {
        void communicate() {}
    }

    class Medic {
        void communicate() {}
    }
}
