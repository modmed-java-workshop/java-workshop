package annotations.challenge;

import annotations.challenge.deprecated.DeprecatedClass;

public class Challenge3 {

    public static void main(String[] args) {
        /*Use the deprecated.DeprecatedClass
         * and suppress the warning*/
        DeprecatedClass challenge3 = new DeprecatedClass();
    }
}
