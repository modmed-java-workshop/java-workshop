package annotations.challenge.challenge4;


/*Create an Annotation that can be used only
 * in the class declaration and placed it here*/
public class AnnotatedClass {

    /*For the following class members (Attribute, Method and ParamMethod)
     * use the corresponded annotations located in this package*/

    private String annotatedField;

    public void annotatedMethod(String annotatedParameter) {
        //Do something...
    }

}
