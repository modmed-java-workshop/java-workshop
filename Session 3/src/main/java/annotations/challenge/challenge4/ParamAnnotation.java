package annotations.challenge.challenge4;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
public @interface ParamAnnotation {
}
