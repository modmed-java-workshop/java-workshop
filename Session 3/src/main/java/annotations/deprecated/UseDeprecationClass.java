package annotations.deprecated;

public class UseDeprecationClass {

    @SuppressWarnings({"all"})
    DeprecatedClass myAttribute = new DeprecatedClass();

    public static void main(String[] args) {
        new UseDeprecationClass().printDeprecatedClass();
    }

    void printDeprecatedClass() {
        System.out.println(myAttribute.getClass());
    }

}
