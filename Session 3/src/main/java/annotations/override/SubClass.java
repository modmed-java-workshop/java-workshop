package annotations.override;

public class SubClass extends SuperClass {
    @Override // Indicates is over-writing its base class method.
    public void printSomething() {
        super.printSomething();
    }
}
