package annotations;

import annotations.challenge.challenge4.FieldAnnotation;
import annotations.challenge.challenge4.MethodAnnotation;
import annotations.challenge.challenge4.ParamAnnotation;

@ClassAnnotation
public class AnnotationsClass {

    @FieldAnnotation
    private String annotatedField;

    @MethodAnnotation
    public void annotatedMethod(@ParamAnnotation String annotatedParameter) {
        //Do something...
    }
}
