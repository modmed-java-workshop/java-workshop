package annotations.custom;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;

@Documented //Indicates that this annotation must be documented by the java doc generation.
@Inherited // Specifies that this annotation can be inherited from the super class
public @interface CustomAnnotation {
}
