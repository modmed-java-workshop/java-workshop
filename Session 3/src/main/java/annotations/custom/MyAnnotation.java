package annotations.custom;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@CustomAnnotation
@Target(ElementType.TYPE)
public @interface MyAnnotation {
    String info();
}
