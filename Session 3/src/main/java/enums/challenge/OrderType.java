package enums.challenge;

import enums.OrderTypePrettyPrint;

/* Challenge: Add a second attribute called 'isActive'(boolean)
to this Enum, including the method to get the value and the initialization*/
public enum OrderType implements OrderTypePrettyPrint {
    LABS("Laboratory"),
    SURGERY("Surgery"),
    RADIOLOGY("Radiology"),
    PROCEDURE("Procedure");

    private final String value;

    OrderType(String value) {
        this.value = value;
    }

    @Override
    public String getPrettyPrint() {
        return "The order for the patient is: " + this.getValue();
    }

    public String getValue() {
        return this.value;
    }

}
