package enums;

public enum OrderType implements OrderTypePrettyPrint {
    LABS("Laboratory"),
    SURGERY("Surgery"),
    RADIOLOGY("Radiology"),
    PROCEDURE("Procedure");

    private final String value;

    OrderType(String value) {
        this.value = value;
    }

    @Override
    public String getPrettyPrint() {
        return "The order for the patient is: " + this.getValue();
    }

    public String getValue() {
        return this.value;
    }

    public String valueToLowerCase() {
        return this.value.toLowerCase();
    }

}
