package enums;

public class EnumAsParameter {

    public static int exam;

    public static void main(String[] args) {

        // Sending intended value for OrderType
        checkExamOrderType(OrderTypeConstants.PROCEDURE);

        checkExamOrderType("Any possible string value");

        // Sending enum value 'LABS' only values from Order type enum are allowed
        checkExamOrderType(OrderType.LABS);

    }

    public static void checkExamOrderType(String orderType) {
        /* You can't be sure that the string 'orderType' is one of the
         all possible values for medical orders, it can be any string value*/
        doSomethingElse(orderType, exam);
    }

    /*type safety at compile-time checking to avoid errors at run-time*/
    public static void checkExamOrderType(OrderType orderType) {
        doSomethingElse(orderType, exam);
    }

    public static void doSomethingElse(String orderType, int exam) {

    }

    public static void doSomethingElse(OrderType orderType, int exam) {

    }
}
