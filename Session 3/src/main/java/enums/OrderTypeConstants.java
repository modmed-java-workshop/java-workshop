package enums;

public class OrderTypeConstants {
    public static String LABS = "Laboratory";
    public static String SURGERY = "Surgery";
    public static String RADIOLOGY = "Radiology";
    public static String PROCEDURE = "Procedure";
}
