package enums;

public class EnumIteration {

    public static void main(String[] args) {

        // Print each constant defined in the enum OrderType
        for (OrderType orderType : OrderType.values()) {
            System.out.println(orderType);
        }

        // Print the enum constant's value
        for (OrderType orderType : OrderType.values()) {
            System.out.println(orderType.getValue());
        }

        //Storing an enum in an Array
        OrderType[] orderTypes = OrderType.values();

        // Iterate over the array and print the result of the implemented method
        for (OrderType orderType : orderTypes) {
            System.out.println(orderType.getPrettyPrint());
        }

    }
}
