package enums;

public class EnumNullSafeTest {

    public static void main(String[] args) {

        enumNull();
        enumNullSafe();
    }

    public static void enumNull() {
        OrderType orderType = null;
        if (orderType.equals(OrderType.RADIOLOGY)) {
            System.out.println("It's Radiology");
        }
        // Will throw an error -> null Pointer Exception
    }

    // Null safe using equals
    public static void enumNullSafe() {
        OrderType orderType = null;
        if (OrderType.RADIOLOGY.equals(orderType)) {
            System.out.println("It's Radiology");
        } else {
            System.out.println("See? null safe!");
        }
    }

}
