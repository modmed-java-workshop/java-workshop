import enums.OrderType;

public class MainClass {
    public static void main(String[] args) {
        /*System.out.println(OrderType.LABS); // prints 'LABS'
        System.out.println(OrderType.SURGERY); // prints 'RADIOLOGY'....
        System.out.println(OrderType.RADIOLOGY);
        System.out.println(OrderType.PROCEDURE);*/
        for (OrderType orderType : OrderType.values()) {
/*
            System.out.println(orderType.getValue());
*/
        }
        switchStatement();
        enumNullSafe();
        enumNull();

        OrderType orderType = OrderType.RADIOLOGY;

        if (orderType.equals(OrderType.LABS)) {
            System.out.println("It's a Laboratory order");
        } else if (orderType.equals(OrderType.SURGERY)) {
            System.out.println("It's a Surgery order");
        } else if (orderType.equals(OrderType.RADIOLOGY)) {
            System.out.println("It's a Radiology order");
        }

        orderType = null;

        if (orderType.equals(OrderType.RADIOLOGY)) {
            System.out.println("It's Radiology");
        }
    }

    public static void enumNull() {
        OrderType orderType = null;
        if (orderType.equals(OrderType.RADIOLOGY)) {
            System.out.println("It's Radiology");
        }
        // Will throw an error -> null Pointer Exception
    }

    // Null safe using equals
    public static void enumNullSafe() {
        OrderType orderType = null;
        if (OrderType.RADIOLOGY.equals(orderType)) {
            System.out.println("It's Radiology");
        } else {
            System.out.println("See? null safe!");
        }
    }

    public static void switchStatement() {
        OrderType orderType = OrderType.SURGERY;
        switch (orderType) {
            case LABS:
                System.out.println("It's a Laboratory order");
                break;
            case SURGERY:
                System.out.println("It's a Surgery order");
                break;
            case RADIOLOGY:
                System.out.println("It's a Radiology order");
                break;
            default:
                System.out.println("It's a procedure");
                break;
        }

    }
}
