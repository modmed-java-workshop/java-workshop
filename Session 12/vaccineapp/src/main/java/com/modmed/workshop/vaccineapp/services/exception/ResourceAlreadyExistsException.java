package com.modmed.workshop.vaccineapp.services.exception;

public class ResourceAlreadyExistsException extends AppException {
    public ResourceAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }
}
