package com.modmed.workshop.vaccineapp.converter;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.util.DateHelper;

public class VaccineDosesTakenConverter extends AbstractConverter<VaccineDosesTaken, VaccineDosesTakenDTO> {

    @Override
    public VaccineDosesTaken fromDto(VaccineDosesTakenDTO dto) {
        VaccineDosesTaken entity = new VaccineDosesTaken();
        if(dto.getPatientDTO() != null){
            PatientConverter patientConverter = new PatientConverter();
            entity.setPatient(patientConverter.fromDto(dto.getPatientDTO()));
        }
        if(dto.getVaccineDTO() != null){
            VaccineConverter vaccineConverter = new VaccineConverter();
            entity.setVaccine(vaccineConverter.fromDto(dto.getVaccineDTO()));
        }
        entity.setTakenAt(DateHelper.toDate(dto.getTakenAT()));
        return entity;
    }

    @Override
    public VaccineDosesTakenDTO fromEntity(VaccineDosesTaken entity) {
        VaccineDosesTakenDTO dto = new VaccineDosesTakenDTO();
        if (null != entity.getVaccine()) {
            VaccineConverter vaccineConverter = new VaccineConverter();
            dto.setVaccineDTO(vaccineConverter.fromEntityIgnoreDosesTaken(entity.getVaccine()));
        }
        dto.setId(entity.getId());
        if (null != entity.getPatient()) {
            PatientConverter patientConverter = new PatientConverter();
            dto.setPatientDTO(patientConverter.fromEntityIgnoreDosesTaken(entity.getPatient()));
        }
        dto.setTakenAT(DateHelper.toStringDate(entity.getTakenAt()));
        return dto;
    }

    public VaccineDosesTaken fromCreateDTOData(Patient patient, Vaccine vaccine, String takenAt){
        VaccineDosesTaken vaccineDosesTaken = new VaccineDosesTaken();
        vaccineDosesTaken.setPatient(patient);
        vaccineDosesTaken.setVaccine(vaccine);
        vaccineDosesTaken.setTakenAt(DateHelper.toDate(takenAt));
        return vaccineDosesTaken;
    }
}
