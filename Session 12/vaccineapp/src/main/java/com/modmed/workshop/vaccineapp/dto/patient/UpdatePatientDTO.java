package com.modmed.workshop.vaccineapp.dto.patient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UpdatePatientDTO implements Serializable {
    private String firstName;
    private String lastName;
    private String imageUrl;
}
