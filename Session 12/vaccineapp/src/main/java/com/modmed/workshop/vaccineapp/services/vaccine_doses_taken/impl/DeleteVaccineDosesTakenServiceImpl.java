package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.impl;

import com.modmed.workshop.vaccineapp.repository.VaccineDosesTakenRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.DeleteVaccineDosesTakenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteVaccineDosesTakenServiceImpl implements DeleteVaccineDosesTakenService {
    private final VaccineDosesTakenRepository vaccineDosesTakenRepository;

    @Autowired
    public DeleteVaccineDosesTakenServiceImpl(VaccineDosesTakenRepository vaccineDosesTakenRepository) {
        this.vaccineDosesTakenRepository = vaccineDosesTakenRepository;
    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException {
        if(!this.vaccineDosesTakenRepository.existsById(id)){
            throw new ResourceNotFoundException(String.format("Vaccine doses taken with id: %d not found", id));
        }
        this.vaccineDosesTakenRepository.deleteById(id);
    }
}
