package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface FindOneVaccineDosesTakenService {
    VaccineDosesTaken findOne(Long id) throws ResourceNotFoundException;
}
