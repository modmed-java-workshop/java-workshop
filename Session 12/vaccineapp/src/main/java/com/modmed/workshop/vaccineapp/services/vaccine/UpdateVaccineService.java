package com.modmed.workshop.vaccineapp.services.vaccine;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.dto.vaccine.UpdateVaccineDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface UpdateVaccineService {
    Vaccine update(Long id, UpdateVaccineDTO updateVaccineDTO) throws ResourceNotFoundException, InvalidResourceException;
}
