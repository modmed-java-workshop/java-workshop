package com.modmed.workshop.vaccineapp.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateHelper {

    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public static String toStringDate(LocalDateTime localDateTime){
        if(localDateTime == null){
            return null;
        }
        return localDateTime.format(FORMATTER);
    }

    public static LocalDateTime toDate(String date){
        return LocalDateTime.parse(date, FORMATTER);
    }
}
