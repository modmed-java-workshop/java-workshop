package com.modmed.workshop.vaccineapp.dto.patient;

import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@ToString
public class PatientDTO implements Serializable {
    private Long id;
    private String dni;
    private String firstName;
    private String lastName;
    private String gender;
    private String dateOfBirthStr;
    private String imageUrl;
    private Set<VaccineDosesTakenDTO> dosageTaken;
}
