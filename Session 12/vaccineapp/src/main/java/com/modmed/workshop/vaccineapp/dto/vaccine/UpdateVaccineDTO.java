package com.modmed.workshop.vaccineapp.dto.vaccine;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UpdateVaccineDTO implements Serializable {
    private String brand;
    private String description;
}
