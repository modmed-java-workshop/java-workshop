package com.modmed.workshop.vaccineapp.repository;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VaccineRepository extends JpaRepository<Vaccine, Long> {
    Vaccine findByBrand(String brand);
}

