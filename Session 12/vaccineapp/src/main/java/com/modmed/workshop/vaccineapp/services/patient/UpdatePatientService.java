package com.modmed.workshop.vaccineapp.services.patient;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.dto.patient.UpdatePatientDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface UpdatePatientService {
    Patient update(Long id, UpdatePatientDTO updatePatientDTO) throws InvalidResourceException, ResourceNotFoundException;
}
