package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.impl;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.UpdateVaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.repository.VaccineDosesTakenRepository;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.FindOneVaccineDosesTakenService;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.UpdateVaccineDosesTakenService;
import com.modmed.workshop.vaccineapp.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateVaccineDosesTakenServiceImpl implements UpdateVaccineDosesTakenService {

    private final FindOneVaccineDosesTakenService findOneVaccineDosesTakenService;
    private final VaccineDosesTakenRepository vaccineDosesTakenRepository;

    @Autowired
    public UpdateVaccineDosesTakenServiceImpl(FindOneVaccineDosesTakenService findOneVaccineDosesTakenService,
                                              VaccineDosesTakenRepository vaccineDosesTakenRepository) {
        this.findOneVaccineDosesTakenService = findOneVaccineDosesTakenService;
        this.vaccineDosesTakenRepository = vaccineDosesTakenRepository;
    }

    @Override
    public VaccineDosesTaken update(Long id, UpdateVaccineDosesTakenDTO updateVaccineDosesTakenDTO) throws ResourceNotFoundException,
            InvalidResourceException {
        VaccineDosesTaken existingVaccineDosesTaken = this.findOneVaccineDosesTakenService.findOne(id);
        existingVaccineDosesTaken.setTakenAt(DateHelper.toDate(updateVaccineDosesTakenDTO.getTakenAt()));
        this.vaccineDosesTakenRepository.save(existingVaccineDosesTaken);
        return existingVaccineDosesTaken;
    }
}
