package com.modmed.workshop.vaccineapp.controllers;

import com.modmed.workshop.vaccineapp.converter.VaccineDosesTakenConverter;
import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.MessageResponse;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.CreateVaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.UpdateVaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.CreateVaccineDosesTakenService;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.DeleteVaccineDosesTakenService;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.FindAllVaccinesDosesTakenService;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.FindOneVaccineDosesTakenService;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.UpdateVaccineDosesTakenService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vaccine-doses-taken")
public class VaccineDosesTakenResource {

    private final FindAllVaccinesDosesTakenService findAllVaccinesDosesTakenService;
    private final FindOneVaccineDosesTakenService findOneVaccineDosesTakenService;
    private final CreateVaccineDosesTakenService createVaccineDosesTakenService;
    private final UpdateVaccineDosesTakenService updateVaccineDosesTakenService;
    private final DeleteVaccineDosesTakenService deleteVaccineDosesTakenService;
    private final VaccineDosesTakenConverter vaccineDosesTakenConverter;

    @Autowired
    public VaccineDosesTakenResource(FindAllVaccinesDosesTakenService findAllVaccinesDosesTakenService,
                                     FindOneVaccineDosesTakenService findOneVaccineDosesTakenService,
                                     CreateVaccineDosesTakenService createVaccineDosesTakenService,
                                     UpdateVaccineDosesTakenService updateVaccineDosesTakenService,
                                     DeleteVaccineDosesTakenService deleteVaccineDosesTakenService) {
        this.findAllVaccinesDosesTakenService = findAllVaccinesDosesTakenService;
        this.findOneVaccineDosesTakenService = findOneVaccineDosesTakenService;
        this.createVaccineDosesTakenService = createVaccineDosesTakenService;
        this.updateVaccineDosesTakenService = updateVaccineDosesTakenService;
        this.deleteVaccineDosesTakenService = deleteVaccineDosesTakenService;
        this.vaccineDosesTakenConverter = new VaccineDosesTakenConverter();
    }

    @GetMapping("")
    @ApiOperation(value = "Find all doses taken for all patients.", response = VaccineDosesTakenDTO.class)
    public List<VaccineDosesTakenDTO> findALl(){
        List<VaccineDosesTaken> vaccinesDosesTaken = this.findAllVaccinesDosesTakenService.findAll();
        return vaccinesDosesTaken.stream().map(this.vaccineDosesTakenConverter::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public VaccineDosesTakenDTO findOne(@PathVariable long id) throws ResourceNotFoundException {
        VaccineDosesTaken vaccineDosesTaken = this.findOneVaccineDosesTakenService.findOne(id);
        return this.vaccineDosesTakenConverter.fromEntity(vaccineDosesTaken);
    }

    @PostMapping("")
    @ApiOperation(value = "apply a dose to a patient.", notes = "date format: dd-MM-yyyy HH:mm:ss")
    public VaccineDosesTakenDTO create(@RequestBody CreateVaccineDosesTakenDTO createVaccineDosesTakenDTO)
            throws InvalidResourceException, ResourceAlreadyExistsException, ResourceNotFoundException {
        VaccineDosesTaken vaccineDosesTaken = this.createVaccineDosesTakenService.create(createVaccineDosesTakenDTO);
        return this.vaccineDosesTakenConverter.fromEntity(vaccineDosesTaken);
    }

    @PutMapping("/{id}")
    public VaccineDosesTakenDTO update(@PathVariable long id, @RequestBody UpdateVaccineDosesTakenDTO updateVaccineDosesTakenDTO) throws InvalidResourceException,
            ResourceNotFoundException {
        VaccineDosesTaken vaccineDosesTaken = this.updateVaccineDosesTakenService.update(id, updateVaccineDosesTakenDTO);
        return this.vaccineDosesTakenConverter.fromEntity(vaccineDosesTaken);
    }

    @DeleteMapping("/{id}")
    public MessageResponse delete(@PathVariable long id) throws ResourceNotFoundException {
        this.deleteVaccineDosesTakenService.delete(id);
        return new MessageResponse("Vaccine doses taken deleted successfully");
    }

}
