package com.modmed.workshop.vaccineapp.services.vaccine.impl;

import com.modmed.workshop.vaccineapp.repository.VaccineRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine.DeleteVaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeleteVaccineServiceImpl implements DeleteVaccineService {

    private final VaccineRepository vaccineRepository;

    @Autowired
    public DeleteVaccineServiceImpl(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    @Override
    @Transactional
    public void delete(Long id) throws ResourceNotFoundException {
        if(!this.vaccineRepository.existsById(id)){
            throw new ResourceNotFoundException(String.format("Vaccine with id: %d not found", id));
        }
        this.vaccineRepository.deleteById(id);
    }
}
