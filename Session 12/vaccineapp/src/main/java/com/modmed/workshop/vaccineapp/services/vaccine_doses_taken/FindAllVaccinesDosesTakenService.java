package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;

import java.util.List;

public interface FindAllVaccinesDosesTakenService {
    List<VaccineDosesTaken> findAll();
}
