package com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken;

import com.modmed.workshop.vaccineapp.dto.patient.PatientDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine.VaccineDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class VaccineDosesTakenDTO implements Serializable {
    private Long id;
    private VaccineDTO vaccineDTO;
    private PatientDTO patientDTO;
    private String takenAT;
}
