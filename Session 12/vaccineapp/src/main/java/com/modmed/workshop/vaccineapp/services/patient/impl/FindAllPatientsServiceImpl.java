package com.modmed.workshop.vaccineapp.services.patient.impl;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.repository.PatientRepository;
import com.modmed.workshop.vaccineapp.services.patient.FindAllPatientsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllPatientsServiceImpl implements FindAllPatientsService {

    @Autowired
    PatientRepository patientRepository;

    @Override
    public List<Patient> findAllPatients(){
        return patientRepository.findAll();
    }
}
