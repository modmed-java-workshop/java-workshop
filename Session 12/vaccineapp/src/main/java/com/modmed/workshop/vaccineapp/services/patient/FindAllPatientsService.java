package com.modmed.workshop.vaccineapp.services.patient;

import com.modmed.workshop.vaccineapp.domain.Patient;

import java.util.List;

public interface FindAllPatientsService {
    List<Patient> findAllPatients();
}
