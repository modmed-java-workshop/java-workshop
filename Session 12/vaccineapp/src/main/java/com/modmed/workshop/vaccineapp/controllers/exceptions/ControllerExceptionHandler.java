package com.modmed.workshop.vaccineapp.controllers.exceptions;

import com.modmed.workshop.vaccineapp.dto.MessageResponse;
import java.util.List;
import java.util.stream.Collectors;

import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.exception.AppException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {PatientAlreadyVaccinatedException.class})
    protected ResponseEntity<Object> handleUserAlreadyExists(
            PatientAlreadyVaccinatedException ex) {
        log.info(ex.getMessage());
        MessageResponse body = new MessageResponse(String.format("Patient %s %s with DNI: %s, is already vaccinated",
                ex.getPatientDni(), ex.getPatientFirstName(), ex.getPatientLastName()));
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {AppException.class})
    protected ResponseEntity<Object> handleVaccineNotFound(AppException appException) {
        log.info(appException.getMessage());
        MessageResponse body = new MessageResponse(appException.getMessage());
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        if(appException instanceof InvalidResourceException){
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
        }
        else if(appException instanceof ResourceAlreadyExistsException){
            httpStatus = HttpStatus.CONFLICT;
        }
        else if(appException instanceof ResourceNotFoundException){
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(body, httpStatus);
    }

    // If we want to validate argument from UI / client
    // If not, remove this method
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        List<MessageResponse> body = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> {
                    log.info("Validated object \"{}\" -> validation mesage: \"{}\" -> field \"{}\", rejected value: {}",
                            x.getObjectName(), x.getDefaultMessage(), x.getField(), x.getRejectedValue());
                    return new MessageResponse(x.getDefaultMessage());
                })
                .collect(Collectors.toList());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleInternalServerError(
            Exception ex) {
        String exceptionMessage = ex.getMessage() == null ? "Internal server error"
                : ex.getMessage();
        log.error(exceptionMessage, ex);
        MessageResponse body = new MessageResponse("Unexpected error from server");
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

