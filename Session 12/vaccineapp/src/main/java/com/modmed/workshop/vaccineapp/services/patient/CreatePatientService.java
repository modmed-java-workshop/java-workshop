package com.modmed.workshop.vaccineapp.services.patient;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.dto.patient.CreatePatientDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;

public interface CreatePatientService {
    Patient create(CreatePatientDTO createPatientDTO) throws InvalidResourceException, ResourceAlreadyExistsException;
}
