package com.modmed.workshop.vaccineapp.services.patient.impl;

import com.modmed.workshop.vaccineapp.repository.PatientRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.patient.DeletePatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeletePatientServiceImpl implements DeletePatientService {
    private final PatientRepository patientRepository;

    @Autowired
    public DeletePatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException {
        if(!this.patientRepository.existsById(id)){
            throw new ResourceNotFoundException(String.format("Patient with id: %d not found", id));
        }
        this.patientRepository.deleteById(id);
    }
}
