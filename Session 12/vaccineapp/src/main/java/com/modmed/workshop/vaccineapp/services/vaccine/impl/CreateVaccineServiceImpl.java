package com.modmed.workshop.vaccineapp.services.vaccine.impl;

import com.modmed.workshop.vaccineapp.converter.VaccineConverter;
import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.dto.vaccine.CreateVaccineDTO;
import com.modmed.workshop.vaccineapp.repository.VaccineRepository;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.vaccine.CreateVaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CreateVaccineServiceImpl implements CreateVaccineService {

    private final VaccineRepository vaccineRepository;
    private final VaccineConverter vaccineConverter;

    @Autowired
    public CreateVaccineServiceImpl(VaccineRepository vaccineRepository, VaccineConverter vaccineConverter) {
        this.vaccineRepository = vaccineRepository;
        this.vaccineConverter = vaccineConverter;
    }

    @Override
    @Transactional
    public Vaccine create(CreateVaccineDTO createVaccineDTO) throws InvalidResourceException, ResourceAlreadyExistsException {
        this.validateData(createVaccineDTO);
        Vaccine vaccine = this.vaccineConverter.fromCreateDTO(createVaccineDTO);
        this.vaccineRepository.save(vaccine);
        return vaccine;
    }

    private void validateData(CreateVaccineDTO createVaccineDTO) throws InvalidResourceException, ResourceAlreadyExistsException {
        String brand = createVaccineDTO.getBrand();
        String description = createVaccineDTO.getDescription();
        if((brand == null || brand.isEmpty()) && (description == null || description.isEmpty())){
            throw new InvalidResourceException("Invalid vaccine brand and description");
        }
        else if(brand == null || brand.isEmpty()){
            throw new InvalidResourceException("Invalid vaccine brand");
        }
        else if(description == null || description.isEmpty()){
            throw new InvalidResourceException("Invalid vaccine description");
        }
        Vaccine existingVaccine = this.vaccineRepository.   findByBrand(brand);
        if(existingVaccine != null){
            throw new ResourceAlreadyExistsException(String.format("A Vaccine with brand %s already exists", brand));
        }
    }
}
