package com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class CreateVaccineDosesTakenDTO implements Serializable {
    private Integer patientId;
    private Integer vaccineId;
    private String takenAt;
}
