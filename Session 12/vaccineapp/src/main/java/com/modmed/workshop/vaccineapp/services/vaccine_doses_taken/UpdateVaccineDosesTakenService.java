package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.UpdateVaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface UpdateVaccineDosesTakenService {
    VaccineDosesTaken update(Long id, UpdateVaccineDosesTakenDTO updateVaccineDosesTakenDTO)
            throws ResourceNotFoundException, InvalidResourceException;
}
