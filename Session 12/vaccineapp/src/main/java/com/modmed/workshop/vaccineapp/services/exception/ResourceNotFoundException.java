package com.modmed.workshop.vaccineapp.services.exception;

public class ResourceNotFoundException extends AppException {
    public ResourceNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
