package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.impl;

import com.modmed.workshop.vaccineapp.converter.VaccineDosesTakenConverter;
import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.CreateVaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.repository.VaccineDosesTakenRepository;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.patient.FindOnePatientService;
import com.modmed.workshop.vaccineapp.services.vaccine.FindOneVaccineService;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.CreateVaccineDosesTakenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateVaccineDosesTakenServiceImpl implements CreateVaccineDosesTakenService {
    private final FindOnePatientService findOnePatientService;
    private final FindOneVaccineService findOneVaccineService;
    private final VaccineDosesTakenConverter vaccineDosesTakenConverter;
    private final VaccineDosesTakenRepository vaccineDosesTakenRepository;

    @Autowired
    public CreateVaccineDosesTakenServiceImpl(FindOnePatientService findOnePatientService,
                                              FindOneVaccineService findOneVaccineService,
                                              VaccineDosesTakenRepository vaccineDosesTakenRepository) {
        this.findOnePatientService = findOnePatientService;
        this.findOneVaccineService = findOneVaccineService;
        this.vaccineDosesTakenRepository = vaccineDosesTakenRepository;
        this.vaccineDosesTakenConverter = new VaccineDosesTakenConverter();
    }

    @Override
    public VaccineDosesTaken create(CreateVaccineDosesTakenDTO createVaccineDosesTakenDTO) throws InvalidResourceException,
            ResourceAlreadyExistsException, ResourceNotFoundException {
        this.validateData(createVaccineDosesTakenDTO);
        Integer patientId = createVaccineDosesTakenDTO.getPatientId();
        Integer vaccineId = createVaccineDosesTakenDTO.getVaccineId();
        VaccineDosesTaken existingVaccineDosesTaken = this.vaccineDosesTakenRepository.findByPatient_IdAndVaccine_Id(patientId, vaccineId);
        if(existingVaccineDosesTaken != null){
            throw new ResourceNotFoundException(String.format("Patient with id: %d has already taken the vaccine with id: %d", patientId, vaccineId));
        }
        Patient existingPatient = this.findOnePatientService.findOne((long) patientId);
        Vaccine existingVaccine = this.findOneVaccineService.findOne((long) vaccineId);
        VaccineDosesTaken vaccineDosesTaken = this.vaccineDosesTakenConverter.fromCreateDTOData(existingPatient, existingVaccine,
                createVaccineDosesTakenDTO.getTakenAt());
        this.vaccineDosesTakenRepository.save(vaccineDosesTaken);
        return vaccineDosesTaken;
    }

    private void validateData(CreateVaccineDosesTakenDTO createVaccineDosesTakenDTO) throws InvalidResourceException {
        Integer patientId = createVaccineDosesTakenDTO.getPatientId();
        Integer vaccineId = createVaccineDosesTakenDTO.getVaccineId();
        String takenAt = createVaccineDosesTakenDTO.getTakenAt();
        boolean invalidPatientId = patientId == null || patientId == 0;
        boolean invalidVaccineId = vaccineId == null || vaccineId == 0;
        boolean invalidTakenAt = takenAt == null || takenAt.isEmpty(); // o no es fecha.
        if(invalidPatientId && invalidVaccineId && invalidTakenAt){
            throw new InvalidResourceException("Invalid patientId, vaccineId and takenAt date");
        }
        else if(invalidPatientId && invalidVaccineId){
            throw new InvalidResourceException("Invalid patientId and vaccineId");
        }
        else if(invalidVaccineId && invalidTakenAt){
            throw new InvalidResourceException("Invalid vaccineId and invalid takenAt date");
        }
        else if(invalidPatientId && invalidTakenAt){
            throw new InvalidResourceException("Invalid patientId and invalid takenAt date");
        }
        else if(invalidPatientId){
            throw new InvalidResourceException("Invalid patientId");
        }
        else if(invalidVaccineId){
            throw new InvalidResourceException("Invalid vaccineId");
        }
        else if(invalidTakenAt){
            throw new InvalidResourceException("Invalid takenAt date");
        }
    }
}
