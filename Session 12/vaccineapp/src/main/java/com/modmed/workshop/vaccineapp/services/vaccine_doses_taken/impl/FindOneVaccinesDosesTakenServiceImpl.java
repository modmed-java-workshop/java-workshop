package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.impl;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.repository.VaccineDosesTakenRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.FindOneVaccineDosesTakenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FindOneVaccinesDosesTakenServiceImpl implements FindOneVaccineDosesTakenService {

    private final VaccineDosesTakenRepository vaccineDosesTakenRepository;

    @Autowired
    public FindOneVaccinesDosesTakenServiceImpl(VaccineDosesTakenRepository vaccineDosesTakenRepository) {
        this.vaccineDosesTakenRepository = vaccineDosesTakenRepository;
    }

    @Override
    public VaccineDosesTaken findOne(Long id) throws ResourceNotFoundException {
        Optional<VaccineDosesTaken> vaccineDosesTakenOptional = this.vaccineDosesTakenRepository.findById(id);
        if(!vaccineDosesTakenOptional.isPresent()){
            throw new ResourceNotFoundException(String.format("Vaccine doses taken with id: %d not found", id));
        }
        return vaccineDosesTakenOptional.get();
    }
}
