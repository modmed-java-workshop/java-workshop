package com.modmed.workshop.vaccineapp.services.patient;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface FindOnePatientService {
    Patient findOne(Long id) throws ResourceNotFoundException;
}
