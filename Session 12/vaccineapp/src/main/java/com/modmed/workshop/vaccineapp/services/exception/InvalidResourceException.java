package com.modmed.workshop.vaccineapp.services.exception;

public class InvalidResourceException extends AppException {
    public InvalidResourceException(String errorMessage) {
        super(errorMessage);
    }
}
