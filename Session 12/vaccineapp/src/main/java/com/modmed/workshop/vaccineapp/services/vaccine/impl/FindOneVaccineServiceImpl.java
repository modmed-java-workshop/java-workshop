package com.modmed.workshop.vaccineapp.services.vaccine.impl;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.repository.VaccineRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine.FindOneVaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FindOneVaccineServiceImpl implements FindOneVaccineService {

    private final VaccineRepository vaccineRepository;

    @Autowired
    public FindOneVaccineServiceImpl(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    @Override
    public Vaccine findOne(Long id) throws ResourceNotFoundException {
        Optional<Vaccine> optionalVaccine = this.vaccineRepository.findById(id);
        if(!optionalVaccine.isPresent()){
            throw new ResourceNotFoundException(String.format("Vaccine with id: %d not found", id));
        }
        return optionalVaccine.get();
    }
}
