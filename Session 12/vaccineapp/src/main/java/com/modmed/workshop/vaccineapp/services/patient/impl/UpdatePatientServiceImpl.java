package com.modmed.workshop.vaccineapp.services.patient.impl;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.dto.patient.UpdatePatientDTO;
import com.modmed.workshop.vaccineapp.repository.PatientRepository;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.patient.UpdatePatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UpdatePatientServiceImpl implements UpdatePatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public UpdatePatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public Patient update(Long id, UpdatePatientDTO updatePatientDTO) throws InvalidResourceException, ResourceNotFoundException {
        String firstName = updatePatientDTO.getFirstName();
        String lastName = updatePatientDTO.getLastName();
        String imageUrl = updatePatientDTO.getImageUrl();
        boolean invalidFirstName = firstName == null || firstName.isEmpty();
        boolean invalidLastName = lastName == null || lastName.isEmpty();
        boolean invalidImageUrl = imageUrl == null || imageUrl.isEmpty();
        if(invalidFirstName || invalidLastName || invalidImageUrl){
            throw new InvalidResourceException("Invalid patient data");
        }
        Optional<Patient> existingPatientOptional = this.patientRepository.findById(id);
        if(!existingPatientOptional.isPresent()){
            throw new ResourceNotFoundException(String.format("Patient with id: %d not found", id));
        }
        Patient patient = existingPatientOptional.get();
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setImageUrl(imageUrl);
        this.patientRepository.save(patient);
        return patient;
    }
}
