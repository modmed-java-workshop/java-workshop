package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken;

import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface DeleteVaccineDosesTakenService {
    void delete(Long id) throws ResourceNotFoundException;
}
