package com.modmed.workshop.vaccineapp.controllers;

import com.modmed.workshop.vaccineapp.converter.VaccineConverter;
import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.dto.MessageResponse;
import com.modmed.workshop.vaccineapp.dto.vaccine.CreateVaccineDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine.UpdateVaccineDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine.VaccineDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping(value ="vaccines")
public class VaccineResource {

    private final FindAllVaccinesService findAllVaccinesService;
    private final UpdateVaccineService updateVaccineService;
    private final DeleteVaccineService deleteVaccineService;
    private final CreateVaccineService createVaccineService;
    private final FindOneVaccineService findOneVaccineService;
    private final VaccineConverter vaccineConverter;

    @Autowired
    public VaccineResource(FindAllVaccinesService findAllVaccinesService, UpdateVaccineService updateVaccineService,
                           DeleteVaccineService deleteVaccineService, CreateVaccineService createVaccineService,
                           FindOneVaccineService findOneVaccineService, VaccineConverter vaccineConverter) {
        this.findAllVaccinesService = findAllVaccinesService;
        this.updateVaccineService = updateVaccineService;
        this.deleteVaccineService = deleteVaccineService;
        this.createVaccineService = createVaccineService;
        this.findOneVaccineService = findOneVaccineService;
        this.vaccineConverter = vaccineConverter;
    }

    @GetMapping("")
    public List<VaccineDTO> findAll(){
        List<Vaccine> vaccines = this.findAllVaccinesService.findAll();
        return vaccines.stream()
                .map(vaccineConverter::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public VaccineDTO findOne(@PathVariable long id) throws ResourceNotFoundException {
        return this.vaccineConverter.fromEntity(this.findOneVaccineService.findOne(id));
    }

    @PostMapping("")
    public VaccineDTO create(@RequestBody CreateVaccineDTO createVaccineDTO) throws InvalidResourceException,
            ResourceAlreadyExistsException {
        return this.vaccineConverter.fromEntity(this.createVaccineService.create(createVaccineDTO));
    }

    @PutMapping("/{id}")
    public VaccineDTO update(@PathVariable long id, @RequestBody UpdateVaccineDTO updateVaccineDTO)
            throws ResourceNotFoundException, InvalidResourceException, ResourceAlreadyExistsException {
        return this.vaccineConverter.fromEntity(this.updateVaccineService.update(id, updateVaccineDTO));
    }

    @DeleteMapping("/{id}")
    public MessageResponse delete(@PathVariable long id) throws ResourceNotFoundException {
        this.deleteVaccineService.delete(id);
        return new MessageResponse("Vaccine deleted successfully");
    }
}
