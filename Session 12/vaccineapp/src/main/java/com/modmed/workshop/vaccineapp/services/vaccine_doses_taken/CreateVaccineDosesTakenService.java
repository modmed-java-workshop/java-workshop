package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.CreateVaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface CreateVaccineDosesTakenService {
    VaccineDosesTaken create(CreateVaccineDosesTakenDTO createVaccineDosesTakenDTO)
            throws InvalidResourceException, ResourceAlreadyExistsException, ResourceNotFoundException;
}
