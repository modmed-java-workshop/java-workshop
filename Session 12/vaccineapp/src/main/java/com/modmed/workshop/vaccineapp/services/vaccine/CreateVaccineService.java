package com.modmed.workshop.vaccineapp.services.vaccine;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.dto.vaccine.CreateVaccineDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;

public interface CreateVaccineService {
    Vaccine create(CreateVaccineDTO createVaccineDTO) throws InvalidResourceException, ResourceAlreadyExistsException;
}
