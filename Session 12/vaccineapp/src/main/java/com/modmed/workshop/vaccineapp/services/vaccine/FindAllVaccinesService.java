package com.modmed.workshop.vaccineapp.services.vaccine;

import com.modmed.workshop.vaccineapp.domain.Vaccine;

import java.util.List;

public interface FindAllVaccinesService {
    List<Vaccine>findAll();
}
