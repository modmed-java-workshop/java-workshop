package com.modmed.workshop.vaccineapp.dto.vaccine;

import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@ToString
public class VaccineDTO implements Serializable {
    private Long id;
    private String brand;
    private String description;
    private Set<VaccineDosesTakenDTO> dosageTaken;
}
