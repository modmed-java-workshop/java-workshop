package com.modmed.workshop.vaccineapp.converter;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.patient.CreatePatientDTO;
import com.modmed.workshop.vaccineapp.dto.patient.PatientDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import java.text.SimpleDateFormat;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PatientConverter extends AbstractConverter<Patient, PatientDTO> {

    @Override
    public Patient fromDto(PatientDTO dto) {
        Patient entity = new Patient();
        entity.setDni(dto.getDni());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setDateOfBirth(new Date(dto.getDateOfBirthStr()));
        //Gender
        if(dto.getDosageTaken() != null){
            VaccineDosesTakenConverter dosesTakenConverter = new VaccineDosesTakenConverter();
            Set<VaccineDosesTaken> vaccinesDosesTaken = dto.getDosageTaken().stream()
                    .map(dosesTakenConverter::fromDto)
                    .collect(Collectors.toSet());
            entity.setDosageTaken(vaccinesDosesTaken);
        }
        return entity;
    }

    @Override
    public PatientDTO fromEntity(Patient entity) {
        PatientDTO dto = fromEntityIgnoreDosesTaken(entity);
        if (entity.getDosageTaken() != null) {
            VaccineDosesTakenConverter dosesTakenConverter = new VaccineDosesTakenConverter();
            Set<VaccineDosesTakenDTO> vaccinesDosesTakenDTOs = entity.getDosageTaken().stream()
                    .map(dosesTakenConverter::fromEntity)
                    .collect(Collectors.toSet());
            dto.setDosageTaken(vaccinesDosesTakenDTOs);
        }
        return dto;
    }

    public Patient fromCreatePatientDto(CreatePatientDTO createPatientDTO){
        Patient patient = new Patient();
        patient.setDni(createPatientDTO.getDni());
        patient.setFirstName(createPatientDTO.getFirstName());
        patient.setLastName(createPatientDTO.getLastName());
        patient.setDateOfBirth(createPatientDTO.getDateOfBirth());
        patient.setGender(createPatientDTO.getGender());
        patient.setImageUrl(createPatientDTO.getImageUrl());
        return patient;
    }

    public PatientDTO fromEntityIgnoreDosesTaken(final Patient entity) {
        PatientDTO dto = new PatientDTO();
        dto.setDni(entity.getDni());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setGender(entity.getGender().toString());
        dto.setId(entity.getId());
        dto.setImageUrl(entity.getImageUrl());
        dto.setDateOfBirthStr(new SimpleDateFormat("yyyy-MM-dd").format(entity.getDateOfBirth()));
        return dto;
    }
}
