package com.modmed.workshop.vaccineapp.dto.patient;

import com.modmed.workshop.vaccineapp.domain.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
public class CreatePatientDTO implements Serializable {
    private String dni;
    private String firstName;
    private String lastName;
    private Gender gender;
    private Date dateOfBirth;
    private String imageUrl;
}
