package com.modmed.workshop.vaccineapp.services.patient.impl;

import com.modmed.workshop.vaccineapp.converter.PatientConverter;
import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.dto.patient.CreatePatientDTO;
import com.modmed.workshop.vaccineapp.repository.PatientRepository;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.patient.CreatePatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreatePatientServiceImpl implements CreatePatientService {

    private final PatientRepository patientRepository;
    private final PatientConverter patientConverter;

    @Autowired
    public CreatePatientServiceImpl(PatientRepository patientRepository, PatientConverter patientConverter) {
        this.patientRepository = patientRepository;
        this.patientConverter = patientConverter;
    }

    @Override
    public Patient create(CreatePatientDTO createPatientDTO) throws InvalidResourceException, ResourceAlreadyExistsException {
        this.validateData(createPatientDTO);
        Patient patient = this.patientConverter.fromCreatePatientDto(createPatientDTO);
        return this.patientRepository.save(patient);
    }

    private void validateData(CreatePatientDTO createPatientDTO) throws InvalidResourceException, ResourceAlreadyExistsException {
        String dni = createPatientDTO.getDni();
        if(dni == null || dni.isEmpty()){
            throw new InvalidResourceException("Invalid patient dni");
        }
        Patient existingPatient = this.patientRepository.findByDni(dni);
        if(existingPatient != null){
            throw new ResourceAlreadyExistsException(String.format("A patient with dni: %s already exists", dni));
        }
    }
}
