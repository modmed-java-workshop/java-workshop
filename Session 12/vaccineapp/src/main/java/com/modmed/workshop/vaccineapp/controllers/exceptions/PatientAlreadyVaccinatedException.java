package com.modmed.workshop.vaccineapp.controllers.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
public class PatientAlreadyVaccinatedException extends RuntimeException {
    private final String patientDni;
    private final String patientLastName;
    private final String patientFirstName;
    public PatientAlreadyVaccinatedException(String dni, String firstName,String lastName) {
        super(String.format("Patient already vaccinated. DNI: %s, First Name: %s, Last Name: %s",
                dni, firstName, lastName));
        this.patientDni = dni;
        this.patientLastName = lastName;
        this.patientFirstName = firstName;
    }
}
