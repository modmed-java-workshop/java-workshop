package com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.impl;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.repository.VaccineDosesTakenRepository;
import com.modmed.workshop.vaccineapp.services.vaccine_doses_taken.FindAllVaccinesDosesTakenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindAllVaccinesDosesTakenServiceImpl implements FindAllVaccinesDosesTakenService {
    private final VaccineDosesTakenRepository vaccineDosesTakenRepository;

    @Autowired
    public FindAllVaccinesDosesTakenServiceImpl(VaccineDosesTakenRepository vaccineDosesTakenRepository) {
        this.vaccineDosesTakenRepository = vaccineDosesTakenRepository;
    }

    @Override
    public List<VaccineDosesTaken> findAll() {
        return this.vaccineDosesTakenRepository.findAll();
    }
}
