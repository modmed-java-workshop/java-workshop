package com.modmed.workshop.vaccineapp.services.vaccine;


import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface DeleteVaccineService {
    void delete(Long id) throws ResourceNotFoundException;
}
