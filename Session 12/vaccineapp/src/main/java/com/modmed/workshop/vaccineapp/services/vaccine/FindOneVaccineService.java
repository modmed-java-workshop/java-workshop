package com.modmed.workshop.vaccineapp.services.vaccine;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface FindOneVaccineService {
    Vaccine findOne(Long id) throws ResourceNotFoundException;
}
