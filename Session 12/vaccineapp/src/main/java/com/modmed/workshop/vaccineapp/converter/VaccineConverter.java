package com.modmed.workshop.vaccineapp.converter;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.vaccine.CreateVaccineDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine.VaccineDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class VaccineConverter extends AbstractConverter<Vaccine, VaccineDTO> {

    @Override
    public Vaccine fromDto(VaccineDTO dto) {
        Vaccine entity = new Vaccine();
        entity.setBrand(dto.getBrand());
        entity.setId(dto.getId());
        entity.setDescription(dto.getDescription());
        if(dto.getDosageTaken() != null){
            VaccineDosesTakenConverter dosesTakenConverter = new VaccineDosesTakenConverter();
            Set<VaccineDosesTaken> vaccineDosesTaken = dto.getDosageTaken().stream()
                    .map(dosesTakenConverter::fromDto)
                    .collect(Collectors.toSet());
            entity.setDosageTaken(vaccineDosesTaken);
        }
        return entity;
    }



    @Override
    public VaccineDTO fromEntity(Vaccine entity) {
        VaccineDTO dto = fromEntityIgnoreDosesTaken(entity);
        if(entity.getDosageTaken() != null){
            VaccineDosesTakenConverter dosesTakenConverter = new VaccineDosesTakenConverter();
            Set<VaccineDosesTakenDTO> vaccineDosesTakenDTOS = entity.getDosageTaken().stream()
                    .map(dosesTakenConverter::fromEntity)
                    .collect(Collectors.toSet());
            dto.setDosageTaken(vaccineDosesTakenDTOS);
        }
        return dto;
    }

    public Vaccine fromCreateDTO(CreateVaccineDTO createVaccineDTO){
        Vaccine vaccine = new Vaccine();
        vaccine.setBrand(createVaccineDTO.getBrand());
        vaccine.setDescription(createVaccineDTO.getDescription());
        return vaccine;
    }

    public VaccineDTO fromEntityIgnoreDosesTaken(final Vaccine entity) {
        VaccineDTO dto = new VaccineDTO();
        dto.setBrand(entity.getBrand());
        dto.setId(entity.getId());
        dto.setDescription(entity.getDescription());
        return dto;
    }
}
