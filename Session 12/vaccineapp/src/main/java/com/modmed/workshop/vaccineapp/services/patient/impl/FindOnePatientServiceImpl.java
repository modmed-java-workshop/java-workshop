package com.modmed.workshop.vaccineapp.services.patient.impl;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.repository.PatientRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.patient.FindOnePatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FindOnePatientServiceImpl implements FindOnePatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public FindOnePatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public Patient findOne(Long id) throws ResourceNotFoundException {
        Optional<Patient> patient = this.patientRepository.findById(id);
        if(!patient.isPresent()){
            throw new ResourceNotFoundException(String.format("Patient with id: %d not found", id));
        }
        return patient.get();
    }
}
