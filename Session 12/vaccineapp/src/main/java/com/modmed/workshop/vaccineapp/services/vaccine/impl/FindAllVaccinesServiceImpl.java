package com.modmed.workshop.vaccineapp.services.vaccine.impl;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.repository.VaccineRepository;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine.FindAllVaccinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindAllVaccinesServiceImpl implements FindAllVaccinesService {

    private final VaccineRepository vaccineRepository;

    @Autowired
    public FindAllVaccinesServiceImpl(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    @Override
    public List<Vaccine> findAll(){
        return this.vaccineRepository.findAll();
    }
}
