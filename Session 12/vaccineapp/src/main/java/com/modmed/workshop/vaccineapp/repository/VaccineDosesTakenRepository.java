package com.modmed.workshop.vaccineapp.repository;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VaccineDosesTakenRepository extends JpaRepository<VaccineDosesTaken, Long> {
    VaccineDosesTaken findByPatient_IdAndVaccine_Id(long patientId, long vaccineId);
}
