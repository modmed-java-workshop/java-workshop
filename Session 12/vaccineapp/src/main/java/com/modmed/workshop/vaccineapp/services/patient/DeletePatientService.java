package com.modmed.workshop.vaccineapp.services.patient;

import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;

public interface DeletePatientService {
    void delete(Long id) throws ResourceNotFoundException;
}
