package com.modmed.workshop.vaccineapp.services.vaccine.impl;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.dto.vaccine.UpdateVaccineDTO;
import com.modmed.workshop.vaccineapp.repository.VaccineRepository;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.vaccine.UpdateVaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UpdateVaccineServiceImpl implements UpdateVaccineService {

    private final VaccineRepository vaccineRepository;

    @Autowired
    public UpdateVaccineServiceImpl(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    @Override
    @Transactional
    public Vaccine update(Long id, UpdateVaccineDTO updateVaccineDTO) throws ResourceNotFoundException, InvalidResourceException{
        String description = updateVaccineDTO.getDescription();
        String brand = updateVaccineDTO.getBrand();
        if(brand == null || brand.isEmpty()){
            throw new InvalidResourceException("Invalid brand description");
        }
        if(description == null || description.isEmpty()){
            throw new InvalidResourceException("Invalid vaccine description");
        }
        Optional<Vaccine> existingVaccineOptional = this.vaccineRepository.findById(id);
        if(!existingVaccineOptional.isPresent()){
            throw new ResourceNotFoundException(String.format("Vaccine with id: %d not found", id));
        }
        Vaccine vaccine = existingVaccineOptional.get();
        vaccine.setBrand(brand);
        vaccine.setDescription(description);
        this.vaccineRepository.save(vaccine);
        return vaccine;
    }
}
