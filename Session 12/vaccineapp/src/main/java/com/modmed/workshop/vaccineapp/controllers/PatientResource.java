package com.modmed.workshop.vaccineapp.controllers;

import com.modmed.workshop.vaccineapp.converter.PatientConverter;
import com.modmed.workshop.vaccineapp.converter.VaccineDosesTakenConverter;
import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.MessageResponse;
import com.modmed.workshop.vaccineapp.dto.patient.CreatePatientDTO;
import com.modmed.workshop.vaccineapp.dto.patient.PatientDTO;
import com.modmed.workshop.vaccineapp.dto.patient.UpdatePatientDTO;
import com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken.VaccineDosesTakenDTO;
import com.modmed.workshop.vaccineapp.services.exception.InvalidResourceException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.vaccineapp.services.exception.ResourceNotFoundException;
import com.modmed.workshop.vaccineapp.services.patient.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("patients")
public class PatientResource {

    private final FindAllPatientsService findAllPatientsService;
    private final FindOnePatientService findOnePatientService;
    private final UpdatePatientService updatePatientService;
    private final CreatePatientService createPatientService;
    private final DeletePatientService deletePatientService;
    private final PatientConverter patientConverter;
    private final VaccineDosesTakenConverter vaccineDosesTakenConverter;

    @Autowired
    public PatientResource(FindAllPatientsService findAllPatientsService, FindOnePatientService findOnePatientService,
                           UpdatePatientService updatePatientService, CreatePatientService createPatientService,
                           DeletePatientService deletePatientService, PatientConverter patientConverter) {
        this.findAllPatientsService = findAllPatientsService;
        this.findOnePatientService = findOnePatientService;
        this.updatePatientService = updatePatientService;
        this.createPatientService = createPatientService;
        this.deletePatientService = deletePatientService;
        this.patientConverter = patientConverter;
        this.vaccineDosesTakenConverter = new VaccineDosesTakenConverter();

    }

    @GetMapping("")
    public List<PatientDTO> getAllPatients(){
        return findAllPatientsService.findAllPatients().stream()
                .map(patientConverter::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public PatientDTO findOne(@PathVariable Long id) throws ResourceNotFoundException {
        return this.patientConverter.fromEntity(this.findOnePatientService
                .findOne(id));
    }

    @PostMapping("")
    public PatientDTO create(@RequestBody CreatePatientDTO createPatientDTO) throws InvalidResourceException,
            ResourceAlreadyExistsException {
        return this.patientConverter.fromEntity(this.createPatientService
                .create(createPatientDTO));
    }

    @PutMapping("/{id}")
    public PatientDTO update(@PathVariable Long id, @RequestBody UpdatePatientDTO updatePatientDTO) throws InvalidResourceException,
            ResourceNotFoundException {
        return this.patientConverter.fromEntity(this.updatePatientService
                .update(id, updatePatientDTO));
    }

    @DeleteMapping("/{id}")
    public MessageResponse delete(@PathVariable Long id) throws ResourceNotFoundException {
        this.deletePatientService.delete(id);
        return new MessageResponse("Patient deleted successfully");
    }

    @GetMapping("/{id}/vaccines-doses-taken")
    public List<VaccineDosesTakenDTO> getPatientVaccinesDosesTaken(@PathVariable long id) throws ResourceNotFoundException {
        Patient patient = this.findOnePatientService.findOne(id);
        return patient.getDosageTaken().stream()
                .map(this.vaccineDosesTakenConverter::fromEntity)
                .collect(Collectors.toList());
    }

}
