package com.modmed.workshop.vaccineapp.services.exception;

public class AppException extends Exception{
    public AppException(String errorMessage){
        super(errorMessage);
    }
}
