package com.modmed.workshop.vaccineapp.dto.vaccine_doses_taken;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UpdateVaccineDosesTakenDTO implements Serializable {
    private String takenAt;
}
