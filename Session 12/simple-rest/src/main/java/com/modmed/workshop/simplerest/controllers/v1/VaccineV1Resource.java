package com.modmed.workshop.simplerest.controllers.v1;

import com.modmed.workshop.simplerest.domain.Vaccine;
import com.modmed.workshop.simplerest.repository.VaccineRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("vaccines")
public class VaccineV1Resource {

    @Autowired
    VaccineRepository vaccineRepository;

    @GetMapping("")
    public List<Vaccine> getAllVaccines() {
        return this.vaccineRepository.findAll();
    }

    @GetMapping("/{id}")
    public Vaccine findOne(@PathVariable Long id) {
        return this.vaccineRepository.findById(id).orElse(null);
    }

    @PostMapping("")
    public Vaccine create(@RequestBody Vaccine vaccine) {
        return this.vaccineRepository.save(vaccine);
    }

}
