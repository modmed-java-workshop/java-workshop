package com.modmed.workshop.simplerest.repository;

import com.modmed.workshop.simplerest.domain.Vaccine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VaccineRepository extends JpaRepository<Vaccine, Long> {
}

