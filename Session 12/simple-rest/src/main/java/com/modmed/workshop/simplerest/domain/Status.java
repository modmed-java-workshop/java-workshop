package com.modmed.workshop.simplerest.domain;

public enum Status {
    SICK, HEALTHY
}
