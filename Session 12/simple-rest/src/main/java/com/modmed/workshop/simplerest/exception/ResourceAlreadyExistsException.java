package com.modmed.workshop.simplerest.exception;

public class ResourceAlreadyExistsException extends AppException {
    public ResourceAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }
}
