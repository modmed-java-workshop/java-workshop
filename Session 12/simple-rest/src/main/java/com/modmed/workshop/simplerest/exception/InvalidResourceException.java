package com.modmed.workshop.simplerest.exception;

public class InvalidResourceException extends AppException {
    public InvalidResourceException(String errorMessage) {
        super(errorMessage);
    }
}
