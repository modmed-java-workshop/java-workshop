package com.modmed.workshop.simplerest.service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.modmed.workshop.simplerest.controllers.v1.VaccineV1Resource;
import com.modmed.workshop.simplerest.controllers.v3.PatientV3Resource;
import com.modmed.workshop.simplerest.domain.Patient;
import com.modmed.workshop.simplerest.domain.Vaccine;
import com.modmed.workshop.simplerest.dto.PatientDTO;
import com.modmed.workshop.simplerest.dto.VaccineDTO;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

@Service
public class LinkProcessorService {

    public CollectionModel<PatientDTO> addPatientLinks(List<Patient> patients) {
        List<PatientDTO> patientsDTO = patients.stream().map(this::cleanAndAddLinks).toList();

        Link link = linkTo(PatientV3Resource.class).withSelfRel();
        return CollectionModel.of(patientsDTO, link);
    }

    private PatientDTO cleanAndAddLinks(Patient patient) {
        var cleanPatient = new PatientDTO();
        cleanPatient.setId(patient.getId());
        cleanPatient.setDni(patient.getDni());
        cleanPatient.setFullName(patient.getFullName());
        cleanPatient.setStatus(patient.getStatus());
        cleanPatient.setNote(patient.getNote());
        cleanPatient.setDosageTaken(new HashSet<>());
        return addLinks(cleanPatient);
    }

    public PatientDTO addPatientLinks(Patient patient) {
        var cleanPatient = new PatientDTO();
        cleanPatient.setId(patient.getId());
        cleanPatient.setDni(patient.getDni());
        cleanPatient.setFullName(patient.getFullName());
        cleanPatient.setStatus(patient.getStatus());
        cleanPatient.setNote(patient.getNote());
        cleanPatient.setDosageTaken(getVaccinesDTO(patient.getDosageTaken()));
        return addLinks(cleanPatient);
    }

    private Set<VaccineDTO> getVaccinesDTO(Set<Vaccine> dosageTaken) {
        return dosageTaken.stream().map( vaccine -> {
            var vaccinesDTO = new VaccineDTO();
            vaccinesDTO.setId(vaccine.getId());
            vaccinesDTO.setDescription(vaccinesDTO.getDescription());
            vaccinesDTO.setName(vaccinesDTO.getName());
            return  vaccinesDTO;
        }).collect(Collectors.toSet());
    }

    public PatientDTO addLinks(PatientDTO patient) {
        patient.add(linkTo(methodOn(PatientV3Resource.class).findOne(patient.getId())).withSelfRel());
        patient.getDosageTaken().forEach(this::addVaccineLink);
        return patient;
    }

    private void addVaccineLink(VaccineDTO vaccine) {
        vaccine.add(linkTo(methodOn(VaccineV1Resource.class).findOne(vaccine.getId())).withSelfRel());
    }
}
