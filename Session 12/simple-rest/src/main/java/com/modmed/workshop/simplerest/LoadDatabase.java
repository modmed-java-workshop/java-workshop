package com.modmed.workshop.simplerest;

import com.modmed.workshop.simplerest.domain.Patient;
import com.modmed.workshop.simplerest.domain.Status;
import com.modmed.workshop.simplerest.domain.Vaccine;
import com.modmed.workshop.simplerest.repository.PatientRepository;
import com.modmed.workshop.simplerest.repository.VaccineRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(PatientRepository patientRepository, VaccineRepository vaccineRepository) {
        return args -> {
            log.info("Preloading " + patientRepository.save(getPedro()));
            log.info("Preloading " + patientRepository.save(getAna()));
            log.info("Preloading " + vaccineRepository.save(getVaccine("Polio", "develop in in 1950")));
            log.info("Preloading " + vaccineRepository.save(getVaccine("BCG vaccine",
                    "develop in in 1921, used against tuberculosis ")));
        };
    }

    private Vaccine getVaccine(String name, String description) {
        Vaccine vaccine = new Vaccine();
        vaccine.setName(name);
        vaccine.setDescription(description);
        return vaccine;
    }

    private Patient getPedro() {
        Patient patient = new Patient();
        patient.setFullName("Pedro Guzman");
        patient.setDni("1");
        patient.setStatus(Status.HEALTHY);
        return patient;
    }

    private Patient getAna() {
        Patient patient = new Patient();
        patient.setFullName("Ana Carrera");
        patient.setDni("2");
        patient.setStatus(Status.HEALTHY);
        return patient;
    }
}