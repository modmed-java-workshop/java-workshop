package com.modmed.workshop.simplerest.exception;

public class AppException extends Exception{
    public AppException(String errorMessage){
        super(errorMessage);
    }
}
