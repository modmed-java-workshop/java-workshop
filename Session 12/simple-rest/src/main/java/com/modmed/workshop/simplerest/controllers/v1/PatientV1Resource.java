package com.modmed.workshop.simplerest.controllers.v1;

import com.modmed.workshop.simplerest.domain.Patient;
import com.modmed.workshop.simplerest.repository.PatientRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("patients")
public class PatientV1Resource {

    @Autowired
    PatientRepository patientRepository;

    @GetMapping("")
    public List<Patient> getAllPatients(){
        return this.patientRepository.findAll();
    }

    @GetMapping("/{id}")
    public Patient findOne(@PathVariable Long id) {
        return this.patientRepository.findById(id).orElse( null);
    }

    @PostMapping("")
    public Patient create(@RequestBody Patient patient) {
        return this.patientRepository.save(patient);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        this.patientRepository.deleteById(id);
    }


}
