package com.modmed.workshop.simplerest.service;

import com.modmed.workshop.simplerest.domain.Patient;
import com.modmed.workshop.simplerest.domain.Vaccine;
import com.modmed.workshop.simplerest.exception.InvalidResourceException;
import com.modmed.workshop.simplerest.exception.ResourceNotFoundException;
import com.modmed.workshop.simplerest.repository.PatientRepository;
import com.modmed.workshop.simplerest.repository.VaccineRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientVaccineService {

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    VaccineRepository vaccineRepository;

    @Autowired
    PatientService patientService;

    public Patient addVaccines(Long patientId, List<Vaccine> vaccines) throws ResourceNotFoundException {
        Patient patient = patientService.findOne(patientId);
        validateVaccines(vaccines);
        patient.getDosageTaken().addAll(vaccines);
        patientRepository.save(patient);
        return patient;
    }

    private void validateVaccines(List<Vaccine> vaccines) throws ResourceNotFoundException {
        for (Vaccine vaccine : vaccines) {
            if (!vaccineRepository.existsById(vaccine.getId())) {
                throw new ResourceNotFoundException(String.format("Vaccine with id: %d not found", vaccine.getId()));
            }
        }
    }
}
