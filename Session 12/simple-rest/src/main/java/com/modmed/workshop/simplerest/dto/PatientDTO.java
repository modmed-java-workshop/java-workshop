package com.modmed.workshop.simplerest.dto;

import com.modmed.workshop.simplerest.domain.Status;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
public class PatientDTO extends RepresentationModel<PatientDTO> {
    private Long id;
    private String dni;

    private String fullName;
    private Status status;
    private String note;

    private Set<VaccineDTO> dosageTaken;
}
