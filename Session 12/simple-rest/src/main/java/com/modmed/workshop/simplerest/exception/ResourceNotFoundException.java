package com.modmed.workshop.simplerest.exception;

public class ResourceNotFoundException extends AppException {
    public ResourceNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
