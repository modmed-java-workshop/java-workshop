package com.modmed.workshop.simplerest.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
public class VaccineDTO extends RepresentationModel<VaccineDTO> {

    private Long id;

    private String name;
    private String description;


}
