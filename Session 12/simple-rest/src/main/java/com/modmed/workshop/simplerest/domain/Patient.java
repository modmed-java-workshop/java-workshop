package com.modmed.workshop.simplerest.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Patient {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String dni;

    private String fullName;
    private Status status;
    private String note;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Vaccine> dosageTaken;
}
