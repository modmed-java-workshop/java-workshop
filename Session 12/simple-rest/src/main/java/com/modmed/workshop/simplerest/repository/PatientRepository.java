package com.modmed.workshop.simplerest.repository;


import com.modmed.workshop.simplerest.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient findByDni(String dni);
}
