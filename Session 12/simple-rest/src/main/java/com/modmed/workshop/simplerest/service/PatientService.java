package com.modmed.workshop.simplerest.service;


import com.modmed.workshop.simplerest.domain.Patient;
import com.modmed.workshop.simplerest.domain.Status;
import com.modmed.workshop.simplerest.exception.InvalidResourceException;
import com.modmed.workshop.simplerest.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.simplerest.exception.ResourceNotFoundException;
import com.modmed.workshop.simplerest.repository.PatientRepository;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> findAll() {
        return patientRepository.findAll();
    }

    public Patient findOne(Long id) throws ResourceNotFoundException {
        Optional<Patient> patient = this.patientRepository.findById(id);
        if (!patient.isPresent()) {
            throw new ResourceNotFoundException(String.format("Patient with id: %d not found", id));
        }
        return patient.get();
    }

    public Patient create(Patient patient) throws InvalidResourceException, ResourceAlreadyExistsException {
        this.validateNewPatient(patient);
        return this.patientRepository.save(patient);
    }

    private void validateNewPatient(Patient patient) throws InvalidResourceException, ResourceAlreadyExistsException {
        String dni = patient.getDni();
        if (dni == null || dni.isEmpty()) {
            throw new InvalidResourceException("Invalid patient dni");
        }
        Patient existingPatient = this.patientRepository.findByDni(dni);
        if (existingPatient != null) {
            throw new ResourceAlreadyExistsException(String.format("A patient with dni: %s already exists", dni));
        }
    }

    public Patient replace(Long id, Patient patientToUpdate) throws InvalidResourceException, ResourceNotFoundException {
        validateInfoAndGetPatient(patientToUpdate);
        Patient patient = findOne(id);
        patient.setFullName(patientToUpdate.getFullName());
        patient.setStatus(patientToUpdate.getStatus());
        patient.setNote(patientToUpdate.getNote());
        this.patientRepository.save(patient);
        return patient;
    }

    public Patient update(Long id, Patient patientToUpdate) throws ResourceNotFoundException {
        Patient patient = findOne(id);
        if (!Strings.isEmpty(patientToUpdate.getFullName())) {
            patient.setFullName(patientToUpdate.getFullName());
        }
        if (patientToUpdate.getStatus() != null) {
            patient.setStatus(patientToUpdate.getStatus());
        }
        if (!Strings.isEmpty(patientToUpdate.getNote())) {
            patient.setNote(patientToUpdate.getNote());
        }
        this.patientRepository.save(patient);
        return patient;
    }

    private void validateInfoAndGetPatient(Patient patientToUpdate) throws InvalidResourceException {
        String fullName = patientToUpdate.getFullName();
        Status status = patientToUpdate.getStatus();
        boolean invalidFullName = fullName == null || fullName.isEmpty();
        boolean invalidStatus = status == null;
        if (invalidFullName || invalidStatus) {
            throw new InvalidResourceException("Invalid patient data");
        }
    }
}
