package com.modmed.workshop.simplerest.controllers.v3;


import com.modmed.workshop.simplerest.domain.Patient;
import com.modmed.workshop.simplerest.domain.Vaccine;
import com.modmed.workshop.simplerest.dto.PatientDTO;
import com.modmed.workshop.simplerest.exception.InvalidResourceException;
import com.modmed.workshop.simplerest.exception.ResourceAlreadyExistsException;
import com.modmed.workshop.simplerest.exception.ResourceNotFoundException;
import com.modmed.workshop.simplerest.service.LinkProcessorService;
import com.modmed.workshop.simplerest.service.PatientService;
import com.modmed.workshop.simplerest.service.PatientVaccineService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v3/patients")
public class PatientV3Resource {

    @Autowired
    PatientService patientService;

    @Autowired
    PatientVaccineService patientVaccineService;

    @Autowired
    LinkProcessorService linkProcessorService;

    @GetMapping("")
    public ResponseEntity<CollectionModel<PatientDTO>> getAllPatients() {
        return ResponseEntity.ok(this.linkProcessorService.addPatientLinks(this.patientService.findAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity findOne(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(this.linkProcessorService.addPatientLinks(this.patientService.findOne(id)));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @PostMapping("")
    public ResponseEntity create(@RequestBody Patient patient) {
        try {
            return ResponseEntity.status(201).body(this.linkProcessorService.addPatientLinks(this.patientService.create(patient)));
        } catch (InvalidResourceException | ResourceAlreadyExistsException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity fullUpdate(@PathVariable Long id, @RequestBody Patient patient) {
        try {
            return ResponseEntity.ok(this.linkProcessorService.addPatientLinks(this.patientService.replace(id, patient)));
        } catch (InvalidResourceException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity partialUpdate(@PathVariable Long id, @RequestBody Patient patient) {
        try {
            return ResponseEntity.ok(this.linkProcessorService.addPatientLinks(this.patientService.update(id, patient)));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @PostMapping("/{id}/vaccines")
    public ResponseEntity partialUpdate(@PathVariable Long id, @RequestBody List<Vaccine> vaccines) {
        try {
            return ResponseEntity.ok(this.linkProcessorService.addPatientLinks(this.patientVaccineService.addVaccines(id, vaccines)));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

}
