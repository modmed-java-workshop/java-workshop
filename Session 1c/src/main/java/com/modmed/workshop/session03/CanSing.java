package com.modmed.workshop.session03;

public interface CanSing {
    String sing();

    static String color(){
        return "black";
    }

    default String jump(){
        return "Jumping";
    }
}
