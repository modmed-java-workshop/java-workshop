package com.modmed.workshop.session03;

public class Bird implements CanSing {
    static String id(){
        return "20";
    }

    @Override
    public String sing() {
        return "piopio";
    }

    @Override
    public String jump() {
        return "INSTANCE JUMP";
    }

    public static void main(String[] args) {
        System.out.println(CanSing.color());
        System.out.println(new Bird().jump());


    }
}
