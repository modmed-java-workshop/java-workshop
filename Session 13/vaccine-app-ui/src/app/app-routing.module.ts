import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
    { path: '', component: WelcomeComponent },
    { path: 'patients', loadChildren: () => import('./patients/patients.module').then(m => m.PatientsModule) },
    { path: 'vaccines', loadChildren: () => import('./vaccines/vaccines.module').then(m => m.VaccinesModule) },
    { path: 'examples', loadChildren: () => import('./examples/examples.module').then(m => m.ExamplesModule) }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
