import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Vaccine } from './vaccine.model';

@Injectable({
  providedIn: 'root'
})
export class VaccineService {

  constructor(private http: HttpClient) { }

  getVaccines(): Observable<Array<Vaccine>> {
    console.log('Get Vaccines');
    return this.http.get<Array<Vaccine>>('http://localhost:8081/vaccines');
  }

  getVaccine(vaccineId: number): Observable<Vaccine> {
    return this.http.get<Vaccine>(`http://localhost:8081/vaccines/${vaccineId}`);
  }

  saveVaccine(vaccine: Vaccine): Observable<Vaccine> {
    return this.http.post<Vaccine>('http://localhost:8081/vaccines', vaccine);
  }

  deleteVaccine(vaccine: Vaccine): Observable<string> {
    return this.http.delete<string>(`http://localhost:8081/vaccines/${vaccine.id}`);
  }
}
