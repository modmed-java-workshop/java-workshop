import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../services/logger.service';
import { Vaccine } from './vaccine.model';
import { VaccineService } from './vaccine.service';

@Component({
  selector: 'app-vaccine',
  templateUrl: './vaccine.component.html',
  styleUrls: ['./vaccine.component.less']
})
export class VaccineComponent implements OnInit {

  vaccineList: Array<Vaccine> = [];

  newVaccine: Vaccine = {};

  constructor(private vaccineService: VaccineService, private logger: LoggerService) { }

  ngOnInit(): void {
    this.displayVaccines();
  }

  saveVaccine() {
    this.vaccineService.saveVaccine(this.newVaccine).subscribe(
      () => { this.displayVaccines() },
      (error) => {
        this.logger.error(error);
      }
    );
  }

  clearVaccine() {
    this.newVaccine = {};
  }

  deleteVaccine(vaccine: Vaccine): void {
    this.vaccineService.deleteVaccine(vaccine).subscribe(
      () => this.displayVaccines()
    );
  }

  private displayVaccines(): void {
    this.vaccineService.getVaccines().subscribe(
      data => this.vaccineList = data
    );
  }

}
