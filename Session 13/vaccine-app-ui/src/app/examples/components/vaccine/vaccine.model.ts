export interface Vaccine {
  id?: number,
  brand?: string,
  description?: string,
  dosageTaken?: Array<any>
}
