import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { VaccineComponent } from './components/vaccine/vaccine.component';
import { LoggerService } from './services/logger.service';

const routes: Routes = [
  { path: '', component: VaccineComponent },
];

@NgModule({
  declarations: [
      VaccineComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule
  ],
  providers: [
      LoggerService
  ]

})
export class ExamplesModule { }
