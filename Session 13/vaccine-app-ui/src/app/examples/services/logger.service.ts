import { Injectable } from '@angular/core';

export class LoggerService {

  constructor() { }

  log(msg: any) {
    console.log(msg);
  }

  error(error: any) {
    console.log('Error!: ' + JSON.stringify(error));
    // console.log('Status: ' + error.status);
    // console.log('Status Text:'  + error.statusText);
    // console.log('Message: ' + error.error.message);
    alert(error.error.message);

  }

  warn(msg: any) {
    console.warn(msg);
  }

}
