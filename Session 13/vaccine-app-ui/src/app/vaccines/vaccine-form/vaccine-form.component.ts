import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientDto } from '../../patients/patient-dto.model';
import { VaccineDto } from '../vaccine-dto';
import { VaccineDynamicFormService } from '../vaccine-dynamic-form.service';
import { VaccineService } from '../vaccine.service';

@Component({
    selector: 'app-vaccines-form',
    templateUrl: './vaccine-form.component.html',
    styleUrls: ['./vaccine-form.component.less']
})
export class VaccineFormComponent implements OnInit {
    form: FormGroup = new FormGroup({
        id: new FormControl({ value: undefined, disabled: true }),
        brand: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.required]),
    });

    constructor(private readonly vaccineService: VaccineService,
                private readonly vaccineDynamicFormService: VaccineDynamicFormService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit(): void {
        this.form = this.vaccineDynamicFormService.newFormGroup();
        const id = Number(this.route.snapshot.paramMap.get('id'));
        let vaccine: VaccineDto;
        if (id) {
            this.vaccineService.getById(id)
                .subscribe(value => {
                        vaccine = value;
                    },
                    () => {},
                    () => this.form = this.vaccineDynamicFormService.fromVaccineOrNew(vaccine));
        }
    }

    submit() {
        let copy = Object.assign({}, this.form.getRawValue());
        if (copy.id) {
            this.vaccineService.update(copy)
                .subscribe(v => {
                    alert(`Vaccine ${v.brand} ${v.description} updated.`);
                    this.router.navigate(['/vaccines'], { relativeTo: this.route }).then(() => {});
                }, error => alert(error.error.message), () => {
                });
        } else {
            this.vaccineService.create(copy)
                .subscribe(
                    v => {
                        alert(`Vaccine ${v.brand} ${v.description} created.`);
                        this.router.navigate(['/vaccines'], { relativeTo: this.route }).then(() => {});
                    },
                    error => alert(error.error.message)
                );
        }
    }
}
