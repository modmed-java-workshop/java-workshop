import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { VaccineDto } from './vaccine-dto';

@Injectable({
  providedIn: 'root'
})
export class VaccineDynamicFormService {

  constructor() { }

  fromVaccineOrNew(vaccine: VaccineDto): FormGroup {
    const from: VaccineDto = vaccine ? vaccine : new VaccineDtoImpl();
    const formGroup = this.newFormGroup();
    formGroup.patchValue(from);
    return formGroup;
  }

  newFormGroup() {
    return new FormGroup({
      id: new FormControl({ value: undefined, disabled: true }),
      brand: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });
  }
}

class VaccineDtoImpl implements VaccineDto {
  id?: number;
  brand: string = '';
  description: string = '';
}
