export interface VaccineDto {
    id?: number;
    brand: string;
    description: string;
    dosageTaken?: Array<any>;
}
