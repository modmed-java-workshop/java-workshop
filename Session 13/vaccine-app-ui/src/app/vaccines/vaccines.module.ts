import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { VaccineFormComponent } from './vaccine-form/vaccine-form.component';
import { VaccinesTableComponent } from './vaccines-table/vaccines-table.component';

const routes: Routes = [
    { path: '', component: VaccinesTableComponent },
    { path: 'editor', component: VaccineFormComponent },
    { path: 'editor/:id', component: VaccineFormComponent }
];

@NgModule({
    declarations: [
        VaccinesTableComponent,
        VaccineFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        ReactiveFormsModule
    ]
})
export class VaccinesModule {
}
