import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VaccineDto } from './vaccine-dto';

@Injectable({
    providedIn: 'root'
})
export class VaccineService {
    private baseUrl = 'http://localhost:8081/vaccines';

    constructor(private readonly httpClient: HttpClient) {
    }

    getAll(): Observable<Array<VaccineDto>> {
        return this.httpClient.get<Array<VaccineDto>>(`${this.baseUrl}`);
    }

    getById(id: number): Observable<VaccineDto> {
        return this.httpClient.get<VaccineDto>(`${this.baseUrl}/${id}`);
    }

    create(vaccine: VaccineDto): Observable<VaccineDto> {
        return this.httpClient.post<VaccineDto>(`${this.baseUrl}`, vaccine);
    }

    update(vaccine: VaccineDto): Observable<VaccineDto> {
        return this.httpClient.put<VaccineDto>(`${this.baseUrl}/${vaccine.id}`, vaccine);
    }

    delete(id: number): Observable<any> {
        return this.httpClient.delete<any>(`${this.baseUrl}/${id}`);
    }
}
