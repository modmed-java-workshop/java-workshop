import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VaccineDto } from '../vaccine-dto';
import { VaccineService } from '../vaccine.service';

@Component({
    selector: 'app-vaccines',
    templateUrl: './vaccines-table.component.html',
    styleUrls: ['./vaccines-table.component.less']
})
export class VaccinesTableComponent implements OnInit {
    vaccines: Array<VaccineDto> = [];

    constructor(private readonly vaccineService: VaccineService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit(): void {
        this.vaccineService.getAll()
            .subscribe(v => this.vaccines = v)
    }

    delete(vaccine: VaccineDto, event: Event) {
        event.preventDefault();
        if (confirm(`Are you sure to delete the Vaccine ${vaccine.brand} ${vaccine.description}`)) {
            this.vaccineService.delete(vaccine.id!)
                .subscribe(({ message }) => alert(message));
        }
    }

    edit(vaccine: VaccineDto, event: Event) {
        event.preventDefault();
        this.router.navigate(['/vaccines/editor/', vaccine.id], { relativeTo: this.route }).then(() => {});
    }
}
