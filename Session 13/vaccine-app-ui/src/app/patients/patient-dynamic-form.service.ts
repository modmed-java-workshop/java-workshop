import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PatientDto } from './patient-dto.model';

@Injectable({
  providedIn: 'root'
})
export class PatientDynamicFormService {

  constructor() { }

  fromPatientOrNew(patient: PatientDto): FormGroup {
    const from: PatientDto = patient ? patient : new PatientDtoImpl();
    const formGroup = this.newFormGroup();
    if (from.hasOwnProperty('dateOfBirthStr')) {
      from.dateOfBirth = from.dateOfBirthStr;
      delete from.dateOfBirthStr;
    }
    formGroup.patchValue(from);
    return formGroup;
  }

  newFormGroup() {
    return new FormGroup({
      id: new FormControl({ value: undefined, disabled: true }),
      dni: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl(undefined, [Validators.required]),
      imageUrl: new FormControl('', [Validators.required]),
    });
  }
}

class PatientDtoImpl implements PatientDto {
  dateOfBirthStr: string = '';
  dni: string = '';
  firstName: string = '';
  gender: string = '';
  imageUrl: string = '';
  lastName: string = '';
}
