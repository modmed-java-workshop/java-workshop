import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientDto } from '../patient-dto.model';
import { PatientService } from '../patient.service';

@Component({
    selector: 'app-patients',
    templateUrl: './patients-table.component.html',
    styleUrls: ['./patients-table.component.less']
})
export class PatientsTableComponent implements OnInit {
    patients: Array<PatientDto> = [];

    constructor(private readonly patientService: PatientService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit(): void {
        this.patientService.getAll()
            .subscribe(value => {
                this.patients = value;
            })
    }

    delete(patient: PatientDto, event: Event): void {
        event.preventDefault();
        if (confirm(`Are you sure to delete the Patient ${patient.firstName} ${patient.lastName}`)) {
            this.patientService.delete(patient.id!)
                .subscribe(({ message }) => alert(message));
        }
    }

    edit(patient: PatientDto, event: Event) {
        event.preventDefault();
        this.router.navigate(['/patients/editor/', patient.id], { relativeTo: this.route }).then(() => {});
    }
}
