import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PatientDto } from './patient-dto.model';

@Injectable({
    providedIn: 'root'
})
export class PatientService {
    private baseUrl = 'http://localhost:8081/patients';

    constructor(private readonly httpClient: HttpClient) {
    }

    getAll(): Observable<Array<PatientDto>> {
        return this.httpClient.get<Array<PatientDto>>(`${this.baseUrl}`);
    }

    getById(id: number): Observable<PatientDto> {
        return this.httpClient.get<PatientDto>(`${this.baseUrl}/${id}`);
    }

    create(patient: PatientDto): Observable<PatientDto> {
        return this.httpClient.post<PatientDto>(`${this.baseUrl}`, patient);
    }

    update(patient: PatientDto): Observable<PatientDto> {
        return this.httpClient.put<PatientDto>(`${this.baseUrl}/${patient.id}`, patient);
    }

    delete(id: number): Observable<any> {
        return this.httpClient.delete<any>(`${this.baseUrl}/${id}`);
    }
}
