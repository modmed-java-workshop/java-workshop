import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { PatientsTableComponent } from './patients-table/patients-table.component';

const routes: Routes = [
    { path: '', component: PatientsTableComponent },
    { path: 'editor/', component: PatientFormComponent },
    { path: 'editor/:id', component: PatientFormComponent }
];

@NgModule({
    declarations: [
        PatientsTableComponent,
        PatientFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        ReactiveFormsModule
    ]
})
export class PatientsModule {
}
