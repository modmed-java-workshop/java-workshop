import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Gender } from '../gender';
import { PatientDto } from '../patient-dto.model';
import { PatientDynamicFormService } from '../patient-dynamic-form.service';
import { PatientService } from '../patient.service';

@Component({
    selector: 'app-patients-form',
    templateUrl: './patient-form.component.html',
    styleUrls: ['./patient-form.component.less']
})
export class PatientFormComponent implements OnInit {
    form!: FormGroup;
    genders = [Gender.FEMALE, Gender.MALE];

    constructor(private readonly patientService: PatientService,
                private readonly patientDynamicFormService: PatientDynamicFormService,
                private readonly route: ActivatedRoute,
                private readonly router: Router) {
    }


    ngOnInit(): void {
        this.form = this.patientDynamicFormService.newFormGroup();
        const id = Number(this.route.snapshot.paramMap.get('id'));
        let patient: PatientDto;
        if (id) {
            this.patientService.getById(id)
                .subscribe(value => {
                        patient = value;
                    },
                    () => {},
                    () => this.form = this.patientDynamicFormService.fromPatientOrNew(patient));
        }
    }

    submit() {
        let copy = Object.assign({}, this.form.getRawValue())
        copy.dosageTaken = [];
        if (copy.id) {
            this.patientService.update(copy)
                .subscribe(p => {
                    alert(`Patient ${p.firstName} ${p.lastName} updated.`);
                    this.router.navigate(['/patients'], { relativeTo: this.route }).then(() => {});
                }, error => alert(error.error.message), () => {});
        } else {
            this.patientService.create(copy)
                .subscribe(
                    p => {
                        alert(`Patient ${p.firstName} ${p.lastName} created.`);
                        this.router.navigate(['/patients'], { relativeTo: this.route }).then(() => {});
                    },
                    error => alert(error.error.message), () => {}
                );
        }
    }
}
