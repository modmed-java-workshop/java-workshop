export interface PatientDto {
    id?: number;
    dni: string;
    firstName: string;
    lastName: string;
    gender: string;
    dateOfBirthStr?: string;
    dateOfBirth?: string;
    imageUrl: string;
    dosageTaken?: Array<any>;
}
