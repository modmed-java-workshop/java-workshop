## Docker installation on Windows
As you may know, Docker is a container manager of several sort of platforms (db-engines, web servers, message queue servers, etc) to make our dev life easier, by enabling environment ready for too many platforms through the whole of the software development cycle until artifact deployment on any environment.

Since Docker arose from the Linux reign, installing Docker on Windows could be a little annoying. Therefore, by following these steps you can get Docker installed on Windows environment.

- Go and download [Docker Desktop on Windows](https://docs.docker.com/desktop/windows/install/)
- **Right click** and **Run as Administrator**
- After installation, you may get an error because WSL2 *Windows Subsystems for Linux* is either missing or not having a [recent update of WSL2](https://docs.microsoft.com/es-es/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)
- Install WSL2 and restart the Docker Desktop for Windows to get Docker Engine running nice and smooth :)