-- Patient
insert into patient
    (dni, first_name, last_name, gender, date_of_birth)
values ('13564654-7', 'Marta', 'Sanchez', 'FEMALE', PARSEDATETIME('05/25/1980', 'MM/DD/YYY'));

insert into patient
    (dni, first_name, last_name, gender, date_of_birth)
values ('20456987-6', 'Humberto', 'Lobos', 'MALE', PARSEDATETIME('11/04/1972', 'MM/DD/YYY'));

insert into patient
    (dni, first_name, last_name, gender, date_of_birth)
values ('5268987-K', 'Mirta', 'Lopez', 'FEMALE', PARSEDATETIME('04/13/1960', 'MM/DD/YYY'));

-- Vaccine
insert into vaccine
(brand)
values ('PFIZER');

insert into vaccine
(brand)
values ('SINOVAC');