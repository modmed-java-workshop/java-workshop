package com.modmed.workshop.vaccineapp.converter;

import com.modmed.workshop.vaccineapp.domain.Vaccine;
import com.modmed.workshop.vaccineapp.dto.VaccineDTO;
import com.modmed.workshop.vaccineapp.dto.VaccineDosesTakenDTO;

import java.util.stream.Collectors;

public class VaccineConverter extends AbstractConverter<Vaccine, VaccineDTO> {
    @Override
    public Vaccine fromDto(VaccineDTO dto, Vaccine entity) {
        return null;
    }

    @Override
    public VaccineDTO fromEntity(Vaccine entity, VaccineDTO dto) {
        VaccineDosesTakenConverter dosesConverter = new VaccineDosesTakenConverter();
        dto.setBrand(entity.getBrand());
        dto.setId(entity.getId());
        dto.setDosageTaken(entity.getDosageTaken().stream().map(d ->
                dosesConverter.fromEntity(d, new VaccineDosesTakenDTO())).collect(Collectors.toSet()));
        return dto;
    }
}
