package com.modmed.workshop.vaccineapp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class VaccineDosesTakenDTO implements Serializable {
    private Long id;
    private PatientDTO patient;
    private VaccineDTO vaccine;
    private String takenAtStr;
}
