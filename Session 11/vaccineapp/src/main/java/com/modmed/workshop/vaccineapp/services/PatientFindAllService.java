package com.modmed.workshop.vaccineapp.services;

import com.modmed.workshop.vaccineapp.domain.Patient;
import java.util.List;

public interface PatientFindAllService {
    List<Patient> findAllPatients();
}
