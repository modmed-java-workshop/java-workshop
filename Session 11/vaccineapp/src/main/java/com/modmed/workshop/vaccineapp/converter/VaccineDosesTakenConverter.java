package com.modmed.workshop.vaccineapp.converter;

import com.modmed.workshop.vaccineapp.domain.VaccineDosesTaken;
import com.modmed.workshop.vaccineapp.dto.PatientDTO;
import com.modmed.workshop.vaccineapp.dto.VaccineDTO;
import com.modmed.workshop.vaccineapp.dto.VaccineDosesTakenDTO;

public class VaccineDosesTakenConverter extends AbstractConverter<VaccineDosesTaken, VaccineDosesTakenDTO> {
    @Override
    public VaccineDosesTaken fromDto(VaccineDosesTakenDTO dto, VaccineDosesTaken entity) {
        return null;
    }

    @Override
    public VaccineDosesTakenDTO fromEntity(VaccineDosesTaken entity, VaccineDosesTakenDTO dto) {
        VaccineConverter vaccineConverter = new VaccineConverter();
        PatientConverter patientConverter = new PatientConverter();
        if (null != entity.getVaccine()) {
            dto.setVaccine(vaccineConverter.fromEntity(entity.getVaccine(), new VaccineDTO()));
        }
        dto.setId(entity.getId());
        if (null != entity.getPatient()) {
            dto.setPatient(patientConverter.fromEntity(entity.getPatient(), new PatientDTO()));
        }
        dto.setTakenAtStr(entity.getTakenAt().toString()); //This should be formatted to ISO (out of scope for now)
        return dto;
    }
}
