package com.modmed.workshop.vaccineapp.services.impl;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.repository.PatientRepository;
import com.modmed.workshop.vaccineapp.services.PatientFindAllService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientFindAllServiceImpl implements PatientFindAllService {

    @Autowired
    PatientRepository patientRepository;

    @Override
    public List<Patient> findAllPatients() {
        return patientRepository.findAll();
    }
}
