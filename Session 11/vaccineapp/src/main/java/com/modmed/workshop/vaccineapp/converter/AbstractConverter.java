package com.modmed.workshop.vaccineapp.converter;

public abstract class AbstractConverter<E, D> {

    public abstract E fromDto(D dto, E entity);

    public abstract D fromEntity(E entity, D dto);
}
