package com.modmed.workshop.vaccineapp.domain.enums;

public enum Gender {
    MALE, FEMALE
}
