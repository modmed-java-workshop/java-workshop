package com.modmed.workshop.vaccineapp.controllers;

import com.modmed.workshop.vaccineapp.converter.PatientConverter;
import com.modmed.workshop.vaccineapp.dto.PatientDTO;
import com.modmed.workshop.vaccineapp.services.PatientFindAllService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
public class PatientResource {

    @Autowired
    PatientFindAllService patientFindAllService;
    @Autowired
    PatientConverter patientConverter;

    @GetMapping(value = "/", produces = "application/json")
    public List<PatientDTO> getAllPatients() {
        return patientFindAllService.findAllPatients().stream()
                .map(patient -> patientConverter.fromEntity(patient, new PatientDTO()))
                .collect(Collectors.toList());
    }
}
