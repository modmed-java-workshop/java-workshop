package com.modmed.workshop.vaccineapp.repository;

import com.modmed.workshop.vaccineapp.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
}
