package com.modmed.workshop.vaccineapp.converter;

import com.modmed.workshop.vaccineapp.domain.Patient;
import com.modmed.workshop.vaccineapp.dto.PatientDTO;
import com.modmed.workshop.vaccineapp.dto.VaccineDosesTakenDTO;

import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class PatientConverter extends AbstractConverter<Patient, PatientDTO> {
    @Override
    public Patient fromDto(PatientDTO dto, Patient entity) {
        return null;
    }

    @Override
    public PatientDTO fromEntity(Patient entity, PatientDTO dto) {
        VaccineDosesTakenConverter dosesConverter = new VaccineDosesTakenConverter();
        dto.setDni(entity.getDni());
        dto.setDosageTaken(entity.getDosageTaken().stream().map(d ->
                dosesConverter.fromEntity(d, new VaccineDosesTakenDTO())).collect(Collectors.toSet()));
        dto.setFirstName(entity.getFirstName());
        dto.setGender(entity.getGender().toString());
        dto.setId(entity.getId());
        dto.setImageUrl(entity.getImageUrl());
        dto.setDateOfBirthStr(entity.getDateOfBirth().toString());
        return dto;
    }
}
