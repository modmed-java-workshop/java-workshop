package com.modmed.workshop.springbasics.di;

import com.modmed.workshop.springbasics.models.Staff;
import com.modmed.workshop.springbasics.services.xml.AppointmentSuiteService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Deprecated
public class UsingDIExample {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        Staff staff = context.getBean("assistant", Staff.class);
        staff.setFullName("Albert Einstein");
        // Get the bean and its dependency
        AppointmentSuiteService appointmentSuiteService = context.getBean("appointmentSuiteService", AppointmentSuiteService.class);
        appointmentSuiteService.createAppointment(staff);
        System.out.println(staff);
    }
}
