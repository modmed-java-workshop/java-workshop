package com.modmed.workshop.springbasics.services.annotated;

import com.modmed.workshop.springbasics.models.Staff;

public interface AppointmentReminderWorkflowService {
    void startWorkflow(Staff staff);
}
