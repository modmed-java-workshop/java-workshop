package com.modmed.workshop.springbasics.hibernate.services;

import com.modmed.workshop.springbasics.hibernate.entities.School;
import com.modmed.workshop.springbasics.hibernate.repository.SchoolRespository;
import com.modmed.workshop.springbasics.hibernate.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SchoolCreationServiceImpl implements SchoolCreationService {
    @Autowired
    SchoolRespository schoolRespository;

    @Autowired
    StudentRepository studentRepository;

    @Override
    public void create(School school) {
        school.getStudents().forEach(student -> studentRepository.add(student));
        schoolRespository.add(school);
    }
}
