package com.modmed.workshop.springbasics.services.annotated;

import com.modmed.workshop.springbasics.models.Staff;
import org.springframework.stereotype.Component;

@Component
public class Reminder {
    public void remind(Staff staff) {
        System.out.printf("Hi, Good evening! %s. You have a reminder for %s\n", staff.getFullName(), staff.getAppointment());
    }
}