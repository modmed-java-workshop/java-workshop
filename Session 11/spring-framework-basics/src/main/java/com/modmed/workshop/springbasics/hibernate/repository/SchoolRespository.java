package com.modmed.workshop.springbasics.hibernate.repository;

import com.modmed.workshop.springbasics.hibernate.entities.School;

public interface SchoolRespository {
    void add(School school);
}
