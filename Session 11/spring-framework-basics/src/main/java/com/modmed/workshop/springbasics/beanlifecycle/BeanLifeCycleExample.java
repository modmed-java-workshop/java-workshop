package com.modmed.workshop.springbasics.beanlifecycle;

import com.modmed.workshop.springbasics.models.Staff;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Deprecated
public class BeanLifeCycleExample {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        Staff drRaymond = context.getBean("assistant", Staff.class);
        drRaymond.setFullName("Raymond Perez");
        drRaymond.setAppointment("New Podiatry Appointment at 12:00");
        System.out.println(drRaymond);

        Staff anotherDoc = context.getBean("assistant", Staff.class);
        System.out.println(anotherDoc);

        System.out.println("Are they the same Doc bean? " + drRaymond.equals(anotherDoc));

        context.close();
    }
}
