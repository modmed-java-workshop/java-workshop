package com.modmed.workshop.springbasics.services.xml;

import com.modmed.workshop.springbasics.models.Staff;
import org.springframework.beans.factory.annotation.Autowired;

public class AppointmentSuiteServiceImpl implements AppointmentSuiteService {

//    @Value("${appointment.timeunit}")
//    private String appointmentTimeUnit;
//
//    @Value("${appointment.duration}")
//    private int appointmentDuration;

    private final AppointmentAssignerService appointmentAssignerService;

    public AppointmentSuiteServiceImpl(AppointmentAssignerService appointmentAssignerService) {
        this.appointmentAssignerService = appointmentAssignerService;
    }

    @Override
    public void createAppointment(Staff staff) {
        this.appointmentAssignerService.assign(staff);
    }
}
