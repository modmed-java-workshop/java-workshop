package com.modmed.workshop.springbasics.hibernate.repository;

import com.modmed.workshop.springbasics.hibernate.entities.School;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SchoolRepositoryImpl implements SchoolRespository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(School school) {
        sessionFactory.getCurrentSession().save(school);
    }
}
