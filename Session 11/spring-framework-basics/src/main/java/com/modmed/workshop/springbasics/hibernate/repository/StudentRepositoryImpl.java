package com.modmed.workshop.springbasics.hibernate.repository;

import com.modmed.workshop.springbasics.hibernate.entities.Student;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(Student student) {
        sessionFactory.getCurrentSession().save(student);
    }

    @Override
    public Student getStudent(int id) {
        return sessionFactory.getCurrentSession().get(Student.class, id);
    }
}
