package com.modmed.workshop.springbasics.config;

import com.modmed.workshop.springbasics.models.Staff;
import com.modmed.workshop.springbasics.services.annotated.AppointmentReminderWorkflowService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppConfigBeanWithDIExample {
    public static void main(String[] args) {
        // load the spring configuration java class
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        // Using default Bean Id
        AppointmentReminderWorkflowService appointmentReminderWorkflowService =
                context.getBean(AppointmentReminderWorkflowService.class);
        Staff staff = context.getBean("assistant", Staff.class);
        staff.setFullName("Raymond Perez");
        staff.setAppointment("New Podiatry Appointment at 12:00");

        appointmentReminderWorkflowService.startWorkflow(staff);

        context.close();
    }
}
