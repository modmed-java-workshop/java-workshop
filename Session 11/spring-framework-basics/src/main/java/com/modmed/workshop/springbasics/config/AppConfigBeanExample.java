package com.modmed.workshop.springbasics.config;

import com.modmed.workshop.springbasics.models.Staff;
import com.modmed.workshop.springbasics.services.xml.AppointmentSuiteService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Deprecated
public class AppConfigBeanExample {
    public static void main(String[] args) {
        // load the spring configuration java class
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        Staff staff = context.getBean("assistant", Staff.class);
        staff.setFullName("Albert Einstein");
        // Get the bean and its dependency
        AppointmentSuiteService appointmentSuiteService = context.getBean("appointmentSuiteService", AppointmentSuiteService.class);
        appointmentSuiteService.createAppointment(staff);

        System.out.println(staff);

        context.close();
    }
}
