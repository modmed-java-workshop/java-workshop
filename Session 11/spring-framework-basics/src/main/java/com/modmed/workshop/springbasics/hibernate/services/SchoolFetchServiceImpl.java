package com.modmed.workshop.springbasics.hibernate.services;

import com.modmed.workshop.springbasics.hibernate.entities.School;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SchoolFetchServiceImpl implements SchoolFetchService {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public School fetch(String name) {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<School> cq = criteriaBuilder.createQuery(School.class);

        Root<School> schoolFrom = cq.from(School.class);
        Predicate namePredicate = criteriaBuilder.equal(schoolFrom.get("name"), name);
        cq.where(namePredicate);

        Query query = sessionFactory.getCurrentSession().createQuery(cq);
        return (School) query.getResultList().stream().findFirst().orElse(null);
    }
}
