package com.modmed.workshop.springbasics.annotations;

import com.modmed.workshop.springbasics.models.Staff;
import com.modmed.workshop.springbasics.services.annotated.AppointmentReminderWorkflowService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class UsingAutowiredAnnotationExample {
    public static void main(String[] args) {
        // load the spring configuration file
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AnnotationsConfigure.class);

        // Using default Bean Id
        Staff assistant = context.getBean("assistant", Staff.class);
        assistant.setFullName("Raymond Perez");
        assistant.setAppointment("New Podiatry Appointment at 12:00");


        Staff provider = context.getBean("doctor", Staff.class);
        provider.setFullName("Christ Robles");
        provider.setAppointment("Another Appointment at 18:00");

        //Getting the AppointmentReminderWorkflowService bean without qualifier
        AppointmentReminderWorkflowService appointmentReminderWorkflowService =
                context.getBean(AppointmentReminderWorkflowService.class);

        System.out.println("For Assistant");
        appointmentReminderWorkflowService.startWorkflow(assistant);

        System.out.println("\n\nFor Provider");
        appointmentReminderWorkflowService.startWorkflow(provider);

        context.close();
    }
}
