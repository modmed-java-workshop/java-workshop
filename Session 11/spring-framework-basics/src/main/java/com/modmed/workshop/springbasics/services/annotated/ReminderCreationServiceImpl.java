package com.modmed.workshop.springbasics.services.annotated;

import com.modmed.workshop.springbasics.models.Staff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ReminderCreationServiceImpl implements ReminderCreationService {

    private final Reminder reminder;

    @Autowired
    public ReminderCreationServiceImpl(Reminder reminder) {
        this.reminder = reminder;
    }

    @Override
    public void create(Staff staff) {
        System.out.println("CREATION PROCESS STARTED!");
        System.out.println("A reminder was created for " + staff);
        reminder.remind(staff);
    }
}
