package com.modmed.workshop.springbasics.services.xml;

import com.modmed.workshop.springbasics.models.Staff;

public class AppointmentAssignerServiceImpl implements AppointmentAssignerService {
    @Override
    public void assign(Staff staff) {
        staff.setAppointment("Eye care appointment");
    }
}
