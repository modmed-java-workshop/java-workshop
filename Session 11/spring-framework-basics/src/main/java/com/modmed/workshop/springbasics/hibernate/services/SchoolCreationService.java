package com.modmed.workshop.springbasics.hibernate.services;

import com.modmed.workshop.springbasics.hibernate.entities.School;

public interface SchoolCreationService {
    void create(School school);
}
