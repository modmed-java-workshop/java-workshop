package com.modmed.workshop.springbasics.annotations;

import com.modmed.workshop.springbasics.models.Staff;
import com.modmed.workshop.springbasics.services.annotated.Reminder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class UsingAnnotationsExample {
    public static void main(String[] args) {
        // load the spring configuration file
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AnnotationsConfigure.class);

        // Using default Bean Id
        Reminder reminder = context.getBean( "reminder", Reminder.class);

        // Using given Bean Id
        Staff assistant = context.getBean("assistant", Staff.class);
        assistant.setFullName("Raymond Perez");
        assistant.setAppointment("New Appointment at 5:00 O'clock! (so early! :( )");

        // call methods on the bean
        reminder.remind(assistant);

        // close the context
        context.close();
    }
}
