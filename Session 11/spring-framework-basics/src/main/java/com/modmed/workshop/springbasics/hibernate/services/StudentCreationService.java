package com.modmed.workshop.springbasics.hibernate.services;

import com.modmed.workshop.springbasics.hibernate.entities.Student;

public interface StudentCreationService {
    void create(Student student);
}
