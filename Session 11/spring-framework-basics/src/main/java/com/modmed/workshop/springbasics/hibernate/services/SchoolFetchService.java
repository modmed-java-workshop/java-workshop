package com.modmed.workshop.springbasics.hibernate.services;

import com.modmed.workshop.springbasics.hibernate.entities.School;

public interface SchoolFetchService {
    School fetch(String name);
}
