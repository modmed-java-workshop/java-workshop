package com.modmed.workshop.springbasics.services.annotated;

import com.modmed.workshop.springbasics.models.Staff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppointmentReminderWorkflowServiceImpl implements AppointmentReminderWorkflowService {

    @Autowired
    private ReminderCreationService reminderCreationService;

    @Override
    public void startWorkflow(Staff staff) {
        reminderCreationService.create(staff);
    }
}
