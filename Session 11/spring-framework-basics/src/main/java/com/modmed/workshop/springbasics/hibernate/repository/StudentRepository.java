package com.modmed.workshop.springbasics.hibernate.repository;

import com.modmed.workshop.springbasics.hibernate.entities.Student;
import org.springframework.stereotype.Repository;

public interface StudentRepository {
    void add(Student student);

    Student getStudent(int id);
}
