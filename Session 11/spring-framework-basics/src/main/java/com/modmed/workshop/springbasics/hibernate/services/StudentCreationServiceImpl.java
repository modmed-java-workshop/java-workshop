package com.modmed.workshop.springbasics.hibernate.services;

import com.modmed.workshop.springbasics.hibernate.entities.Student;
import com.modmed.workshop.springbasics.hibernate.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class StudentCreationServiceImpl implements StudentCreationService{
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public void create(Student student) {
        studentRepository.add(student);
    }
}
