package com.modmed.workshop.springbasics.services.annotated;

import com.modmed.workshop.springbasics.models.Staff;

public interface ReminderCreationService {
    void create(Staff staff);
}
