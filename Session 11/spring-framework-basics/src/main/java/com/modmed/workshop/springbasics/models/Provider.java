package com.modmed.workshop.springbasics.models;

import org.springframework.stereotype.Component;

@Component("doctor")
public class Provider implements Staff {
    private String appointment;
    private String fullName;

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String getFullName() {
        return this.fullName;
    }

    @Override
    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    @Override
    public String getAppointment() {
        return appointment;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "appointment='" + appointment + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}
