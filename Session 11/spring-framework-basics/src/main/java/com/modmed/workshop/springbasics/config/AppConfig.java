package com.modmed.workshop.springbasics.config;

import com.modmed.workshop.springbasics.models.Assistant;
import com.modmed.workshop.springbasics.models.Staff;
import com.modmed.workshop.springbasics.services.xml.AppointmentAssignerService;
import com.modmed.workshop.springbasics.services.xml.AppointmentAssignerServiceImpl;
import com.modmed.workshop.springbasics.services.xml.AppointmentSuiteService;
import com.modmed.workshop.springbasics.services.xml.AppointmentSuiteServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.modmed.workshop.springbasics.services.annotated")
//@PropertySource("classpath:app.properties")
public class AppConfig {
    @Bean(initMethod = "init", destroyMethod = "destroy")
    public Assistant assistant() {
        return new Assistant();
    }

    @Bean
    public AppointmentAssignerService appointmentAssignerService() {
        return new AppointmentAssignerServiceImpl();
    }

    @Bean
    public AppointmentSuiteService appointmentSuiteService() {
        return new AppointmentSuiteServiceImpl(appointmentAssignerService());
    }
}
