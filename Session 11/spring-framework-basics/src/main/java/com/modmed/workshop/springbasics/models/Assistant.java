package com.modmed.workshop.springbasics.models;

import org.springframework.stereotype.Component;

@Component
public class Assistant implements Staff {
    private String appointment;
    private String fullName;

    public void init() {
        System.out.println("** Just after starting the Assistant bean creation **");
    }

    @Override
    public String getFullName() {
        return this.fullName;
    }

    @Override
    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    @Override
    public String getAppointment() {
        return appointment;
    }

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "Assistant{" +
                "appointment='" + appointment + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }

    public void destroy() {
        System.out.println("** Just before destroying the Assistant bean **");
    }
}
