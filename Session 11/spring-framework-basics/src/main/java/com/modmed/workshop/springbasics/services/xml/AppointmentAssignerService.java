package com.modmed.workshop.springbasics.services.xml;

import com.modmed.workshop.springbasics.models.Staff;

public interface AppointmentAssignerService {
    void assign(Staff staff);
}
