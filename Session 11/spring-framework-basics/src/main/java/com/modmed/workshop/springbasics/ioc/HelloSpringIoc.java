package com.modmed.workshop.springbasics.ioc;

import com.modmed.workshop.springbasics.models.Staff;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Deprecated
public class HelloSpringIoc {
    public static void main(String[] args) {
        // load the spring configuration file
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        // retrieve bean from spring container
        Staff staff = context.getBean("assistant", Staff.class);
        staff.setFullName("Albert Ioc");
        // call methods on the bean
        System.out.println(staff.getFullName());
        // close the context
        context.close();
    }
}
