package com.modmed.workshop.springbasics.hibernate;

import com.modmed.workshop.springbasics.hibernate.entities.School;
import com.modmed.workshop.springbasics.hibernate.entities.Student;
import com.modmed.workshop.springbasics.hibernate.services.SchoolCreationService;
import com.modmed.workshop.springbasics.hibernate.services.SchoolFetchService;
import com.modmed.workshop.springbasics.hibernate.services.StudentCreationService;
import org.hibernate.Session;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Deprecated
public class SpringHibernateExample {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfig.class);

        School school = new School();
        school.setName("Child's School");
        school.add(new Student("Juanito"));
        school.add(new Student("Alberto"));
        school.add(new Student("Juan"));
        school.add(new Student("Max"));
        school.add(new Student("Alvaro"));

        SchoolCreationService schoolCreationService = context.getBean(SchoolCreationService.class);

        schoolCreationService.create(school);

        SchoolFetchService schoolFetchService = context.getBean(SchoolFetchService.class);

        //TODO test
        School school1 = schoolFetchService.fetch("Child's School");

        context.close();
    }
}
