package com.modmed.workshop.springbasics.models;

public interface Staff {
    void setFullName(String fullName);
    String getFullName();
    void setAppointment(String appointment);
    String getAppointment();
}
