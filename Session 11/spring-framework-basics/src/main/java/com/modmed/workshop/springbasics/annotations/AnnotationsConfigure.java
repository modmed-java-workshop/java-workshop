package com.modmed.workshop.springbasics.annotations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.modmed.workshop.springbasics.services.annotated")
@ComponentScan("com.modmed.workshop.springbasics.models")
public class AnnotationsConfigure {

}