create table school
(
    id int auto_increment,
    name varchar(100) null,
    constraint school_pk
        primary key (id)
);

create table student
(
    id        int auto_increment primary key,
    name      varchar(100) null,
    school_id int          null,
    constraint student_school_fk
        foreign key (school_id) references school (id)
);