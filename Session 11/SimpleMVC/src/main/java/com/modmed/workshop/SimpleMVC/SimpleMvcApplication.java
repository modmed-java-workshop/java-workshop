package com.modmed.workshop.SimpleMVC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class SimpleMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleMvcApplication.class, args);
	}

}
