package com.modmed.workshop.SimpleJPA.data;

import com.modmed.workshop.SimpleJPA.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
