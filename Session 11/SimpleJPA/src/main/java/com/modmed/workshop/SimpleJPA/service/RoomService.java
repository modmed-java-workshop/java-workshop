package com.modmed.workshop.SimpleJPA.service;

import com.modmed.workshop.SimpleJPA.data.RoomRepository;
import com.modmed.workshop.SimpleJPA.models.Room;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class RoomService {
    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public List<Room> getAllRooms(){
        return roomRepository.findAll();
    }
}