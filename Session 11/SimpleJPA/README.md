# Modernizing Medicine (modmed) Java Workshop
# Simple JPA application

## Setup application resources

Prior to get this application up and running, you have to set up the environment correctly. Please, follow these steps: 


NOTE: We are assuming you are using a unix terminal. Anyway there is a couple of `.bat` files to run.

- Install docker on your local computer (https://www.docker.com/)
- Copy from the `bin` directory `data.sql` and `schema.sql` to  `src/main/resources`
- Go to the `bin` folder by typing `cd bin` from your project root directory (inside SimpleJPA folder)
- Run `./start_postgres.sh`
- Run your SimpleApp within you preferred IDE or run `mvn spring-boot:run` within your terminal. **It will run on any operating system**

# Install
````shell
$ ./mvnw clean install
````