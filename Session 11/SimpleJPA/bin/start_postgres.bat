@echo off

set "-euo" "pipefail"
which "docker" REM UNKNOWN: {"type":"Redirect","op":{"text":">","type":"great"},"file":{"text":"/dev/null","type":"Word"}} || REM UNKNOWN: {"type":"Subshell","list":{"type":"CompoundList","commands":[{"type":"LogicalExpression","op":"and","left":{"type":"Command","name":{"text":"echoerr","type":"Word"},"suffix":[{"text":"Please ensure that docker is in your PATH","type":"Word"}]},"right":{"type":"Command","name":{"text":"exit","type":"Word"},"suffix":[{"text":"1","type":"Word"}]}}]}}
mkdir "-p" "%HOME%\docker\volumes\postgres"
DEL /S "%HOME%\docker\volumes\postgres\data"
docker "run" "--rm" "--name" "pg-docker" "-e" "POSTGRES_PASSWORD=postgres" "-e" "POSTGRES_DB=dev" "-d" "-p" "5432:5432" "-v" "%HOME%/docker/volumes/postgres:\var\lib\postgresql" "postgres"
sleep "3"
export "PGPASSWORD=postgres"
docker "cp" "%CD%\schema.sql" "pg-docker:\docker-entrypoint-initdb.d\schema.sql"
docker "cp" "%CD%\data.sql" "pg-docker:\docker-entrypoint-initdb.d\data.sql"
docker "exec" "-u" "postgres" "pg-docker" "psql" "dev" "postgres" "-f" "docker-entrypoint-initdb.d\schema.sql"
docker "exec" "-u" "postgres" "pg-docker" "psql" "dev" "postgres" "-f" "docker-entrypoint-initdb.d\data.sql"