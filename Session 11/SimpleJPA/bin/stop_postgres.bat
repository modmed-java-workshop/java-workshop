@echo off

set "-euo" "pipefail"
which "docker" REM UNKNOWN: {"type":"Redirect","op":{"text":">","type":"great"},"file":{"text":"/dev/null","type":"Word"}} || REM UNKNOWN: {"type":"Subshell","list":{"type":"CompoundList","commands":[{"type":"LogicalExpression","op":"and","left":{"type":"Command","name":{"text":"echoerr","type":"Word"},"suffix":[{"text":"Please ensure that docker is in your PATH","type":"Word"}]},"right":{"type":"Command","name":{"text":"exit","type":"Word"},"suffix":[{"text":"1","type":"Word"}]}}]}}
docker "stop" "pg-docker"