package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Carnivore extends Animal {
    void eatAnimal();
}
