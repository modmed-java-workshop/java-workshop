package com.modmed.workshop.session2.example.polymorphism.decorator.composition;

import com.modmed.workshop.session2.example.polymorphism.decorator.Prescription;

public class PrescritionBuilder {
    Prescription prescription;

    public PrescritionBuilder(Prescription prescription) {
        this.prescription = prescription;
    }

    public PrescritionBuilder allTrim() {
        prescription = new AllTrim(prescription);
        return this;
    }

    public PrescritionBuilder printLength() {
        prescription = new PrintLength(prescription);
        return this;
    }

    public PrescritionBuilder checkEmpty() {
        prescription = new CheckEmpty(prescription);
        return this;
    }

    public Prescription build() {
        return prescription;
    }
}
