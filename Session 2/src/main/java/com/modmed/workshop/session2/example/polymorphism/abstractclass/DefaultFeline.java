package com.modmed.workshop.session2.example.polymorphism.abstractclass;

public abstract class DefaultFeline implements Feline {

    @Override
    public void purr() {
        System.out.println("Prrrrrr");
    }

    @Override
    public boolean hateHumans() {
        return true;
    }
}
