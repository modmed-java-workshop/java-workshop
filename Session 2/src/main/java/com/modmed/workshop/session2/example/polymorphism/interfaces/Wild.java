package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Wild extends Animal {
    void beCareful();
}
