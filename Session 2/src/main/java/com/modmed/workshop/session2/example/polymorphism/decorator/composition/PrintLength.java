package com.modmed.workshop.session2.example.polymorphism.decorator.composition;

import com.modmed.workshop.session2.example.polymorphism.decorator.Prescription;

public class PrintLength implements Prescription {

    private final Prescription origin;

    public PrintLength(Prescription prescription) {
        this.origin = prescription;
    }

    @Override
    public String[] instructions() {
        System.out.println(origin.instructions().length);
        return origin.instructions();
    }
}
