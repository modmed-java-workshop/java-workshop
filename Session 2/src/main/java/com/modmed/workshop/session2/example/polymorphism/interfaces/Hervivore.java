package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Hervivore extends Animal {
    void eatPlant();
}
