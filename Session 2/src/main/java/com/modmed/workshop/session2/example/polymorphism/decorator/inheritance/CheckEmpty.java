package com.modmed.workshop.session2.example.polymorphism.decorator.inheritance;

public class CheckEmpty extends PrintLength {
    public CheckEmpty(final String[] instruction) {
        super(instruction);
    }

    @Override
    public String[] instructions() {
        String[] original = super.instructions();
        this.foo();
        if (original.length == 0) {
            System.out.println("This is empty");
        }
        return original;
    }
}
