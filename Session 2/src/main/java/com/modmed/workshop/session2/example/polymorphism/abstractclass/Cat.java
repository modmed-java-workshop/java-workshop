package com.modmed.workshop.session2.example.polymorphism.abstractclass;

public class Cat extends DefaultFeline {

    @Override
    public void getFood() {
        System.out.println("give me food");
    }
}
