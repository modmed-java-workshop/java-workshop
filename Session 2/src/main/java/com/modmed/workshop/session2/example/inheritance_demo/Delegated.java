package com.modmed.workshop.session2.example.inheritance_demo;

public class Delegated extends Person {

    private String flyTicketNumber;
    private String teamName;

    public String getFlyTicketNumber() {
        return flyTicketNumber;
    }

    public void setFlyTicketNumber(String flyTicketNumber) {
        this.flyTicketNumber = flyTicketNumber;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
