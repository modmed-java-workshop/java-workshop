package com.modmed.workshop.session2.example.polymorphism.decorator;

public class Basic implements Prescription {

    private final String[] instructions;

    public Basic(String[] instructions) {
        this.instructions = instructions;
    }

    @Override
    public String[] instructions() {
        return instructions;
    }
}
