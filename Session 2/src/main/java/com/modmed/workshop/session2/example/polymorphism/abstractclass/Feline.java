package com.modmed.workshop.session2.example.polymorphism.abstractclass;

public interface Feline {
    void purr();

    boolean hateHumans();

    void getFood();
}
