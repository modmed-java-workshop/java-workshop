package com.modmed.workshop.session2.example.polymorphism.interfaces;

public class SomeAnimal implements WrongAnimal {
    @Override
    public void catSurvive() {

    }

    @Override
    public void lionSurvive() {

    }
}
