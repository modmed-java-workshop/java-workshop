package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Aquatic extends Animal {
    void swim();
}
