package com.modmed.workshop.session2.example.polymorphism.abstractclass;

public class AbstractionMain {

    public static void main(String[] args) {
        theGood();
    }

    static void theGood() {
        Feline cat = new Cat();
        cat.getFood();

        Feline lion = new Lion();
        lion.getFood();
    }

    /**
     * Cannot instanciate abstract classes
     */
    static void theBad() {
        // Feline feline = new DefaultFeline();
    }

    /**
     * Don't extend classes that are not meant to be extended.
     * Use final.
     */
    static void theUgly() {
        Cat lazyCat = new LazyCat();
    }
}
