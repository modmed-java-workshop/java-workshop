package com.modmed.workshop.session2.example.polymorphism.decorator.composition;

import com.modmed.workshop.session2.example.polymorphism.decorator.Prescription;

public class CheckEmpty implements Prescription {

    private final Prescription origin;

    public CheckEmpty(Prescription prescription) {
        this.origin = prescription;
    }

    @Override
    public String[] instructions() {
        String[] original = origin.instructions();
        // this.foo(); cannot do this anymore
        if (original.length == 0) {
            System.out.println("This is empty");
        }
        return original;
    }
}
