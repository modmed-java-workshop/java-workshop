package com.modmed.workshop.session2.example.polymorphism.decorator.inheritance;

import com.modmed.workshop.session2.example.polymorphism.decorator.Basic;
import com.modmed.workshop.session2.example.polymorphism.decorator.Prescription;

public class InheritanceMain {
    public static void main(String[] args) {
        theGood();
    }

    static void theGood() {
        String[] recommendations = {"dont' do Drugs", "don't do Amphetamines"};
        // lets see how it goes
        Prescription basic = new Basic(recommendations);
        basic.instructions();
        Prescription inmutable = new AllTrim(recommendations);
        inmutable.instructions();
        Prescription hadouken = new PrintLength(recommendations);
    }

    /**
     * Rely on abstractions
     */
    static void theBad() {
        String[] recommendations = {"dont' do Drugs", "don't do Amphetamines"};
        Basic basic = new Basic(recommendations);
        basic.instructions();
    }

    static void theUgly() {
        /**
         * look at check empty & print length implementation
         */
    }
}
