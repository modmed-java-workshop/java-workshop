package com.modmed.workshop.session2.example.polymorphism.classes;

public class ClassPolymorphismMain {
    public static void main(String[] args) {
        theGood();
    }

    /**
     * Remember is-a
     */
    static void theGood() {
        Pheasant pheasant = new Pheasant();
        printColor(pheasant);

        FemalePheasant femalePheasant = new FemalePheasant();
        printColor(femalePheasant);
        printColor(femalePheasant, "Hello");

        Pheasant malePheasant = new MalePheasant();
        printColor(malePheasant);
    }

    static void printColor(Pheasant pheasant) {
        System.out.println(pheasant.getColor());
    }

    static void printColor(FemalePheasant pheasant) {
        System.out.println("Female color " + pheasant.getColor());
    }

    static void printColor(FemalePheasant pheasant, String preffix) {
        System.out.println(preffix + "-" + pheasant.getColor());
    }

    /**
     * Be careful with your design
     */
    static void theBad() {
        Pheasant cat = new Cat(); // this does not make sense
        printColor(cat);
    }

    /**
     * Respect object's state
     */
    static void theUgly() {
        FemalePheasant femalePheasant = new FemalePheasant();
        String color = femalePheasant.color;
    }
}
