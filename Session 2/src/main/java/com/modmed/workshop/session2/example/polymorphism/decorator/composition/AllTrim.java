package com.modmed.workshop.session2.example.polymorphism.decorator.composition;

import com.modmed.workshop.session2.example.polymorphism.decorator.Prescription;

public class AllTrim implements Prescription {

    private final Prescription origin;

    public AllTrim(Prescription prescription) {
        this.origin = prescription;
    }

    @Override
    public String[] instructions() {
        String[] original = origin.instructions();
        String[] trimed = new String[original.length];
        for (int i = 0; i < original.length; i++) {
            trimed[i] = original[i].trim();
        }
        return trimed;
    }
}
