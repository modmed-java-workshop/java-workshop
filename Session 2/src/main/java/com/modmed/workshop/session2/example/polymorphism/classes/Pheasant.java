package com.modmed.workshop.session2.example.polymorphism.classes;

public class Pheasant {

    String color = "I need more info to specify a color.";

    public String getColor() {
        return "I need more info to specify a color.";
    }
}
