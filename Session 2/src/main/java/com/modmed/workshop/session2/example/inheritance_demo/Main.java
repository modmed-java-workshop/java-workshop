package com.modmed.workshop.session2.example.inheritance_demo;

public class Main {

    public static void main(String[] args) {

//        player.setName("Alexis Sanchez");
//        player.settShirtNumber(7);
//
//        Coach coach = new Coach();
//        coach.setName("Lassarte");
//
//        System.out.println(player.getName());
//        System.out.println(coach.getName());
//
//        System.out.println(coach.getDescription());
//        System.out.println(player.getDescription());
//
//        System.out.println(Player.getDescription());
//        System.out.println(Person.getDescription());


//        Object obj = new Player();
////        obj = new Coach();
//
//        if (obj instanceof Player) {
//            Player player1 = (Player) obj;
//            System.out.println("es un player");
//        }

        Player player = new Player();
        Coach coach = new Coach();
        Person person = new Person();
        Delegated delegated = new Delegated();

        player = (Player) delegated;
        player = (Player) person;

        delegated = player;

        person = player;

        player = (Player) person;




        // Attendant attendant = (Attendant) player;

    }
}
