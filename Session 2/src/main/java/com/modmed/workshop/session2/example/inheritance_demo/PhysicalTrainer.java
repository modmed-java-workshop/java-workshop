package com.modmed.workshop.session2.example.inheritance_demo;

public class PhysicalTrainer {

    private String name;
    private String flyTicketNumber;
    private String teamName;
    private String specialty;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlyTicketNumber() {
        return flyTicketNumber;
    }

    public void setFlyTicketNumber(String flyTicketNumber) {
        this.flyTicketNumber = flyTicketNumber;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }
}
