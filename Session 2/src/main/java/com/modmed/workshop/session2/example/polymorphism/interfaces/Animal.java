package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Animal {
    void survive();
}
