package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Terrestrial extends Animal {
    void walk();
}
