package com.modmed.workshop.session2.example.polymorphism.classes;

public class MalePheasant extends Pheasant {
    @Override
    public String getColor() {
        return "I'm brown";
    }
}
