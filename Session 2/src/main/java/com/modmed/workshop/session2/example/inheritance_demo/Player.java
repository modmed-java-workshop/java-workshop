package com.modmed.workshop.session2.example.inheritance_demo;

public class Player extends Delegated {

    private int tShirtNumber;

    public int gettShirtNumber() {
        return tShirtNumber;
    }

    public void settShirtNumber(int tShirtNumber) {
        this.tShirtNumber = tShirtNumber;
    }


    public static String getDescription() {
        return "Soy un jugador";
    }
}
