package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface WrongAnimal {
    void catSurvive();
    void lionSurvive();
}
