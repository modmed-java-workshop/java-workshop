package com.modmed.workshop.session2.example.polymorphism.abstractclass;

public class Lion extends DefaultFeline {
    @Override
    public void getFood() {
        System.out.println("Hunting ...");
    }
}
