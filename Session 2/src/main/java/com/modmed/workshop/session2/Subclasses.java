package com.modmed.workshop.session2;

public class Subclasses {
    class Vaccine {
        String name;
        Vaccine(String name) {
            this.name = name;
        }
        protected void inject(){}
        protected void foo(){}
        private void bar(){}
    }

    class Pfizer extends Vaccine {
        double dose;
        Pfizer(String name, double dose) {
            super(name);
            this.dose = dose;
        }
        @Override
        protected void foo(){}
    }
}
