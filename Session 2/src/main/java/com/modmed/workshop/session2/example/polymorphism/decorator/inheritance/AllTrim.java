package com.modmed.workshop.session2.example.polymorphism.decorator.inheritance;

import com.modmed.workshop.session2.example.polymorphism.decorator.Basic;

public class AllTrim extends Basic {
    public AllTrim(String[] instruction) {
        super(instruction);
    }

    @Override
    public String[] instructions() {
        String[] original = super.instructions();
        String[] trimed = new String[original.length];
        for (int i = 0; i < original.length; i++) {
            trimed[i] = original[i].trim();
        }
        return trimed;
    }
}
