package com.modmed.workshop.session2.example.polymorphism.interfaces;

public class Shark implements LivingBeing {
    @Override
    public void survive() {

    }

    @Override
    public void swim() {

    }

    @Override
    public void eatAnimal() {

    }

    @Override
    public void dontWorry() {

    }

    @Override
    public void eatPlant() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void beCareful() {

    }
}
