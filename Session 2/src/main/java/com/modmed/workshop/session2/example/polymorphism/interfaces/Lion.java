package com.modmed.workshop.session2.example.polymorphism.interfaces;

public class Lion implements Terrestrial, Wild, Carnivore {
    @Override
    public void survive() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void eatAnimal() {

    }

    @Override
    public void beCareful() {

    }
}
