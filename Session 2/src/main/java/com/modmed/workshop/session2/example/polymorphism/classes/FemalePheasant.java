package com.modmed.workshop.session2.example.polymorphism.classes;

public class FemalePheasant extends Pheasant {
    @Override
    public String getColor() {
        return "I'm gray.";
    }
}
