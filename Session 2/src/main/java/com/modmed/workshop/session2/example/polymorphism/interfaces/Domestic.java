package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface Domestic extends Animal {
    void dontWorry();
}
