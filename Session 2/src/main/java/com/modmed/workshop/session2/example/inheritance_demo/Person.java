package com.modmed.workshop.session2.example.inheritance_demo;

public class Person {

    private String name;



    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getDescription() {
        return "Soy una persona";
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
