package com.modmed.workshop.session2.example.polymorphism.lookup;

public class MethodLookup {
    static abstract class A {
        int m() {
            return 3 - this.n();
        }

        abstract int n();
    }

    static class B extends A {
        int m() {
            return super.m() - this.n();
        }

        int n() {
            return 0;
        }
    }

    static class C extends B {
        int n() {
            return 1;
        }
    }

    static class D extends C {
        int m() {
            return this.n() + super.m();
        }
    }

    static class E extends D {
        int n() {
            return 2;
        }
    }

    public static void main(String[] args) {
        System.out.println(new E().m()); // what will this print?
    }
}
