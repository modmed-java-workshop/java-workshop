package com.modmed.workshop.session2.example.polymorphism.decorator.inheritance;

public class PrintLength extends AllTrim {
    public PrintLength(String[] instruction) {
        super(instruction);
    }

    @Override
    public String[] instructions() {
        System.out.println(super.instructions().length);
        return super.instructions();
    }

    public void foo() {
        // ...
    }
}
