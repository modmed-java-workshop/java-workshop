package com.modmed.workshop.session2.example.polymorphism.decorator.composition;

import com.modmed.workshop.session2.example.polymorphism.decorator.Basic;
import com.modmed.workshop.session2.example.polymorphism.decorator.Prescription;

public class CompositionMain {
    public static void main(String[] args) {
        theGood();
    }

    static void theGood() {
        String[] recommendations = {"dont' do Drugs", "don't do Amphetamines"};
        // lets see how it goes
        Basic basic = new Basic(recommendations);
        Prescription prescription =
                new PrintLength(
                        new CheckEmpty(
                                new AllTrim(basic)
                        )
                );
        prescription.instructions();
    }

    /**
     * Always relay on abstractions rather than implementations
     */
    static void theBad() {
        String[] recommendations = {"dont' do Drugs", "don't do Amphetamines"};
        // lets see how it goes
        Basic basic = new Basic(recommendations);
        /*PrintLength prescription =
                new CheckEmpty(
                        new PrintLength(
                                new CheckEmpty(
                                        new AllTrim(basic)
                                )
                        )
                );*/
    }


    static void theUgly() {
        String[] recommendations = {"dont' do Drugs", "don't do Amphetamines"};
        // lets see how it goes
        Basic basic = new Basic(recommendations);
        Prescription prescription =
                new PrintLength(
                        new PrintLength(
                                new PrintLength(
                                        new PrintLength(
                                                new PrintLength(
                                                        new PrintLength(
                                                                new PrintLength(
                                                                        new PrintLength(
                                                                                new PrintLength(
                                                                                        new PrintLength(
                                                                                                new AllTrim(basic)
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                );
    }

    static void theBuilder() {
        String[] recommendations = {"dont' do Drugs", "don't do Amphetamines"};
        // lets see how it goes
        Basic basic = new Basic(recommendations);
        Prescription withBuilder = new PrescritionBuilder(basic)
                .allTrim()
                .printLength()
                .checkEmpty()
                .printLength()
                .printLength()
                .printLength()
                .printLength()
                .printLength()
                .printLength()
                .printLength()
                .printLength()
                .build();
        withBuilder.instructions();
    }
}
