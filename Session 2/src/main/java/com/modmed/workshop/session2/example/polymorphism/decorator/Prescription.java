package com.modmed.workshop.session2.example.polymorphism.decorator;

public interface Prescription {
    String[] instructions();
}
