package com.modmed.workshop.session2.example.polymorphism.interfaces;

public class InterfaceMain {
    public static void main(String[] args) {
        theGood();
    }

    static void survive(Animal animal) {
        animal.survive();
    }

    static void theGood() {
        Animal lion = new Lion();
        survive(lion);
    }

    /**
     * Classes decide concrete implementations
     */
    static void theBad() {
        WrongAnimal animal = new SomeAnimal();
        animal.catSurvive();
        animal.lionSurvive();
    }

    /**
     * Look at what Shark implements
     */
    static void theUgly() {
        LivingBeing shark = new Shark();
        shark.beCareful();
        shark.eatAnimal();
        shark.survive();
    }
}
