package com.modmed.workshop.session2.example.polymorphism.interfaces;

public interface LivingBeing {
    void survive();
    void swim();
    void eatAnimal();
    void dontWorry();
    void eatPlant();
    void walk();
    void beCareful();
}
