# I/O Challenge

## What are the tasks we have to accomplish?

This challenge is about to read a given file of `Staff` string representation with some information and write it in another file. The main objective is to use the BufferedStream implementation for reading and writing.

- We have an abstract class `PopulateAndStoreProcessor` which asks to implement these two methods:
  -- `readFromFile` -> Given file path (getPathFileToRead) and using correct `BufferedReader` implementation, read each line from the flat file. You can use any of given examples about BufferedStreams.
  -- `writeToFile` -> in the same way above, take that list and write it into the file under the given filepath.

**Tip**: To map or convert the Staff String representation to an object, you can use `Staff s = mapStringToStaff(nextLine);` when the buffer is reading each line.

### Example of using a list (collections)
```
List<Staff> staffList = new ArrayList<>();
...
staffList.add(s);
```