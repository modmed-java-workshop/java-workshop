package com.modmed.workshop.session7.examples;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

class ReaderExample {
    public static void main(String[] args) {
        // Creates an array of character
        char[] array = new char[100];
        Reader input = null;

        try {
            // Creates a reader using the FileReader
            input = new FileReader("readme.md");

            // Checks if reader is ready
            System.out.println("Is there data in the stream?  " + input.ready());

            // Reads characters
            input.read(array);
            System.out.println("Data in the stream:");
            System.out.println(array);

            // Closes the reader
            input.close();

        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            try {
                if(input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}