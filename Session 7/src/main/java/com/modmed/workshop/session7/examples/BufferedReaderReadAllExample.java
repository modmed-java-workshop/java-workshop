package com.modmed.workshop.session7.examples;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Collectors;

public class BufferedReaderReadAllExample {
    public static void main(String[] args) throws FileNotFoundException {
        // Initialization
        BufferedReader reader =
                new BufferedReader(new FileReader("src/main/resources/input.txt"));
        System.out.println(reader.lines().collect(Collectors.joining(System.lineSeparator())));
    }
}
