package com.modmed.workshop.session7.examples;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamExample {
    public static void main(String[] args) {
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream("output.txt");
            byte[] bytes = "writing".getBytes(); // get the byte arrays of the String
            for (byte data : bytes) {
                outputStream.write(data); // Writes in the each byte in the output stream
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
