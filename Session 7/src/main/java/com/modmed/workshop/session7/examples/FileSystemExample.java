package com.modmed.workshop.session7.examples;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class FileSystemExample {

    public static void main(String[] args) throws URISyntaxException {
        // getResource() returns an URL
        URL url = ClassLoader.getSystemClassLoader().getResource("input.txt");
        System.out.println(url.getPath());

        URI uri = new URI(url.toString());
        System.out.println(uri.getPath());
    }
}
