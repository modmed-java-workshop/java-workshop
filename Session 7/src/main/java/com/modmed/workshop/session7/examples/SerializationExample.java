package com.modmed.workshop.session7.examples;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Patient implements Serializable {

    String name;
    int age;

    public Patient(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

public class SerializationExample {

    public static void main(String[] args) {

        // Creates an object of Patient class
        Patient patient = new Patient("Tyson", 44);

        try {
            FileOutputStream fileOut = new FileOutputStream("patient.txt");

            // Creates an ObjectOutputStream
            ObjectOutputStream objOut = new ObjectOutputStream(fileOut);

            // Writes objects to the output stream
            objOut.writeObject(patient);

            objOut.close();

            // Reads the object
            FileInputStream fileIn = new FileInputStream("patient.txt");
            ObjectInputStream objIn = new ObjectInputStream(fileIn);

            // Reads the objects
            Patient newPatient = (Patient) objIn.readObject();

            System.out.println("Patient Name: " + newPatient.name);
            System.out.println("Patient Age: " + newPatient.age);

            objIn.close();
        }

        catch (Exception e) {
            e.getStackTrace();
        }
    }
}
