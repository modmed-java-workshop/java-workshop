package com.modmed.workshop.session7.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class LambdaExample {
    public static void main(String[] args) throws IOException {

        Path path = Paths.get("Session 9/book_index.txt");

        BufferedReader br = Files.newBufferedReader(path);
        Stream<String> lines = br.lines();
        lines.forEach(System.out::println);
        lines.close();

        List<String> strList = Files.readAllLines(path);
        Stream<String> lines2 = strList.stream();
        lines2.forEach(System.out::println);
        lines2.close();
    }
}
