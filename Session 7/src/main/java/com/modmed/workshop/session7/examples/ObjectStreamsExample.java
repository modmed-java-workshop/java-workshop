package com.modmed.workshop.session7.examples;

import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model.Staff;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectStreamsExample {

    public static void main(String[] args) {
        try {
//            writeObject();
            readObject();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeObject() throws IOException {

        int data1 = 5;
        String data2 = "This is programiz";
        Staff staff = new Staff();

        FileOutputStream file = new FileOutputStream("file.txt");
        ObjectOutputStream output = new ObjectOutputStream(file);

        // Writing to the file using ObjectOutputStream
        output.writeInt(data1);
        output.writeObject(data2);

        output.close();
    }

    private static void readObject() throws IOException, ClassNotFoundException {
        FileInputStream fileStream = new FileInputStream("file.txt");
        // Creating an object input stream
        ObjectInputStream objStream = new ObjectInputStream(fileStream);

        //Using the readInt() method
        System.out.println("Integer data :" + objStream.readInt());

        // Using the readObject() method
        System.out.println("String data: " + objStream.readObject());

        objStream.close();
    }
}
