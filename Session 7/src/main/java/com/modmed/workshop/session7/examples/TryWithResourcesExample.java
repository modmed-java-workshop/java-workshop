package com.modmed.workshop.session7.examples;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

public class TryWithResourcesExample {
    public static void main(String[] args) throws IOException, URISyntaxException {

        String path = ClassLoader.getSystemClassLoader().getResource("input.txt").toURI().getPath();

        //Try with resource
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            System.out.println(bufferedReader.readLine());
        }

        //Equivalent
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
        try {
            System.out.println(bufferedReader.readLine());
        } finally {
            bufferedReader.close();
        }
    }
}
