package com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model;

import java.io.Serializable;

public class Staff implements Serializable {
    private StaffType type;
    private String firstName;
    private String lastName;
    private Integer employeeId;
    private Short age;

    public void setType(StaffType type) {
        this.type = type;
    }

    public StaffType getType() {
        return type;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Short getAge() {
        return age;
    }

    public void setAge(Short age) {
        this.age = age;
    }

    public String outputFormatted() {
        return String.format("OUTPUT;%s;%d;%s;%s", type, employeeId, lastName, firstName);
    }
}
