package com.modmed.workshop.session7.examples;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathExample {

    private static void pathInformation() {
        Path path = Paths.get("/patient/bill/charges");
        System.out.println(path);

        Path parentPath = path.getParent();
        System.out.println(parentPath);

        Path rootPath = path.getRoot();
        System.out.println(rootPath);
    }

    /**
     * Removes redundancies as /../ or /./
     */
    private static void pathNormalization() {
        Path path = Paths.get("/home/../patient/appointments");

        Path cleanPath = path.normalize();

        System.out.println(cleanPath.toString());
    }

    private static void pathConversion() {
        Path path = Paths.get("home/patient/appointments.html");

        System.out.println(path.toAbsolutePath());

        System.out.println(path.toUri());
    }

    private static void pathJoin() {
        Path path = Paths.get("/test/articles");

        Path path2 = path.resolve("java");

        System.out.println(path2.toString());

    }

    private static void pathComparison() {
        Path p1 = Paths.get("/test/articles");
        Path p2 = Paths.get("/test/articles");
        Path p3 = Paths.get("/test/authors");

        System.out.println(p1.equals(p2));
        System.out.println(p1.equals(p3));
    }

    public static void main(String[] args) {
        try {
            //pathInformation();
            //pathNormalization();
            pathConversion();
            //pathJoin();
            //pathComparison();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
