package com.modmed.workshop.session7.examples;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ReaderWriterExample {

    public static void main(String[] args) throws IOException {

        InputStream input = null;

        try {
            input = new FileInputStream("resources/input.txt"); //11000011 10110001

            InputStreamReader inputWriter = new InputStreamReader(new FileInputStream("resources/byteAndCharDiff.txt"));

            int byte1 = input.read();//11000011 -> 195
            int byte2 = input.read();//10110001 -> 177

            int charSetUnicodeBytes = inputWriter.read();

            System.out.println("byte1 (byte stream):" + byte1);//11000011 -> 195
            System.out.println("byte2 (byte stream):" + byte2);//10110001 -> 177

            System.out.println("2 bytes int representation (Character stream):" + charSetUnicodeBytes);

            System.out.println((char) charSetUnicodeBytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(input != null) {
                input.close();
            }
        }
    }
}
