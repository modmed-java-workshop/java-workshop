package com.modmed.workshop.session7.examples;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedWriterExample {
    public static void main(String[] args) throws IOException {
        Doctor doctor = new Doctor("Pedro");
        BufferedWriter writer = new BufferedWriter(new FileWriter("bufferedOutput.txt"));
        writer.write(doctor.toString());
        writer.flush();
        writer.close();
    }
}

class Doctor {
    private final String name;

    public Doctor(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "name='" + name + '\'' +
                '}';
    }
}
