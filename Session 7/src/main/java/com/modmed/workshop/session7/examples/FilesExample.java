package com.modmed.workshop.session7.examples;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class FilesExample {

    private static String HOME;

    /**
     * File Checks
     */
    public static void fileCheckExample() {
        Path p = Paths.get(HOME);
        System.out.println(Files.exists(p));
        System.out.println(Files.isRegularFile(p));
        System.out.println(Files.isReadable(p));
        System.out.println(Files.isWritable(p));
        System.out.println(Files.isExecutable(p));

        //Comparing files
        Path p1 = Paths.get(HOME);
        Path p2 = Paths.get(HOME);

        try {
            System.out.println(Files.isSameFile(p1, p2));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void fileCreationExample() throws IOException{
        String fileName = "myfile_" + UUID.randomUUID().toString() + ".txt";
        Path path = Paths.get(HOME + "/" + fileName);
        System.out.println(path);
        System.out.println("File exists: " + Files.exists(path));

        Files.createFile(path);
        System.out.println("File created");

        System.out.println("File exists: " + Files.exists(path));
    }

    private static void fileDeletionExample() throws IOException {
        Path path = Paths.get(HOME + "/fileToDelete.txt");

        Files.createFile(path);
        System.out.println("File exists: " + Files.exists(path));

        Files.delete(path);
        System.out.println("File exists: " + Files.exists(path));
    }

    private static void tempFileCreationExample() throws IOException {
        String prefix = "log_";
        String suffix = ".txt";
        Path path = Paths.get(HOME + "/");

        Files.createTempFile(path, prefix, suffix);

        //The default location of the created file will be the file system provided temporary-file directory
        path = Files.createTempFile(null, null);
        System.out.println("File exists: " + Files.exists(path));
        System.out.println(path);
    }

    public static void main(String[] args) {
        Path currentRelativePath = Paths.get("Session 9");
        HOME = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current path is: " + HOME);

        try {
            //fileCheckExample();
            fileCreationExample();
            //fileDeletionExample();
            //tempFileCreationExample();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
