package com.modmed.workshop.session7.challenges.readwrite.solutions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class ReadWritePatientFileSolved {

    public static void main(String[] args) throws IOException {

        /*Given the file 'patient_data.csv' read it and show the content in the console*/
        InputStream is = new FileInputStream("patient_data.csv");

        int c;

        while ((c = is.read()) != -1) {
            System.out.print((char)c);
        }
        is.close();
        /*Create a new file called patient_file2.csv and include a new patient*/
        String patientInfo = "firstName;lastName;1234567-T";

        byte[] patientInfoBytes = patientInfo.getBytes(StandardCharsets.UTF_8);

        OutputStream out = new FileOutputStream("patient_data2.csv");

        for (byte character: patientInfoBytes) {
            out.write(character);
        }
        out.close();
    }

}
