package com.modmed.workshop.session7.challenges.readwrite.medicalstaff.fileprocessor;

import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model.Staff;
import java.util.List;

public class BufferedReadWriteStaffStoreProcessor extends PopulateAndStoreProcessor {
    @Override
    public List<Staff> readFromFile(String path) {
        return null;
    }

    @Override
    public void writeToFile(String pathAndFile, List<Staff> staffList) {

    }
}
