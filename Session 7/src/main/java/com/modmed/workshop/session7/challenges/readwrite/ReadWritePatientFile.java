package com.modmed.workshop.session7.challenges.readwrite;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ReadWritePatientFile {

    public static void main(String[] args) throws IOException {

        /*Given the file 'patient_data.csv' read it and show the content in the console*/
        InputStream is = new FileInputStream("patient_data.csv");

        int c;

        // -----------------------------------------------------------------

        /*Given the byte array 'patientInfoBytes' put the content in a new file called patient_file2.csv */
        String patientInfo = "firstName;lastName;1234567-T";
        byte[] patientInfoBytes = patientInfo.getBytes();

        /*Create the output stream pointing to the file you want to create*/

        /*Write the data*/

    }

}
