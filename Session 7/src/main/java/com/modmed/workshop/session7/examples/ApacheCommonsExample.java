package com.modmed.workshop.session7.examples;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

public class ApacheCommonsExample {

    /**
     * FileUtils - copying and reading a file
     */
    public static void fileUtilsExample(){
        try {
            File file = FileUtils.getFile(ApacheCommonsExample.class.getClassLoader()
                    .getResource("input.txt").toURI()
                    .getPath());

            File tempDir = FileUtils.getTempDirectory();

            FileUtils.copyFileToDirectory(file, tempDir);

            File newTempFile = FileUtils.getFile(tempDir, file.getName());

            String data = FileUtils.readFileToString(newTempFile, Charset.defaultCharset());

            System.out.println(data);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * FilenameUtils Example
     */
    public static void filenameUtilsExample() throws URISyntaxException {
        File file = FileUtils.getFile(ApacheCommonsExample.class.getClassLoader()
                .getResource("input.txt").toURI()
                .getPath());

        String path = file.toString();
        String fullPath = FilenameUtils.getFullPath(path);
        String extension = FilenameUtils.getExtension(path);
        String baseName = FilenameUtils.getBaseName(path);

        System.out.println(fullPath);
        System.out.println(extension);
        System.out.println(baseName);
    }


    public static void main(String[] args) throws URISyntaxException {

        //fileUtilsExample();
        filenameUtilsExample();
    }
}
