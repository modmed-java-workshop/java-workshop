package com.modmed.workshop.session7.challenges.readwrite.medicalstaff.fileprocessor;

import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model.Staff;
import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model.StaffType;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public abstract class PopulateAndStoreProcessor {

    private final List<CsvHeaderColumn> headerColumns = new ArrayList<>();

    public String getPathFileToRead() {
        String path = null;
        try {
            URL url = ClassLoader.getSystemClassLoader().getResource("staff_input.txt");
            if(url != null) {
                path = Paths.get(url.toURI()).toString();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return path;
    }

    /***
     * Given file path (getPathFileToRead) and using correct implementation, read each line from the flat file
     * @param path
     * @return
     */
    public abstract List<Staff> readFromFile(String path);

    protected Staff mapStringToStaff(String data) {
        if (headerProcessed(data)) {
            return new Staff();
        }

        Staff staff = new Staff();
        String[] stringStaffArray = data.split(";");

        for(int i = 0; i < stringStaffArray.length; i++) {
            switch (headerColumns.get(i)) {
                case TYPE:
                    staff.setType(StaffType.valueOf(stringStaffArray[i]));
                    break;
                case EMPLOYEE_ID:
                    staff.setEmployeeId(Integer.valueOf(stringStaffArray[i]));
                    break;
                case FIRST_NAME:
                    staff.setFirstName(stringStaffArray[i]);
                    break;
                case LAST_NAME:
                    staff.setLastName(stringStaffArray[i]);
                    break;
                case AGE:
                    staff.setAge(Short.valueOf(stringStaffArray[i]));
                    break;
            }
        }

        return staff;
    }

    private boolean headerProcessed(String data) {
        if(headerColumns.isEmpty() && data.contains(CsvHeaderColumn.TYPE.name())) {
            String[] headerCols = data.split(";");
            for (String headerCol: headerCols) {
                headerColumns.add(CsvHeaderColumn.valueOf(headerCol));
            }
            return true;
        }
        return false;
    }

    public abstract void writeToFile(String pathAndFile, List<Staff> staffList);
}
