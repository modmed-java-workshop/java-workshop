package com.modmed.workshop.session7.examples;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class WriterExample {
    public static void main(String[] args) {
        oldWay();
        try {
            tryWithResourcesWay();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void tryWithResourcesWay() throws IOException {
        try (FileWriter fileWriter = new FileWriter("FileWriterTest.txt")) {
            fileWriter.write("Hello Folks!");
        }
    }

    private static void oldWay() {
        Writer writer = null;
        try {
            OutputStream outputStream = new FileOutputStream("FileWriterTest.txt");
            writer = new OutputStreamWriter(outputStream);
            String helloWorld = "Hello World!";
            writer.write(helloWorld.toCharArray());
        } catch (IOException e) {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
