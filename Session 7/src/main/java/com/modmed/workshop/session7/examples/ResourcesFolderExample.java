package com.modmed.workshop.session7.examples;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ResourcesFolderExample {

    public static void main(String[] args) throws URISyntaxException {

        URL url = ClassLoader.getSystemClassLoader().getResource("input.txt");
        File file = Paths.get(url.toURI()).toFile();

        try {
            System.out.println(new String(Files.readAllBytes(file.toPath())));
        } catch (IOException e) {

            e.printStackTrace();

        }
    }
}
