package com.modmed.workshop.session7.challenges.readwrite;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadWritePatientFilewithReaders {

    public static void main(String[] args) throws IOException {

        /*Given the file 'patient_data.csv' read it and show the content in the console*/
        InputStreamReader is = new FileReader("patient_data.csv");

        int c;

        // -----------------------------------------------------------------

        /*Given the byte array 'patientInfoBytes' put the content in a new file called patient_file2.csv */
        String patientInfo = "firstName;lastName;1234567-T";
        byte[] patientInfoBytes = patientInfo.getBytes();

        /*Create the output reader pointing to the file you want to create*/

        /*Write the data*/

    }

}
