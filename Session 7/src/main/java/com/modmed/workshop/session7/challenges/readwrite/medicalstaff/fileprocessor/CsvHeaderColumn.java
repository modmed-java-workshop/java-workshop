package com.modmed.workshop.session7.challenges.readwrite.medicalstaff.fileprocessor;

public enum CsvHeaderColumn {
    TYPE,
    EMPLOYEE_ID,
    FIRST_NAME,
    LAST_NAME,
    AGE
}
