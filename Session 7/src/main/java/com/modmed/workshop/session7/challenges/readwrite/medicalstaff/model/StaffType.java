package com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model;

public enum StaffType {
    PROVIDER,
    ASSISTANT,
    RADIOLOGIST
}
