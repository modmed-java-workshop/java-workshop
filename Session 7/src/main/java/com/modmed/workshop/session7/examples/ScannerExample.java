package com.modmed.workshop.session7.examples;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerExample {

    public ScannerExample() {

        String path = this.getClass().getClassLoader().getResource("input.txt").getPath();
        File file = new File(path);

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //new scanner with nextInt
        try {
            Scanner scanner2 = new Scanner(file);
            int sum = 0;
            while (scanner2.hasNextLine()){
                sum+=scanner2.nextInt(); //you can use nextInt() - it looks for
            }
            System.out.println (sum);
        } catch (FileNotFoundException ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ScannerExample();
    }
}
