package com.modmed.workshop.session7.challenges.readwrite.medicalstaff;

import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.fileprocessor.BufferedReadWriteStaffStoreProcessor;
import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.fileprocessor.PopulateAndStoreProcessor;
import com.modmed.workshop.session7.challenges.readwrite.medicalstaff.model.Staff;
import java.util.List;

public class ProcessStaffFile {
    public static void main(String[] args) {
        PopulateAndStoreProcessor bufferedReadWriteStaffStoreProcessor = new BufferedReadWriteStaffStoreProcessor();
        // 1. Get the file path to read
        String pathFile = bufferedReadWriteStaffStoreProcessor.getPathFileToRead();
        List<Staff> staffList = null;
        if (pathFile != null) {
            // 2. Implement the `readFromFileMethod`
            staffList = bufferedReadWriteStaffStoreProcessor.readFromFile(pathFile);
        }
        // 3 Get the file path to write the content in a file called "output_staff.txt"
        bufferedReadWriteStaffStoreProcessor.writeToFile("output.txt", staffList);
    }
}
