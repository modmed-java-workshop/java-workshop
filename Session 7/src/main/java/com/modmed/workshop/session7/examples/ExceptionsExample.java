package com.modmed.workshop.session7.examples;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionsExample {

    public static void main(String[] args) {

        try
        {
            new FileInputStream("nonexistent.txt");
            System.out.println("This is not printed");
        }
        catch(IOException exception)
        {
            exception.printStackTrace();
        }

        try
        {
            new FileInputStream("nonexistent.txt");
            System.out.println("This is not printed");
        }
        catch (FileNotFoundException exception)
        {
            exception.printStackTrace();
        }

        /**
         * Same way there are other exceptions types as
         * @see java.io.EOFException
         * @see java.io.UTFDataFormatException
         * @see java.nio.file.FileSystemException
         * @see java.nio.file.AccessDeniedException
         * ...
         */
    }
}
