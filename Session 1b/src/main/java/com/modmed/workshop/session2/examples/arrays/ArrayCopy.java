package com.modmed.workshop.session2.examples.arrays;

import java.util.Arrays;

public class ArrayCopy {

    static char[] patientSexScore = new char[1000];

    public static void main(String[] args) {
        char[] records = {'F', 'F', 'F', 'M', 'M', 'M', 'F'};
        addRecords(records);

        System.out.println(new String(patientSexScore));


        //copy with stream
        String[] strArray = {"orange", "red", "green'"};
        String[] copiedArray = Arrays.stream(strArray).toArray(String[]::new);
    }

    public static int getIndex() {
        int i = 0;
        while (patientSexScore[i] != '\u0000') {
            i++;
        }
        return i;
    }

    public static void addRecords(char[] records) {
        System.arraycopy(records, 0, patientSexScore, getIndex(), records.length);
    }
}
