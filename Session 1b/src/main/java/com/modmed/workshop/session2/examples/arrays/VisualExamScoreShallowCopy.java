package com.modmed.workshop.session2.examples.arrays;

import java.util.Arrays;

// code illustrating shallow copy
public class VisualExamScoreShallowCopy {

    private int[] data;

    // makes a shallow copy of values
    public VisualExamScoreShallowCopy(int[] data) {
        this.data = data;
    }

    public void showData() {
        System.out.println(Arrays.toString(data));
    }
}
