package com.modmed.workshop.session2.examples.model;

public class Patient implements Person {

    private Visit[] visits;

    public Patient() {
        visits = new Visit[] {
                new Visit(50),
                new Visit(65),
                new Visit(23),
                new Visit(20),
                new Visit(45),
                new Visit(15),
        };
    }

    @Override
    public boolean isPatient() {
        return true;
    }

    @Override
    public Visit[] getVisits() {
        return visits;
    }

    public void setVisits(Visit[] visits) {
        this.visits = visits;
    }
}
