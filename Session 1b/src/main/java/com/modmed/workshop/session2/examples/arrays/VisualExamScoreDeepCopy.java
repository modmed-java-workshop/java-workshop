package com.modmed.workshop.session2.examples.arrays;

import java.util.Arrays;

// code illustrating deep copy
public class VisualExamScoreDeepCopy {

    private int[] data;

    // makes a deep copy of values
    public VisualExamScoreDeepCopy(int[] values) {
        data = new int[values.length];
        for(int i = 0; i < values.length; i++) {
            data[i] = values[i];
        }
    }

    public void showData() {
        System.out.println(Arrays.toString(data));
    }
}
