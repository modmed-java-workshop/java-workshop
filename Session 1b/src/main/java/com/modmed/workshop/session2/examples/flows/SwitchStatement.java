package com.modmed.workshop.session2.examples.flows;

public class SwitchStatement {

    private static void switchExample() {
        String countryCode = "de";
        String countryName;

        switch (countryCode) {

            case "us":
                countryName = "United States";
                break;

            case "de":
                countryName = "Germany";
                break;

            case "hu":
                countryName = "Hungary";
                break;

            default:
                countryName = "Unknown";
                break;
        }

        System.out.println(countryName);
    }

    public static void main(String args[]) {
        switchExample();
    }
}
