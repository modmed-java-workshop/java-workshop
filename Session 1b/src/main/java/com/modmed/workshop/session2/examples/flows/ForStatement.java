package com.modmed.workshop.session2.examples.flows;

public class ForStatement {

    private static void forExample() {
        String word = "Looping";

        //initialExpression; testExpression; updateExpression
        for (int i = 0; i < word.length(); i++) {
            System.out.print(word.charAt(i));
        }
    }

    private static void backwardsForExample() {
        String word = "Looping";

        for (int i = word.length() - 1; i >= 0; i--) {
            System.out.print(word.charAt(i));
        }
    }

    private static void forEachExample() {
        int[] numbers = {3, 9, 5, -5};

        //type element: array
        for (int number: numbers) {
            System.out.print(number);
        }
    }

    public static void main(String args[]) {
        forExample();
        System.out.println("");
        backwardsForExample();
        System.out.println("");
        forEachExample();
    }
}
