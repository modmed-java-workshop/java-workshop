package com.modmed.workshop.session2.examples.flows;

import java.util.Random;

public class BreakStatement {

    private static void breakForExample() {
        for (int i = 0; i < 10; i++) {
            if(i == 7) {
                break;
            }
            System.out.print(i + " ");
        }
    }

    private static void breakWhileExample() {
        Random random = new Random();

        while (true) {

            int num = random.nextInt(10);
            System.out.print(num + " ");

            if (num == 7) {
                break;
            }
        }
    }

    public static void main(String args[]) {
        breakForExample();
        System.out.println("");
        breakWhileExample();
    }
}
