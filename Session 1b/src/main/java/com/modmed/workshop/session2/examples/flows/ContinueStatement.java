package com.modmed.workshop.session2.examples.flows;

public class ContinueStatement {

    private static void continueExample() {
        for (int i = 1; i <= 10; ++i) {

            // Skipping values between 4 and 7
            if (i > 4 && i < 7) {
                continue;
            }
            System.out.println(i);
        }
    }

    public static void main(String args[]) {
        continueExample();
    }
}
