package com.modmed.workshop.session2.examples.arrays;

import java.util.Arrays;

public class ArrayManipulation {

    static int[] patientBoxes = new int[10];

    public static void main(String[] args) {

        emptyBoxes();

        addPatient(getEmptyBox(), 2345);
        addPatient(getEmptyBox(), 3214);

        System.out.println(Arrays.toString(patientBoxes));
    }

    public static void emptyBoxes() {
        Arrays.fill(patientBoxes, 0);
    }

    public static int getEmptyBox() {
        //returns a index, it can by any
        Arrays.sort(patientBoxes);
        return Arrays.binarySearch(patientBoxes, 0);
    }

    public static void addPatient(int boxIndex, int patientId) {
        if (boxIndex >= 0) {
            patientBoxes[boxIndex] = patientId;
        }
    }

}
