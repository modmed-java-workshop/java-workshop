package com.modmed.workshop.session2.examples.flows;

public class IfStatement {

    private static void simpleIf() {
        int patientAge = 60;

        if (patientAge < 65) {
            System.out.println("Still young");
        }
    }

    private static void simpleIfElse() {
        int patientAge = 25;
        float discount;

        if (patientAge > 65) {
            discount = .5f;
        } else {
            discount = .1f;
        }

        System.out.println("Discount: " + discount);
    }

    private static void ifElseLadder() {
        int patientAge = 32;
        float discount;

        if (patientAge == 60 ) {
            discount = .5f;
        } else if (patientAge == 31){
            discount = .3f;
        } else if (patientAge == 22){
            discount = .2f;
        } else {
            discount = .1f;
        }

        System.out.println("Discount: " + discount);
    }

    public static void main(String[] args) {
        //simpleIf();
        //simpleIfElse();
        ifElseLadder();
    }
}



