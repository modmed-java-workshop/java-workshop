package com.modmed.workshop.session2.examples.flows;

public class WhileStatement {

    private static void whileExample() {
        int i = 0;
        int sum = 0;

        while (sum < 10) {

            i += 2;
            sum += i;
        }

        System.out.println(sum);
    }

    private static void doWhileExample() {
        int i = 1, n = 5;

        do {
            System.out.print(i + " ");
            i++;
        } while(i <= n);
    }

    public static void main(String args[]) {
        whileExample();
        doWhileExample();
    }

}
