package com.modmed.workshop.session2.examples.operators;

public class AssignmentsOperators {
    public static void main(String[] args) {
        // create variables
        int a = 4;
        int result;

        // assign value using =
        result = a;
        System.out.println("result using =: " + result);

        // assign value using =+
        result += a;
        System.out.println("result using +=: " + result);

        // assign value using =*
        result *= a;
        System.out.println("result using *=: " + result);
    }
}
