package com.modmed.workshop.session2.examples.model;

public interface Person {
    boolean isPatient();
    Visit[] getVisits();
}
