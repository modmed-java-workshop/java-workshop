package com.modmed.workshop.session2.examples.arrays;

public class DeepAndShallowCopy {
    public static void main(String[] args) {
        int[] vals = {3, 7, 9};
        VisualExamScoreShallowCopy visualExamScoreShallowCopy = new VisualExamScoreShallowCopy(vals);
        visualExamScoreShallowCopy.showData(); // prints out [3, 7, 9]
        vals[0] = 10;
        visualExamScoreShallowCopy.showData(); // prints out [10, 7, 9]


        int[] vals2 = {3, 7, 9};
        VisualExamScoreDeepCopy visualExamScoreDeepCopy = new VisualExamScoreDeepCopy(vals2);
        visualExamScoreDeepCopy.showData(); // prints out [3, 7, 9]
        vals2[0] = 10;
        visualExamScoreDeepCopy.showData(); // prints out [3, 7, 9]
    }
}
