package com.modmed.workshop.session2.examples.model;

import java.util.Date;

public class Visit {
    private Date date = new Date();
    private int durationInMinutes;

    public Visit(int durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(int durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

}
