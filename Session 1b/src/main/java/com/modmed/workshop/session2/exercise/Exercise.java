package com.modmed.workshop.session2.exercise;

import com.modmed.workshop.session2.examples.model.Patient;
import com.modmed.workshop.session2.examples.model.Person;

public class Exercise {

    public static void main(String[] args) {
        Person patient = new Patient();

        // Imprimir nombre del paciente si patient tiene mas de 5 visitas
        conditionalCheck(patient);

        // Iterar imprimendo la duracion de las visitas del paciente hasta encontrar una igual a 45 minutos
        iterationCheck(patient);

    }

    private static void conditionalCheck(Person patient) {

    }

    private static void iterationCheck(Person patient) {
    }

}
